<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax_api extends CI_Controller
{

    public $page_level = 'app/';
    public $page_level2 = "";
    public $product_id = 2;
    public $inbox_table;
    public $out_table;

    function __construct()
    {
        parent::__construct();

        $user_type = $this->session->user_type;

        if ($user_type == 1) {
            $this->page_level = 'app/';
        } elseif ($user_type == 2) {
            $this->page_level = 'admin/';
        } elseif ($user_type == 3) {
            $this->page_level = 'client/';
        } else {
            $this->page_level = 'home/';
        }

        $this->inbox_table = 'inbox_' . date('dmY');
        $this->out_table = 'outbox_' . date('dmY');
        $this->create_inbox_table();

    }

    function create_inbox_table($date = null)
    {
        $this->load->library('auto_create_table');

        isset($date) ? $this->auto_create_table->create_inbox_table($date) : $this->auto_create_table->create_inbox_table();

    }

    function inbox_notification()
    {
        $n = $this->db->where(array('replied' => 'N'))->from('inbox')->count_all_results();

        echo $n;

    }

    function get_city($id)
    {

        foreach ($this->db->select('title,state')->from('state')->where('country', $id)->order_by('title', 'asc')->get()->result() as $sc) { ?>
            <option
                    value="<?php echo $sc->state ?>" <?php echo set_select('city', $sc->title); ?>><?php echo $sc->title ?> </option>
        <?php } ?>

        <?php

    }


    function dialing_code($id)
    {
        $c = $this->db->select('dialing_code')->from('country')->where('a2_iso', $id)->get()->row();
        if (count($c) == 1) {
            echo trim($c->dialing_code);
        }

    }


    function realtime_monitoring()
    {

        /*
         * Paging
         */

        $iTotalRecords = $this->db->count_all_results('ci_sessions');
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


        $this->db->select()->from('ci_sessions');
        $query = $this->db->order_by('timestamp', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($query as $row):


            $session_data = $row->data;

            //  print_r($session_data);
            $return_data = array();
            $offset = 0;
            while ($offset < strlen($session_data)) {
                if (!strstr(substr($session_data, $offset), "|")) {
                    throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
                }
                $pos = strpos($session_data, "|", $offset);
                $num = $pos - $offset;
                $varname = substr($session_data, $offset, $num);
                $offset += $num + 1;
                $data = unserialize(substr($session_data, $offset));
                $return_data[$varname] = $data;
                $offset += strlen(serialize($data));
            }

            if (!empty($return_data['username'])) {


                $records["data"][] = array(

//                    '<input type="checkbox"  name="id[]" value="' . $sb->id . '">',

                    $no,
                    trending_date($row->timestamp),
                    $return_data['username'],
                    $row->ip_address,
                    $return_data['first_name'] . '&nbsp;' . $return_data['last_name'],

                    $return_data['email'],
                    isset($return_data['browser']) ? $return_data['browser'] : '',
                    isset($return_data['platform']) ? $return_data['platform'] : ''


                );


            }


            $no++; endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    function users($type = null)
    {


        $status = isset($type) && $type == 'blocked' ? 2 : 0;
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        $this->db->from('users a')->where(array('id !=' => $this->session->id));

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("first_name" => $dir),
            array("phone" => $dir),
            array("phone" => $dir),
            array("created_on " => $dir),
            array("user_type " => $dir),
            array("status " => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.title,')->from('users a')
            ->join('user_type b', 'a.user_type=b.id')
            ->join('country c', 'c.a2_iso=a.country')
            ->where(array('a.id !=' => $this->session->id));

        //filtering
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 24:59:59')) : '';


        isset($_REQUEST["customRoleName"]) && strlen($_REQUEST["customRoleName"]) > 0 ? $this->db->where('a.user_type', $_REQUEST["customRoleName"]) : '';


        isset($_REQUEST["customStatusName"]) && strlen($_REQUEST["customStatusName"]) > 0 ? $this->db->where('a.status', $_REQUEST["customStatusName"]) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $users = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($users as $sb):

            $output = $this->custom_library->role_exist('create user') ? (' <li>' . anchor($this->page_level . 'users/edit/' . $sb->id * date('Y'), '<i class="fa fa-edit"></i> Edit') . '</li>') : '';

            $output .= $this->custom_library->role_exist('create user') && $sb->user_type != 9 ? (' <li>' . anchor($this->page_level . 'users/topup/' . $sb->id * date('Y'), '<i class="icon-file-upload"></i> Topup') . '</li>') : '';


            $output .= $this->custom_library->role_exist('reset user password') ? (' <li>' . anchor($this->page_level . 'users/reset_password/' . $sb->id * date('Y'), '<i class="fa fa-edit"></i> Reset Password') . '</li>') : '';

            $output .= $this->custom_library->role_exist('delete user') ? (' <li onclick="return confirm(\'Deleting user will delete all records attached to a user, Are you sure ? \')">' . anchor($this->page_level . 'users/delete/' . $sb->id * date('Y'), '<i class="fa fa-trash"></i> Delete') . '</li>') : '';

            $output .= $this->custom_library->role_exist('unblock user') || $this->custom_library->role_exist('block user') ? '<li>' . ($sb->status == 2 ? anchor($this->page_level . $this->page_level2 . 'users/unblock/' . $sb->id * date('Y'), '  <i class="fa fa-check"></i> Unblock') : anchor($this->page_level . 'users/ban/' . $sb->id * date('Y'), '  <i class="fa fa-ban"></i> Block', 'onclick="return confirm(\'You are about to ban User from accessing the System \')"')) . '</li>' : '';

            $output .= '<ul/>';


            $status = $sb->status;
            $status_btn = '<div class="btn btn-xs btn-' . ($status == '2' ? 'danger' : 'success') . '">' . ($status == '2' ? 'Blocked' : 'Active') . '</div>';


            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',

                anchor($this->page_level . 'users/edit/' . $sb->id * date('Y'), strlen($sb->first_name) > 0 ? ucwords($sb->first_name . ' ' . $sb->last_name) : '<span style="color: red;">Registration not Completed</span>'),
                $sb->country,
                $sb->phone,


                $sb->rate,
                $sb->balance,
                $sb->title,
                $status_btn,

                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    function billing($type = null)
    {


        $this->db->select('count(a.id) as nos');

        if (isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0) {
            $dates = split_date($_REQUEST['date_range'], '~');

            $from_date = $dates['date1'];
            $to_date = $dates['date2'] . ' 23:59:59';

            $this->db->where(array('a.topup_datetime >=' => $from_date, 'a.topup_datetime <=' => $to_date));
        }
        $this->session->user_type == 7 ? $this->db->where(array('b.parent_id' => $this->session->id)) : '';

        $this->db->from('topup_history a')->join('users b', 'a.account_id=b.id');

        $trecords = $this->db->get()->row();
        $iTotalRecords = $trecords->nos;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("topup_datetime " => $dir),
            array("username" => $dir),
            array("topup_credit" => $dir),
            array("balance_credit" => $dir),
            array("description" => $dir),

        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.username')->from('topup_history a')
            ->join('users b', 'a.account_id=b.id');

        $this->session->user_type == 7 ? $this->db->where(array('b.parent_id' => $this->session->id)) : '';

        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {
            $this->db->group_start();
            $this->db->like(array('b.username' => $_REQUEST['search']['value']));
            $this->db->or_like('a.description', $_REQUEST['search']['value']);
            $this->db->group_end();
        }

        isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0 ? $this->db->where(array('a.topup_datetime >=' => $from_date, 'a.topup_datetime <=' => $to_date)) : '';
        isset($_REQUEST['username']) && strlen($_REQUEST["username"]) > 0 ? $this->db->where(array('a.account_id' => $_REQUEST["username"])) : '';

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));
        $history = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($history as $sb):

            $records["data"][] = array(
                $sb->topup_datetime,
                anchor($this->page_level . 'users/edit/' . $sb->id * date('Y'), $sb->username),
                $sb->topup_credit,
                $sb->balance_credit,
                $sb->description,


            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    function contacts($type = null)
    {


        if (isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0) {
            $dates = split_date($_REQUEST['date_range'], '~');

            $from_date = $dates['date1'];
            $to_date = $dates['date2'] . ' 23:59:59';

            $this->db->where(array('a.topup_datetime >=' => $from_date, 'a.topup_datetime <=' => $to_date));
        }

        $this->db->where(array('id !=' => $this->session->id))->from('contacts a');

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("topup_datetime " => $dir),
            array("username" => $dir),
            array("topup_credit" => $dir),
            array("balance_credit" => $dir),
            array("description" => $dir),

        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.username')->from('contacts a')
            ->join('group b', 'a.account_id=b.id', 'left');

        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {
            $this->db->group_start();
            $this->db->like(array('b.username' => $_REQUEST['search']['value']));
            $this->db->or_like('a.description', $_REQUEST['search']['value']);
            $this->db->group_end();
        }

        isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0 ? $this->db->where(array('a.topup_datetime >=' => $from_date, 'a.topup_datetime <=' => $to_date)) : '';
        isset($_REQUEST['username']) && strlen($_REQUEST["username"]) > 0 ? $this->db->where(array('a.account_id' => $_REQUEST["username"])) : '';

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 24:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));
        $history = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($history as $sb):

            $records["data"][] = array(
                ucwords($sb->first_name . ' ' . $sb->last_name),
                $sb->phone_no,
                $sb->status,
                ''


            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    function messages($type = null, $year = null)
    {
//        http://localhost/sms_gateway/index.php/ajax_api/messages/mt/2015

        // echo $_REQUEST['export'];
        if (isset($_REQUEST['message_date']) && strlen($_REQUEST["message_date"]) > 0) {
            $filter_date = date('dmY', strtotime($_REQUEST['message_date']));

            $filter_table = $this->inbox_table = 'inbox_' . $filter_date;

            $this->create_inbox_table($filter_table);
        }

        if (isset($year) && ($year == 2015 || $year == 2016 || $year == 2017)) {

            $message_table = 'inbox' . $type . '_' . $year;

        } else {

            $message_table = $this->inbox_table;
        }

        $this->db->from($message_table . ' a');

        // isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0 ? $this->db->where(array('a.created_on >=' => $from_date, 'a.created_on <=' => $to_date)) : '';

        $this->session->user_type != 1 ? $this->db->where('username', $this->session->id) : '';
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("phone" => $dir),
            array("network" => $dir),
            array("username" => $dir),
            array("from" => $dir),
            array("message" => $dir),
            array("client_ref_id" => $dir),
            array("message_status" => $dir),
            array("created_on" => $dir),
            array("donedate" => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select()->from($message_table . ' a');

        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();
            $this->db->or_like('a.username', substr($_REQUEST['search']['value'], 1));
            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));
            $this->db->or_like('a.network', substr($_REQUEST['search']['value'], 1));
            $this->db->or_like('a.subject', substr($_REQUEST['search']['value'], 1));
            $this->db->or_like('a.message', substr($_REQUEST['search']['value'], 1));
            $this->db->or_like('a.client_ref_id', substr($_REQUEST['search']['value'], 1));
            $this->db->group_end();

        }

        $this->session->user_type != 1 ? $this->db->where('username', $this->session->id) : '';

        //isset($_REQUEST['date_range']) && strlen($_REQUEST["date_range"]) > 0 ? $this->db->where(array('a.created_on >=' => $from_date, 'a.created_on <=' => $to_date)) : '';
        isset($_REQUEST['network']) && strlen($_REQUEST["network"]) > 0 ? $this->db->where(array('a.network' => $_REQUEST["network"])) : '';
        isset($_REQUEST['username']) && strlen($_REQUEST["username"]) > 0 ? $this->db->where(array('a.username' => $_REQUEST["username"])) : '';
        isset($_REQUEST['msisdn']) && strlen($_REQUEST["msisdn"]) > 0 ? $this->db->where(array('a.phone' => $_REQUEST["msisdn"])) : '';
        isset($_REQUEST['sender_id']) && strlen($_REQUEST["sender_id"]) > 0 ? $this->db->like('a.from', $_REQUEST['sender_id']) : '';

        /*date_range:2017-12-28 ~ 2017-12-28
        network:
        username:
        msisdn:
        sender_id:*/

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;

        foreach ($result as $sb):

            $status = $sb->status;
            $status_btn = '<div class="btn btn-xs btn-' . ($status == '2' ? 'danger' : 'success') . '">' . ($status == '2' ? 'Blocked' : 'Active') . '</div>';


            $account = $this->model->get_user_details($sb->username);

            $data = array(


                $account != false ? anchor($this->page_level . 'users/edit/' . $account->id * date('Y'), $account->username) : $sb->username,

                anchor($this->page_level . 'messages/#' . $sb->id * date('Y'), $sb->phone),
                strtoupper(humanize($sb->network)),
                $sb->from,
                '<span title="' . $sb->content . '"  data-popup="tooltip">' . word_limiter($sb->content, 3) . '</span>',
            );


            isset($year) && ($year == 2015 || $year == 2016 || $year == 2017) ? '' : array_push($data, array($sb->client_ref_id));
            isset($year) && ($year == 2015 || $year == 2016 || $year == 2017) ? '' : array_push($data, array($sb->message_status));

            array_push($data, trending_date($sb->created_on));

            $donedate = strlen($sb->donedate) == 10 ? trending_date(sms_time($sb->donedate)) : '-';

            isset($year) && ($year == 2015 || $year == 2016 || $year == 2017) ? '' : array_push($data, $donedate);

            $records["data"][] = $data;

            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    function get_group($id = null)
    {
        if (isset($id)) {
            $contacts = $this->model->get_group_contacts($id, 5);
            if (count($contacts) > 0) {
                $phone = '';
                foreach ($contacts as $c) {

                    $phone .= ' <span class="label token-label label-success">' . $c->phone_no . '</span> ';

//              $phone .=$c->phone_no.',';
                }
            }

            echo count($contacts) > 0 ? $phone . ' ....' : '<span class="message text-danger">No Contacts found.....</span>';


        } else {

            echo 'Missing Parameter';
        }

    }


    function usage_report()
    {
        $networks = json_decode(json_encode(Networks('RW')));

        if (isset($_REQUEST["date_range"])) {

            $dates = split_date($_REQUEST["date_range"], '~');

            $fdate = $dates['date1'];
            $ldate = $dates['date2'];
        } else {
            $fdate = date('Y-m-01');
            $ldate = date('Y-m-d');
        }
        $dates = period($fdate, $ldate);


        $username=isset($_REQUEST['username'])?$_REQUEST['username']:'';

        $iTotalRecords = 0;
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();

//these are the initial values
        $no = 1;
        $overall_total = 0;
        $net1 = 0;
        $net2 = 0;
        $net3 = 0;
        $net4 = 0;

        foreach ($dates as $r):

            $date = strtotime($r->format("Y-m-d"));

            $data = array(date('Y-m-d', $date));

            $sub_total = 0;

            $nn = 1;
            foreach ($networks as $n) {


                $sms = strlen($username)>0?$this->model->sms_per_day($date, $n->title,$username):$this->model->sms_per_day($date, $n->title);


                array_push($data, $sms);

                //adding Daily Overall totals
                $sub_total += $sms;

                //adding Net work totals
                if ($nn == 1) {
                    $net1 += $sms;
                } elseif ($nn == 2) {
                    $net2 += $sms;
                } elseif ($nn == 3) {
                    $net3 += $sms;
                } elseif ($nn == 4) {
                    $net4 += $sms;
                }
                $nn++;
                //end of adding Network Totals

            }

            array_push($data, $sub_total);

            $records["data"][] = $data;

            $no++;

            //adding Grand total
            $overall_total += $sub_total;

        endforeach;
        $records["data"][] = array('<b>TOTAL</b>', $net1, $net2, $net3, $net4, '<b>' . $overall_total . '</b>');


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


}


?>