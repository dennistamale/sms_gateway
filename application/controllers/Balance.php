<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class balance extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

    }


    //this function credentials messages
    function index()
    {

        if (isset($_REQUEST) && count($_REQUEST) > 0) {

            if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {

                $user_cred = array('username' => $_REQUEST['username'], 'password' => hashValue($_REQUEST['password']));

                $user = $this->db->select('id,balance')->from('users')->where($user_cred)->get()->row();

                if (count($user) > 0) {


                    $output = array(
                        'alert' => 'success',
                        'message' => $user->balance
                    );


                } else {
                    $output = array(
                        'alert' => 'error',
                        'message' => 'Invalid Credentials'
                    );

                }


            } else {

                $output = array(
                    'alert' => 'error',
                    'message' => 'Some Parameters are missing'
                );
            }

        } else {


            $output = array(
                'alert' => 'error',
                'message' => 'No Content Received'
            );
        }


        header('Content-Type: application/json');
        echo json_encode($output);
    }


}