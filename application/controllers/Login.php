<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";

    public $footer_news = "";

    public $notification = array();

    public $view_data = array();

    function __construct()
        {
            parent::__construct();
            $this->page_level = $this->uri->slash_segment(1);
            $this->page_level2 = $this->uri->slash_segment(2);

        }

    public function index($page = null)
        {
            
            $this->login();
//            $this->home();
//
           // $this->load->library('Custom_library');

           // $this->custom_library->site_visits();


            //$this->load->view('login/styles');


        }


    public function login()
        {
            $data['title'] = 'login';
            $data['subtitle'] = 'login';
            $this->load->view('login/header',$data);

            if (strlen($this->session->userdata('username')) > 0) {
                $user_type = $this->session->userdata('user_type');

             $this->login_module($user_type);

            }
            else {

                if (isset($_REQUEST['username'])) {

                    $this->form_validation->set_rules('username', 'Email', 'required|trim');
                    $this->form_validation->set_rules('password', 'Password', 'required|trim');


                    if ($this->form_validation->run() == true) {
                        $this->username = $this->input->post('username', true);
                        $this->password = $this->input->post('password', true);


                        $results = $this->model->account_exists($this->username, $this->password);


                        if (isset($results->username)) {
                            //this check wetha the accounnt is not disabled
                            if ($results->status == 2) {
                                //echo 'account disabled';
                                $data['message'] = '<br/>Your Account is Currently Suspended <br/> <strong>Contact the Administrator</strong>';
                                $this->load->view('login/login', $data);
                                //$this->load->view('locked', $data);
                            }
                            else {
                                //$amount=$this->db->select('current_amount')->from('wallet')->where('user',$results->id)->get()->row();
                                $session_data = array(
                                    'id' => $results->id,
                                    'username' => $results->username,
                                    'first_name' => $results->first_name,
                                    'last_name' => $results->last_name,
                                    'photo' => $results->photo,
                                    'email' => $results->email,
                                    'country_mission'=>$results->country,
                                    'user_type' => $results->user_type,
                                    'user_title' => $results->title,
                                    'sub_type' => $results->sub_type,
                                    'currency'=>$results->currency,
                                    'phone' => $results->phone,
                                    'verified'=>$results->verified,
                                    'platform' => $this->agent->platform(),
                                    'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                                    'agent_string' => $this->agent->agent_string(),
                                    'agent_referal' => $this->agent->is_referral() ? $this->agent->referrer() : '',
                                    'balance_p'=>$results->balance_p

                                );


                                $this->session->set_userdata($session_data);
                                $this->model->get_permissions($this->session->user_type);
                                $this->add_logs('', 'login', '', ' Logged in');
                                $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));
                                //this code redirects the users of the website
                                $this->login_module($results->user_type);
                            }
                            //end of the account status check


                        }
                        else {

                            //$message = 'Testing';

                            $data['message'] = 'Invalid Username or Password';
                            $this->load->view('login/login', $data);

                        }


                    }
                    else {

                        $data['message'] = 'All fields are Required';

                        $this->load->view('login/login', $data);

                    }


                }
                else {

                    $this->load->view('login/login', $data);
                }

            }

        }


        function login_module($user_type){

            redirect('app', 'refresh');


//            if ($user_type == 1) {
//                redirect('app', 'refresh');
//            }
//            elseif ($user_type == 2) {
//                redirect('admin', 'refresh');
//            }
//            elseif ($user_type == 3) {
//                redirect('farmer', 'refresh');
//            }
//            elseif ($user_type == 4) {
//                redirect('trader', 'refresh');
//            }
//            elseif ($user_type == 5) {
//                redirect('agent', 'refresh');
//            }
//            elseif ($user_type == 6) {
//                redirect('partner', 'refresh');
//            }
//            else {
//                $this->logout();
//            }
        }



    public function verify_sms_code()
        {
            if (strlen($this->session->userdata('id')) > 0) {
                $phno = $this->session->userdata('mobile');
                $_REQUEST['mobile'] = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
                $_REQUEST['code'] = $_REQUEST['verification_code'];

                if ($this->db->select('id')->from('verification_codes')->where(array('mobile' => $_REQUEST['mobile'], 'status' => 0, 'code' => $_REQUEST['code']))) {
                    //$this->db->where('mobile = "'.$_REQUEST['mobile'].'" and code = "'.$_REQUEST['code'].'"');
                    $this->db->update('verification_codes', array('status' => 1), array('mobile' => $_REQUEST['mobile'], 'code' => $_REQUEST['code']));
                    $this->db->update('users', array('username' => $_REQUEST['mobile'], 'mobile_verified' => 1, 'status' => 1), array('mobile' => $_REQUEST['mobile']));

                    $data['mobile_verified'] = 1;

                    $data['SESSION_ID'] = sha1(date('Y-m-d H:i:s') . $_REQUEST['mobile']);
                    $this->session->set_userdata($data);

                    $this->db->insert('sessions',
                        array('session_id' => $data['SESSION_ID'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'mobile' => $_REQUEST['mobile']));


                    $data['status'] = 8;
                    $data['message'] = 'Your account has been verified successfully';
                }
                else {


                    $data['status'] = 9;
                    $data['message'] = 'You have used an unknown code to activate';
                }


                redirect($this->page_level . 'myaccount', 'refresh');

            }

        }


    public function request_code()
        {

            if (strlen($this->session->userdata('id')) > 0) {

                $verification_code = $this->GetSMSCode(5);


                $this->db->where('mobile = "' . $this->session->userdata('mobile') . '" and status = 0');
                $this->db->update('verification_codes', array('status' => 2));
                $this->db->insert('verification_codes',
                    array('mobile' => $this->session->userdata('mobile'),
                        'code' => $verification_code,
                        'created_at' => date('Y-m-d H:i:s')));

                $message = 'ePay Verification Code : ' . $verification_code;


                $values = array(
                    'created' => date('Y-m-d H:i:s'),
                    'sender' => 'ePay',
                    'receiver' => $this->session->userdata('mobile'),
                    'message' => $message,
                    'type' => 'VERIFICATION_CODE',
                    'mobile' => $this->session->userdata('mobile'),
                    'transactionid' => 0);

                $this->db->insert('outbox_messages', $values);


                $this->compticketmodel->SendSMS('ePay', $message, $this->session->userdata('mobile'));

                redirect('home', 'refresh');

            }
        }


    public function isloggedin()
        {
            if (strlen($this->session->id) > 0) {
                return true;

            }
            else {
                return false;

            }

        }






    /**
     * @param $trans_type accountModification_accountCreation_accountDeletion_AccountChange
     * @param string $target
     * @param string $desc
     */
    function add_logs($user = null, $trans_type, $target = '', $desc = '')
        {
            $this->load->library('user_agent');
            $user = strlen($user) > 0 ? $user : $this->session->id;

            $values = array(
                'transaction_type' => $trans_type,
                'target' => character_limiter($target, 100),
                'details' => $desc,
                'created_by' => $user,
                'created_on' => time(),
                'platform' => $this->agent->platform(),
                'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                'agent_string' => $this->agent->agent_string(),
                'ip' => $this->input->ip_address(),
                'agent_referral' => $this->agent->is_referral() ? $this->agent->referrer() : '',
            );
            $this->db->insert('logs', $values);
        }


    //function sending an email


    function test_ip()
        {
            $ip = $this->input->ip_address();

            //$ip='154.73.12.119';

            $this->load->library('Custom_library');

            $r = $this->custom_library->site_visits();

            //  print_r($r);

            echo($r->statusCode);


        }


    //this of the password change
    function password_change($id, $phone)
        {


            $user = $data['user'] = $this->db->select()->from('users')->where(array('id' => $id / date('Y'), 'mobile' => $this->phone($phone)))->get()->row();
            if (isset($user->id)) {
                $data = array('view' => 'password_change', 'sublink' => 'password_change');
                $this->form_validation->set_rules('password', 'Password', 'trim|matches[passconf]|required|xss_clean');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean');
                if ($this->form_validation->run() == false) {

                }
                else {
                    $this->db->where(array('mobile' => $phone, 'id' => $id / date('Y')))->update('users', array('password' => hashValue($this->input->post('password'))));

                    $data['message'] = 'Your Password Has Been Updated Successfully. ' . anchor($this->page_level . 'login', 'Click Here to Login');
                    $data['alert'] = 'success';
                    $msg = 'Your account  Password Has been Changed ( ' . base_url() . ' )';
                    //$this->SendSMS('Virtual Wallet',$phone,$msg);
                    $this->sendemail($user->email, 'Password Reset', $msg);

                }
            }
            else {
                $data = array('view' => 'reset_password', 'sublink' => 'reset_password');
                $data['message'] = 'An error has Occurred Please Try again';
                $data['alert'] = 'warning';
            }

            $this->load_home($data);

        }

    // this is the function for the reseting the password
    function reset_password()
        {
            $data['view'] = 'reset_password';
            $data['sublink'] = 'reset_password';
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_email_exist');
            if ($this->form_validation->run() == false) {
                //$this->load->view('account_reset');
            }
            else {
                $this->load->helper('text');
                $user = $this->db->select()->from('users')->where('email', $this->input->post('email'))->get()->row();
                $data['message'] = 'Check Your email to complete account Reset ';
                $data['alert'] = 'success';
                $email_msg = "Dear " . $user->fname . " " . $user->lname . " \n\r\n We Thank you for using Our Services Please follow the link below to reset the account \n\r\n" . site_url($this->page_level . 'password_change/' . $user->id * date('Y') . '/' . $user->mobile) . "'>Follow This Link'";
                //$this->load->view('account_reset',$data);
                $this->sendemail($user->email, 'Virtual Wallet Account Reset', $email_msg);
            }
            $this->load_home($data);

        }

    //this is the function
    function email_exist($str)
        {
            $user = $this->db->select('email')->from('users')->where('email', $str)->get()->row();
            if (isset($user->email)) {
                return true;
            }
            else {
                $this->form_validation->set_message('email_exist', 'The %s you entered is wrong Please Try Again');
                return false;
            }
        }

    //this is the function for email verification
    function email_verification()
        {
            if (strlen($this->session->userdata('username')) > 0) {
                $data['title'] = 'Email Verification';
                $data['group'] = 'profile';
                $data['desc'] = 'Send an Admin a Message for any inquiry'; // the is the page description
                $this->load->view('header', $data);
                $this->load->helper('text');
                $user = $this->Octopusmodel->select_user($this->input->post('mobile'));
                $data['msg'] = 'Please Check Your email to complete account Verificaton ' . anchor('octopus', 'Go Back') . ' ' . $this->session->userdata('email');
                $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services Please follow the link below to Verify your email account \n\r\n" . site_url('/octopus/reg_complete/' . $this->session->userdata('id'));
                $this->sendemail($this->session->userdata('email'), 'Octopus Account Verification', $email_msg);
                //this shows the message alert
                $data['alert_type'] = 'info';
                $this->load->view('admin/alert', $data);
                $this->load->view('footer', $data);
            }
            else {
                $this->login();
            }
        }

    //this checks for the email weather it exist when signing up an account
    //checking for the valid number
    //checking for the username weather it exists in the database
    public function username_check($str)
        {
            $results = $this->db->select('username')->from('users')->where('email', $str)->get()->row();

            if (isset($results->username)) {
                $this->form_validation->set_message('username_check', 'The %s already Exists');
                return false;
            }
            else {
                return true;
            }
        }

    // this is the email check for the user
    public function email_check($str)
        {
            $results = $this->db->select('email')->from('users')->where('email', $str)->get()->row();

            if (isset($results->email)) {
                $this->form_validation->set_message('email_check', 'The %s already Exists');
                return false;
            }
            else {
                return true;
            }
        }


    // This is the function getting code
    public function GetCode($length)
        {

            $codes = array();
            $chars = "01a2B3c4D5EF6G7H8I9jK";
            srand((double)microtime() * 1000000);
            $i = 0;
            $code = '';
            $serial = '';

            $i = 0;

            while ($i < $length) {
                $num = rand() % 10;
                $tmp = substr($chars, $num, 1);
                $serial = $serial . $tmp;
                $i++;
            }

            return $serial;

        }




    function register()
    {
        $data['title'] = 'register';
        $data['subtitle'] = 'signup';

        //$data['code']=$this->Octopusmodel->country_code();
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]xss_clean');
        //$this->form_validation->set_rules('lastname', 'Last Name','trim|required|min_length[3]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'callback_email_check|trim|valid_email|required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[10]|max_length[13]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
        $this->form_validation->set_rules('username', 'Username', 'trim|is_unique[users.username]');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|max_length[1]|required');
        $this->form_validation->set_rules('city', 'City', 'trim');
        //$this->form_validation->set_rules('dob','Date of Birth','trim|required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|required');

        if ($this->form_validation->run() == false) {
            //$this->load->view('register',$data);

        } else {
            $data['msg'] = 'The Registration Process is completed Successfully <br/> <p> An Email Has been Sent For Verification to ' . $this->input->post('email') . ' </p>';
            //$this->load->view('loginmsg', $data);
            $v['full_name'] = $this->input->post('fullname');
            $username = $v['username'] = $this->input->post('username');
            $email = $v['email'] = strtolower($this->input->post('email'));
            $v['city'] = $this->input->post('city');
            $v['gender'] = $this->input->post('gender');
            $v['country'] = $this->input->post('country');
            $v['password'] = hashValue($password = $this->input->post('password'));
            $v['phone'] = $this->input->post('dialing_code').$this->input->post('phone');
            //$v['date_of_birth'] = $this->input->post('dob');
            $v['created_on'] = time();

            $this->db->insert('users', $v);


            $msg = $this->input->post('fullname') . ' Your account ' . $username . ' Has been created with us, Thank You for Registering ' . site_url();
            //$this->notification($username,$msg,$this->input->post('fname').' '.$this->input->post('lname'),$this->input->post('email'));
            //$this->sendemail($email,' Account Creation',$msg);
            $this->session_assign($username, $password);
            header('Refresh: 1; url=' . base_url('index.php/home/login/'));

        }
        //$this->load->view($this->page_level . 'login/register', $data);
        $this->load->view($this->page_level . 'static/main_page', $data);
    }



    // this is the function for contact us


    public function logout()
        {
            $this->session->sess_destroy();
            redirect($this->page_level . 'login', 'refresh');
        }

    //this is the error page
    public function error()
        {
            $this->load->view('404');
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */