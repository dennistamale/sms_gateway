<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class send extends CI_Controller
{

    public $country = "UG";
    public $product_id = 2;
    public $inbox_table;
    public $out_table;

    function __construct()
    {
        parent::__construct();

        $this->inbox_table = 'inbox_' . date('dmY');
        $this->out_table = 'outbox_' . date('dmY');
    }


    //this function recieves messages
    function index()
    {
//        http://10.150.61.147:3070/send?username=rdb&password=Rd4b2016&to=250785250842&content=hello+world&from=test&tags=4949494&dlr=yes&dlr-url=http://196.223.254.144:8031/soa/MobileMoney/SMS_Notification_new&dlr-level=3&dlr-method=post


        if (isset($_REQUEST) && count($_REQUEST) > 0) {

            $this->create_inbox_table();
            // $this->create_outbox_table();




            if(isset($_REQUEST['username'])&&isset($_REQUEST['password'])&&isset($_REQUEST['client_ref'])&&isset($_REQUEST['from'])) {

                $user_cred = array('username' => $_REQUEST['username'], 'password' => hashValue($_REQUEST['password']));

                if ($this->db->from('users')->where($user_cred)->count_all_results() == 1) {




                    if ($this->db->from($this->inbox_table)->where('client_ref_id', $_REQUEST['client_ref'])->count_all_results() == 0) {



                        if(isset($_REQUEST['dlr-level'])&&isset($_REQUEST['dlr'])&&isset($_REQUEST['dlr-url'])&&isset($_REQUEST['dlr-method'])){



                            $this->insms($_REQUEST['username'], $_REQUEST['password'], $_REQUEST['from'], $_REQUEST['content'], $_REQUEST['to'], $_REQUEST['client_ref'], $_REQUEST['dlr'], urldecode($_REQUEST['dlr-url']), $_REQUEST['dlr-level'], $_REQUEST['dlr-method']);

                        }else{

                            $dlr='yes';
                            $dlr_url=base_url('send/dummy_url');
                            $dlr_level=3;
                            $dlr_method='POST';

                            $this->insms($_REQUEST['username'], $_REQUEST['password'], $_REQUEST['from'], $_REQUEST['content'], $_REQUEST['to'], $_REQUEST['client_ref'],$dlr,$dlr_url,$dlr_level,$dlr_method);

                        }




                    } else {

                        echo 'Reference Exists';

                    }
                } else {

                    echo 'Invalid Credentials';
                }
            }else{
                echo 'Some Parameters are missing';
            }

        } else {
            echo 'No Content Received';
        }


    }


    function dummy_url(){

    }


    function insms($username,$password,$from = null, $content = null, $to = null, $server_ref = null,$dlr=null, $dlr_url = null, $dlr_level = null, $dlr_method = null)
    {

        $sms = urldecode($content);

        if (isset($username) &&isset($password) &&isset($from) && isset($content) && isset($to)) {

            $user=$this->model->get_user_details($username,'username');

            $time = time();
            $values = array(
                'username' => $user!=false?$user->id:$username,
                'content' => $content,
                'content_count' => message_count($content),
                'phone' => $phone = phone(trim($to)),
                'from' => $from,
                'ip' => $this->input->ip_address(),
                'created_on' => $time,
                'client_ref_id' => $server_ref,
                'dlr' => $dlr,
                'dlr_url' => $dlr_url,
                'dlr_level' => $dlr_level,
                'dlr_method' => $dlr_method,
                'network'=>getNetwork($phone)
            );

            if ($this->db->insert($this->inbox_table, $values)) {

                echo $this->send_sms($username,$password,$phone, $sms, $from, $server_ref);

            }


        } else {
            echo 'Error Missing parameters';
        }


    }

    function send_sms($username,$password,$phone, $sms, $subject = 'company', $client_ref_id = null)

    {
        if (!empty($phone)) {

            if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                if (strstr($phone, ',')) {
                    $phone = substr($phone, 0, strpos($phone, ','));
                }
                if (strstr($phone, '/')) {
                    $phone = substr($phone, 0, strpos($phone, '/'));
                }
                if (strstr($phone, '-')) {
                    $phone = substr($phone, 0, strpos($phone, '-'));
                }
                $phone = trim($phone);
            }

            if (strlen($phone) >= 9) {

                if (substr($phone, 0, 1) == '0') {
                    if (strlen($phone) == 10) {
                        $phone = '250' . substr($phone, 1);
                    }
                }
                if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                    $phone = '250' . $phone;
                }
                if (substr($phone, 0, 1) == '+') {
                    $phone = substr($phone, 1);
                }


                $from = $subject;
                $username = 'daniel';
                $password = "daniel";

                $url_encoded_sms = urlencode($sms);
                $url_to_reply = base_url('send/send_client_reply/');


                $url = "http://127.0.0.1:1401/send?username=$username&password=$password&to=$phone&content=$url_encoded_sms&from=$from";
                $url .= "&dlr=yes&dlr-level=2&dlr-url=$url_to_reply&dlr-method=GET";


                $socket = url_get_contents($url);

                if ($socket) {


                    //insert data into database
                    $this->db->insert('outbox', array('to_user' => $phone, 'subject' => $subject, 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => '0'));
//

                    //update the message in reference
                    $response = explode(' ', $socket);
                    $values = array(
                        'status' => $response[0],
                        'server_ref_id' => str_replace('"', '', $response[1])
                    );

                    $this->db->where('client_ref_id', $client_ref_id)->update($this->inbox_table, $values);


                    return $socket;

                } else {
                    return 'An error has occurred while sending message';
                }

            } else {
                return 'Phone Number is Invalid';
            }
        } else {
            return 'Phone number missing';

        }
    }


    function send_client_reply()
    {

        if (isset($_REQUEST) && count($_REQUEST) > 0) {

            echo 'ACK/Jasmin';


            $server_ref_id = $_REQUEST['id'];
            $where = array('server_ref_id' => $server_ref_id);
            $qry = $this->db
                ->select('server_ref_id,client_ref_id,dlr_url')
                ->from($this->inbox_table)
                ->where($where)->get()->row();

            if (count($qry) == 1) {

                $values = array(
                    'message_status' => $message_status = $_REQUEST['message_status'],
                    'donedate' => $_REQUEST['donedate'],
                    'id_smsc' => $_REQUEST['id_smsc']
                );

                if ($this->db->where($where)->update($this->inbox_table, $values)) {


                    //Posting to a client feedback
                    $tags = $qry->client_ref_id;
                    $message_status = $message_status;

                    $url = $qry->reply_url . "?tags=$tags&message_status=stat:$message_status";


//                    $output = $this->post_to_http($url);
                    $output = $this->post_to_api($url);
//                    $output = file_get_contents($url);
                    if ($output !== false) {

                        $replies = array(
                            'full_reply_url' => $url,
                            'full_reply_url_time' => time()
                        );
                        $this->db->where($where)->update($this->inbox_table, $replies);


                    }
                }


            } else {
                $string = "=>Error String:" . 'No data found' . '>>>>' . date('Y-m-d H:i:s') . PHP_EOL;
                $this->log_client($string);
            }

        } else {
            echo 'No Content Received';
        }


    }


    function post_to_http($url)
    {

        $ch = curl_init();                    // initiate curl
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);  // tell curl you want to post something
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //verifying
        $output = curl_exec($ch); // execute
        curl_close($ch); // close curl handle
        return $output; // show output
    }


    function post_to_api($url)
    {
        $ch = curl_init();// initiate curl
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// return the output in string format
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //verifying


        $output = curl_exec($ch); // execute

        if ($output === false) {
            $data = " =>Error Number:" . curl_errno($ch) . '>>>>';
            $data .= " Error String:" . curl_error($ch) . '>>>>' . date('Y-m-d H:i:s') . ',' . PHP_EOL;;
//            $data .= " Extra Info:" . var_dump(curl_getinfo($ch)) . '>>>>' . date('Y-m-d H:i:s').',' . PHP_EOL;
            $this->log_client($data);

        }
        curl_close($ch);
        return $output;
    }


    function log_client($string)
    {
        $this->load->helper("file");

        $path = 'client_logs/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $date = date('dmY');
        $file = "client_log_$date.txt";

        if (!write_file($path . $file, $string, "a+")) {
            //echo 'Unable to write the file';
        } else {
            //echo 'File written!';
        }

    }


    function create_inbox_table()
    {
        $this->load->library('auto_create_table');
        $this->auto_create_table->create_inbox_table();

    }

    /*
     username
password
to
content
from
client_ref(tags)
dlr (default =yes)
dlr-url (your delivery report API)
dlr-level (default = 3)
dlr-method (default = get)
     * */


    function testing_jasmin(){

        echo '<h2>Connector</h2>';
        $this->load->library('jasmin_connector');
        $this->jasmin_connector->dennis();
        echo '<hr/>';

        echo '<h2>User</h2>';
//        $connection = JasminConnector::init('jcliadmin', 'jclipwd');
        $connection=$this->jasmin_connector->init('jcliadmin', 'jclipwd');

        $gf=$this->jasmin_connector->getResponse();
        print_r($gf);

        //print_r($connection);
       print_r($connection);
       print_r($connection->fp,'username');
        $this->load->library('jasmin_user',$connection);

      $users= $this->jasmin_user->dennis();

      print_r($users);


    }


    function sms_time($time = 0)
    {
        $year = $substring = substr($time, 0, 2);
        $month = $substring = substr($time, 2, 2);
        $date = $substring = substr($time, 4, 2);
        $hour = $substring = substr($time, 6, 2);
        $min = $substring = substr($time, 8, 2);

        return $old_date = mktime($hour, $min, 00, $month, $date, $year);

    }

}


?>


