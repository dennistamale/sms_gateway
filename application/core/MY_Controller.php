<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";
    public $notification = array();
    public $view_data = array();
    public $site_name = '';
    public $site_email = '';
    public $inbox_table;
    public $outbox_table;


    function __construct()
    {
        parent::__construct();

        $this->isloggedin();
        //if logged in continue
        $this->site_name = $this->site_options->title('site_name');
        $this->site_email = $this->site_options->title('contact_email');


        $this->page_level = $this->uri->slash_segment(1);
        $this->page_level2 = $this->uri->slash_segment(2);
        $this->page_level3 = $this->uri->slash_segment(3);
        // $this->load->library('cart');
//            $this->output->enable_profiler(TRUE);

//        $n=30;
//        $this->output->cache($n);
        //$this->page_level2 != 'logout' ? $this->verified() : '';


        $this->inbox_table = 'inbox_' . date('dmY');

        $this->out_table = 'outbox_' . date('dmY');


    }

    function verified()
    {
        if ($this->session->verified == 0 && $this->session->user_type == 5) {

            $page = $this->uri->segment(2);
            $page != 'profile' ? redirect($this->page_level . 'profile/change_password') : '';
        }
    }

    public function isloggedin()
    {
        return strlen($this->session->user_type) > 0 ? true : false;

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('', 'refresh');
        //echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/login>';
    }

    public function multiple_upload($prefered_name = 'image')
    {
        //$this->load->library('upload');
        $number_of_files_uploaded = count($_FILES['userfile']['name']);
        // Faking upload calls to $_FILE
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
            $_FILES['userfile']['name'] = $_FILES['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $_FILES['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $_FILES['userfile']['size'][$i];
            $config = array(
                'file_name' => $prefered_name,
                'allowed_types' => 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG',
                'max_size' => 3000,
                'overwrite' => FALSE,

                /* real path to upload folder ALWAYS */
                'upload_path' => './uploads/arrival/'
            );
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) :
                $error = array('error' => $this->upload->display_errors());
                $error;
            //$this->load->view('upload_form', $error);
            else :
                $final_files_data[] = $this->upload->data();
                // Continue processing the uploaded data
            endif;
        endfor;
    }


    public function reports($type = 'details', $id = null)
    {

        $data['title'] = $title = 'reports';//$this->uri->segment(2);
        $data['subtitle'] = $type;


        switch ($type) {

            default:

                $this->form_validation
                    ->set_rules('export', 'Export', 'trim|required')
                    ->set_rules('message_date', 'Message Date', 'trim')
                    ->set_rules('network', 'Message Date', 'trim')
                    ->set_rules('username', 'Username', 'trim')
                    ->set_rules('msisdn', 'msisdn', 'trim')
                    ->set_rules('sender_id', 'Username', 'trim');


                if ($this->form_validation->run() == true) {

                    if ($this->input->post('export') == 'excel') {

                        $filter_date = date('dmY', strtotime($this->input->post('message_date')));

                        $message_date = strlen($this->input->post('message_date')) > 0 ? $filter_date : date('dmY');
                        $network = $this->input->post('network');
                        $username = $this->input->post('username');
                        $sender_id = $this->input->post('sender_id');
                        $msisdn = $this->input->post('msisdn');
                        $parameters = '?';
                        $parameters .= "message_date=$message_date";
                        $parameters .= strlen($network) > 0 ? "&network=$network" : "";
                        $parameters .= strlen($username) > 0 ? "&username=$username" : "";
                        $parameters .= strlen($sender_id) > 0 ? "&sender_id=$sender_id" : "";
                        $parameters .= strlen($msisdn) > 0 ? "&msisdn=$msisdn" : "";


                        $year = $this->uri->segment(4);
                        $year = strlen($year) > 0 && ($year == 2015 || $year == 2016 || $year == 2017) ? $this->uri->segment(3) . '/' . $year : "";


                        redirect("excel_export/messages/$year" . $parameters);

                    }

                }
                $data['page_view'] = $this->load->view('app/reports/messages', $data, true);
                break;

            case 'edit':


                $data['id'] = $id = $id / date('Y');


                $this->form_validation
                    ->set_rules('week_no', 'Week no', 'trim|required')
                    ->set_rules('report_year', 'Year', 'trim|required')
                    ->set_rules('facility', 'Facility', 'trim|required');

                if ($this->form_validation->run() == true) {


                    foreach ($this->input->post() as $r => $t) {

                        if ($r != 'week_no' || $r != 'report_year' || $r != 'facility') {

                            $where = array(
                                'week_no' => $this->input->post('week_no'),
                                'report_year' => $this->input->post('report_year'),
                                'facility' => $this->input->post('facility'),
                                'indicator' => $r
                            );

                            $values = array('indicator_value' => $t);


                            $this->db->where($where)->update('facility_report', $values);

                        }
                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'Report has been updated successfully'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                    // $data['page_view'].=$this->load->view('app/'.$this->page_level2.$title,$data,true);


                } else {

                    $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                }


                break;


            case 'view':


                $data['id'] = $id = $id / date('Y');

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                break;


            case 'dashboard':
                $data['subtitle'] = 'report_dashboard';

                $this->form_validation->set_rules('filter', 'Filter', 'trim');
                if ($this->form_validation->run() == true) {


                    $data['week_no'] = $week = $this->input->post('week_no');
                    $data['year'] = $year = $this->input->post('year');

                    //calculating the actual week and the year incase the year has 53 weeks
                    $actual_week = $week + 1 != 53 ? $week + 1 : 1;
                    $actual_year = $week + 1 != 53 ? $year : $year + 1;

                    $getStartAndEndDate = getStartAndEndDate($actual_week, $actual_year);

                    //outputing the days to view
                    $data['fdate'] = $getStartAndEndDate[0] . ' 00:00:00';
                    $data['last_day'] = $getStartAndEndDate[1] . ' 23:59:59';
                    $data['subtitle'] = 'Week:' . $week . ' Year:' . $year;


                } else {

                    $data['fdate'] = date('Y-m-01');
                }

                $data['indicators'] = $this->db->select('code,indicator,initial')->from('indicators')->get()->result();
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'dashboard/overall_chat', $data, true);
                break;

            case 'usage_report':


                $this->form_validation
                    ->set_rules('export', 'Export', 'trim|required')
                    ->set_rules('message_date', 'Message Date', 'trim')
                    ->set_rules('network', 'Message Date', 'trim')
                    ->set_rules('username', 'Username', 'trim')
                    ->set_rules('msisdn', 'msisdn', 'trim')
                    ->set_rules('sender_id', 'Username', 'trim');


                if ($this->form_validation->run() == true) {

                    if ($this->input->post('export') == 'excel') {

                        $filter_date = date('dmY', strtotime($this->input->post('message_date')));

                        $message_date = strlen($this->input->post('message_date')) > 0 ? $filter_date : date('dmY');
                        $network = $this->input->post('network');
                        $username = $this->input->post('username');
                        $sender_id = $this->input->post('sender_id');
                        $msisdn = $this->input->post('msisdn');
                        $parameters = '?';
                        $parameters .= "message_date=$message_date";
                        $parameters .= strlen($network) > 0 ? "&network=$network" : "";
                        $parameters .= strlen($username) > 0 ? "&username=$username" : "";
                        $parameters .= strlen($sender_id) > 0 ? "&sender_id=$sender_id" : "";
                        $parameters .= strlen($msisdn) > 0 ? "&msisdn=$msisdn" : "";


                        $year = $this->uri->segment(4);
                        $year = strlen($year) > 0 && ($year == 2015 || $year == 2016 || $year == 2017) ? $this->uri->segment(3) . '/' . $year : "";


                        // redirect("excel_export/messages/$year" . $parameters);

                    }

                }

                $data['networks'] = json_decode(json_encode(Networks('RW')));

                $data['page_view'] = $this->load->view('app/reports/' . $type, $data, true);
                break;

        }


        $this->load->view('app/static/main_page', $data);

    }


    public function users($type = null, $id = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type
        );
        $type != 'filter' ? $id = $data['id'] = $id / date('Y') : '';
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data['page_view'] = '';


        switch ($type) {

            default:

                $this->form_validation->set_rules('user_role', 'User role', 'trim')->set_rules('status', 'Status', 'trim');
                if ($this->form_validation->run() == 'true') {
                    strlen($user_role = $this->input->post('user_role')) > 0 ? $data['user_role'] = $user_role : '';
                    strlen($status = $this->input->post('status')) > 0 ? $data['status'] = $status : '';
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                break;

            case 'new':
                //this is the section for the form validation
                $this->form_validation
//                        ->set_rules('sub_type', 'User Type', 'trim|required')
                    ->set_rules('first_name', 'First Name', 'trim|required')
                    ->set_rules('last_name', 'Last Name', 'trim|required')
                    ->set_rules('username', 'Username', 'trim|required|is_unique[users.username]')
                    ->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]')
//                    ->set_rules('zip_code', 'Zip Code', 'trim|required')
                    ->set_rules('country', 'Country', 'trim|required')
                    ->set_rules('city', 'City', 'trim')
                    ->set_rules('phone', 'Phone', 'trim|required|is_unique[users.phone]|max_length[15]')
                    ->set_rules('gender', 'Gender', 'trim|required|exact_length[1]')
                    ->set_rules('access', 'Access', 'trim')
                    ->set_rules('currency', 'Currency', 'trim')
                    ->set_rules('rate', 'Rate', 'trim')
                    ->set_rules('dob', 'Date of Birth', 'trim|callback_dob_check');
                // ->set_rules('referral', 'Referral', 'trim');


                if ($this->form_validation->run() == true) {

                    $password = code(6);

                    $dob = $this->input->post('dob');


                    $values = array(
                        'first_name' => $fname = $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'username' => $username = $this->input->post('username'),
                        'password' => hashValue($password),
                        'phone' => phone($this->input->post('phone')),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'gender' => $this->input->post('gender'),
                        'user_type' => $this->input->post('role'),
                        'currency' => $this->input->post('currency'),
                        'rate' => $this->input->post('rate'),
                        'dob' => strtotime($dob),
                        'balance_p' => str_rot13($password),
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id'),
//                            'sub_type' => strlen($this->input->post('referral')) > 0 ? 2 : 1,
//                            'referral' => strlen($this->input->post('referral')) > 0 ? $this->input->post('referral') : ''

                    );


                    if ($this->db->insert('users', $values)) {

                        $this->load->model('jasmin_model');
                        $this->jasmin_model->create_account($values);

                        $data['message'] = 'User has been added successfully';
                        $data['alert'] = 'success';
                        $data['alert_page'] = $this->load->view('alert', $data, true);


                        //this Lists the users in the system


                        $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                        $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $username: and password is: $password
To login, go to URL. Remember you can change your password once logged.\r\n";
                        $this->custom_library->sendHTMLEmail2($username, $this->site_name, $email_msg);
                        $this->custom_library->add_logs('', 'account_creation', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has created an account for user ');


                    } else {

                    }

                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                }

                break;
            case 'edit':

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('email', 'Email', 'valid_email|trim')
                    ->set_rules('phone', 'Phone', 'trim|max_length[15]|min_length[10]')
                    ->set_rules('first_name', 'First Name', 'trim')
                    ->set_rules('last_name', 'Last Name', 'trim')
                    ->set_rules('role', 'Role', 'trim')
                    ->set_rules('currency', 'Currency', 'trim')
                    ->set_rules('rate', 'Rate', 'trim');

                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'profile_tabbed', $data, true);

                } else {

                    $this->db->where('id', $id)
                        ->update('users', array(
                            'email' => $this->input->post('email'),
                            'phone' => $this->input->post('phone'),
                            'user_type' => $this->input->post('role'),
                            'first_name' => $fname = $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'currency' => $this->input->post('currency'),
                            'rate' => $this->input->post('rate'),


                        ));

                    $data['message'] = 'User account is updated Successfully';
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                    $email = $this->input->post('email');
                    $email_msg = "Dear $fname ,\r\n Your account has been Updated\n\r";
                    $this->custom_library->sendHTMLEmail2($email, $this->site_name, $email_msg);
                    $this->custom_library->add_logs('', 'account_modification', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated account for user');

                }
                break;

            case 'tabbed':


                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'profile_tabbed', $data, true);
                break;
            //this is the part for changing avatar
            case 'change_avatar':


                // this is the function which uploads the profile image
                $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
                //this is the company name

                $cname = 'users_profile';
                //managing of the images
                $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '200';
                $config['max_width'] = '1920';
                $config['max_height'] = '850';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                    $values = array(
                        'photo' => $path . $this->upload->file_name,
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')
                    );
                    $this->db->where('id', $id)->update('users', $values);
                    // $this->session->set_userdata(array('photo'=>$path.$this->upload->file_name));
                    $data['message'] = 'Image has been updated Successfully';
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);

                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account Profile Picture has been Updated";
                    $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);

                    $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has changed picture for user ');

                } else {
                    $data['error'] = $this->upload->display_errors();
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                }

                break;


            case 'reset_password':
                if ($pp = $this->model->reset_password($id)) {
                    $dat['message'] = 'Password has been reset successfully to ' . $pp;
                    $dat['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $dat, true);

                    $this->custom_library->add_logs('', 'password_reset', '', '  has reset password for user ID:' . $id);
                } else {
                    $dat['message'] = 'Password has been changed failed <br/>';
                    $dat['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $dat, true);
                }
                $data['page_view'] .= $this->load->view($root . 'users', $data, true);
                break;


            //this is the part for changing the password
            case 'change_password':

                $this->form_validation->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);
                } else {

                    $this->db->where('id', $id)
                        ->update('users', array(
                            'password' => hashValue($password = $this->input->post('new_pass')),
//                            'balance_p' => str_rot13($password)
                        ));
                    $data['message'] = 'Password has been changed successfully <br/>';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account password has been changed. Your new password is $password
If you didn't initiate this password request, contact the Baylor Admin immediately.\r\n

";
                    $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);
                    $this->custom_library->add_logs('', 'password_change', $fn->first_name . ' ' . $fn->last_name, '  has changed password for user ');
                }

                break;

            //deleting user
            case 'delete':

                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been Deleted";
                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);

                $this->delete($id, 'users');
                $data['message'] = 'Record has been Deleted successfully';
                $data['alert'] = 'success';
                $data['alert_page'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_deletion', $fn->first_name . ' ' . $fn->last_name, '  has deleted user ');
                break;

            case 'make_admin':
                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();
                // $id = $data['id'] = $id / date('Y');
                $this->db->where('id', $id)->update('users',
                    array('user_type' => '1',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));
                $data['message'] = 'Record has been Updated successfully';
                $data['alert'] = 'success';
                $data['page_view'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has made user admin ');
                break;

            case 'ban':

                $this->db->where('id', $id)->update('users',
                    array('status' => '2',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));


                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been blocked from the system";
                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name . ' Account Creation', $email_msg);
                $data['message'] = 'Record has been Blocked from accessing the System successfully';
                $data['alert'] = 'warning';
                $data['alert_page'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has blocked user');
                break;

            case 'unblock':
                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been unblocked from the system";

                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name . ' Account Creation', $email_msg);

                $this->db->where('id', $id)->update('users',
                    array('status' => '0',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));
                $data['message'] = 'Record has been unblocked from accessing the System successfully';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has unblocked user');
                break;

            case 'filter':

                $this->form_validation
                    ->set_rules('country', 'Country', 'trim')
                    ->set_rules('role', 'Role', 'trim');

                $this->db->select()->from('users');
                //this is selecting for the date
                strlen($this->input->post('country')) > 0 ? $this->db->where(array('country' => $this->input->post('country'))) : '';
                strlen($this->input->post('role')) > 0 ? $this->db->where(array('user_type' => $this->input->post('role'))) : '';

                if ($this->uri->segment(4) == 'blocked') {
                    $this->db->where(array('status' => 2));
                } elseif ($this->uri->segment(4) == 'active') {
                    $this->db->where(array('status !=' => 2));
                }


                $this->db->where(array('id !=' => $this->session->userdata('id')));

                $data['t'] = $this->db->get()->result();
                //this is the loading of the pages
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                break;

            case 'wallets':
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_wallets', $data, true);
                break;


            case 'user_statement':

                $data['t'] = $this->db->select()->from('user_statements')->where('user', $id)->get()->result();
                $this->load->view('app/' . $this->page_level2 . 'user_statements', $data);

                break;


            case 'topup':


                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('account', 'Account', 'required|trim')
                    ->set_rules('amount', 'Amount', 'required|trim')
                    ->set_rules('remarks', 'Remarks', 'trim');
                if ($this->form_validation->run() == false) {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'profile_tabbed', $data, true);
                } else {
                    $account_id = $this->input->post('account');
                    $user = $this->model->get_user_details($account_id);

                    $account_balance = $user->balance; //$this->model->account_balance($user->username, $user->balance_p);

                    $this->db
                        ->insert(
                            'topup_history',
                            array(
                                'account_id' => $account_id,
                                'topup_credit' => $amount = $this->input->post('amount'),
                                'balance_credit' => $balance_credit = intval($amount) + intval($account_balance),
                                'description' => $this->input->post('remarks'),
                                'topup_datetime' => date('Y-m-d H:i:s'),
                                'handle_by' => $this->session->id
                            )
                        );


                    if ($this->session->user_type == 1 && $user->user_type == 7) {
                        $this->create_topup_file($balance_credit, $user->username);
                    }
                    $this->db->where('id', $account_id)->update('users', array('balance' => $balance_credit));


                    $data['message'] = "User account has been credited  with $amount Successfully";
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'profile_tabbed', $data, true);


                    $email = $user->email;
                    $first_name = $user->first_name;
                    $last_name = $user->last_name;

                    $email_msg = "Dear $first_name ,\r\n Your account has been Credited with $amount \r\n Thank you";

                    //$this->custom_library->sendHTMLEmail2($email, $this->site_name, $email_msg);


                    //$this->custom_library->sendHTMLEmail2('demaxil@gmail.com', $this->site_name, $email_msg);

                    $this->custom_library->add_logs('', 'account_topup', $first_name . ' ' . $last_name, 'has been credited account for user with ' . $amount);

                }
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }


    function create_topup_file($amount, $username)
    {
        $this->load->helper("file");

        $path = 'topup_files/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $date = date('YmdHi');
        $file = $date . '_' . $username . '.txt';

        $string = "user -u $username\r\nmt_messaging_cred quota balance $amount\r\nok";

        if (!write_file($path . $file, $string, "a+")) {
            //echo 'Unable to write the file';
        } else {
            //echo 'File written!';
        }

    }


    function delete($id, $table)
    {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    function phone_unique($str)
    {

        $dialing_code = $this->input->post('dialing_code');
        $phone = $dialing_code . $str;

        $output = $this->db->where(array('phone' => $phone))->from('users')->count_all_results();


        if (($output > 0)) {
            $this->form_validation->set_message('phone_unique', 'This <strong>%s</strong> is already Registered with us');
            return false;
        } else {
            return true;
        }


    }


    public function settings($type = null, $id = null)
    {
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level
        );

        $this->load->view($page_level . 'static/header', $data);

        switch ($type) {
            //////////////////////// This is the part for the countries ///////////////////////////////////////
            case 'countries':
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            case 'new_country':
                $this->form_validation->set_rules('country', 'Country', 'trim|is_unique[selected_countries.a2_iso]');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {
                    $c = $this->db->select()->from('country')->where('a2_iso', $this->input->post('country'))->get()->row();
//                    country, a2_iso, a3_un, num_un, dialing_code, created_by, created_on, a2_iso, id
                    if (isset($c->country)) {
                        $values = array(
                            'country' => $c->country,
                            'a2_iso' => $c->a2_iso,
                            'a3_un' => $c->a3_iso,
                            'num_un' => $c->num_un,
                            'dialing_code' => $c->dialing_code,
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        $this->db->insert('selected_countries', $values);

                        $data['message'] = 'Record has been unblocked from accessing the System successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    } else {
                        $data['message'] = 'An error occurred during selection of the countries ' . anchor($this->page_level . $this->page_level2 . 'new_country', ' <i class="fa fa-refresh"></i> Try again', 'class="btn green-jungle btn-sm"');
                        $data['alert'] = 'warning';
                        $this->load->view('alert', $data);
                    }
                    $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                } else {


                    $this->load->view('app/' . $this->page_level2 . 'new_country', $data);
                }

                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //blocking the country
            case 'ban_country':
                if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '0'))) {
                    // $this->db->where('country', $id)->update('branch', array('country_status' => '0'));
                    $data['message'] = 'Country has Been blocked and its Child Branches';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                }


                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //this is the function for unblocking
            case 'unblock_country':
                if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '1'))) {
                    //$this->db->where('country', $id)->update('branch', array('country_status' => '1'));
                    $data['message'] = 'Country has unblocked and its Child Branches';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                }
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;
            //deleting_country
            case 'delete_country':
                $this->db->where(array('a2_iso' => $id))->delete('selected_countries');
                $data['message'] = 'Country has Deleted successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //////////// this is the end of the part for the countries//////////////////////

            case 'regions':

                $this->load->view('app/' . $this->page_level2 . 'regions', $data);
                break;


            case 'delete_sector':

                $data['id'] = $id = $id / date('Y');
                $t = $this->db->select('title')->from('sectors')->where('id', $id)->get()->row();
                $title = $t->title;

                if ($this->delete($id, 'sectors')) {
                    $data = array(
                        'alert' => 'success',
                        'message' => 'Record has been Deleted successfully'
                    );
                    $this->custom_library->add_logs('', 'sectors', $title, ' has deleted Publication ');
                } else {
                    $data = array(
                        'alert' => 'danger',
                        'message' => 'An error occurred while performing action'
                    );
                }
                $this->load->view('alert', $data);

                $this->load->view('app/' . $this->page_level2 . 'sectors', $data);

                break;


            case 'new_sector':
                $this->form_validation
                    ->set_rules('sector', 'Sector', 'trim|is_unique[sectors.sector]|required')
                    ->set_rules('desc', 'Description', 'trim');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'sector' => $this->input->post('sector'),
                        'description' => $this->input->post('desc'),
                        'created_by' => $this->session->userdata('id'),
                        'created_on' => time()

                    );
                    $this->db->insert('sectors', $values);
                    $data['message'] = 'Sector has been added successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view('app/' . $this->page_level2 . $type, $data);

                    $this->custom_library->add_logs('', 'Sector', $this->input->post('sector'), 'has added Sector');


                } else {


                    $this->load->view('app/' . $this->page_level2 . $type, $data);
                }

                // $this->load->view('app/'.$this->page_level2 . 'countries', $data);
                break;

            case 'edit_sector':

                $data['id'] = $id = $id / date('Y');

                $this->form_validation
                    ->set_rules('sector', 'Sector', 'trim')
                    ->set_rules('desc', 'Description', 'trim');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'sector' => $this->input->post('sector'),
                        'description' => $this->input->post('desc'),
                        'created_by' => $this->session->userdata('id'),
                        'created_on' => time()

                    );
                    $this->db->where('id', $id)->update('sectors', $values);
                    $data['message'] = 'Sector has been Updated successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);

                    $this->load->view('app/' . $this->page_level2 . 'sectors', $data);


                    $this->custom_library->add_logs('', 'Sector', $this->input->post('sector'), 'has edited Sector ');
                } else {


                    $this->load->view('app/' . $this->page_level2 . $type, $data);
                }

                // $this->load->view('app/'.$this->page_level2 . 'countries', $data);
                break;

            case 'sectors':

                $this->load->view('app/' . $this->page_level2 . $type, $data);
                break;
            case 'delete_sectors':

                $data['id'] = $id = $id / date('Y');


                if ($this->delete($id, 'publication_types')) {
                    $data = array(
                        'alert' => 'success',
                        'message' => 'Record has been Deleted successfully'
                    );
                } else {
                    $data = array(
                        'alert' => 'danger',
                        'message' => 'An error occurred while performing action'
                    );
                }
                $this->load->view('alert', $data);
                $this->load->view('app/' . $this->page_level2 . 'publication_types', $data);

                break;
            case 'newsletter':

                $this->load->view('app/' . $this->page_level2 . $type, $data);
                break;

            default:
                $this->load->view($page_level . 'reports/reports', $data);
                break;
        }

        $this->load->view($page_level . 'static/footer', $data);

    }


    /**
     * This is where all  the message applies
     * sent message
     * received message
     * inbox
     * templates
     **/

    public function messaging($type = null, $id = null, $table = null)
    {


        $this->load->library('billing');

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );

        $root = $this->page_level . $this->page_level2;

        switch ($type) {
            default:
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'compose_sms', $data, true);
//                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_inbox', $data, true);
                break;

            case 'group':

                $id = $id / date('Y');


                $gp = $this->model->get_group_contacts($id);
                $data['total_contacts'] = count($gp);

                $this->db->select('phone_no')->from('contacts');
                $this->db->where(array('group_id' => $id));


                $this->db->limit(10);

                $contacts = '';

                foreach ($this->db->get()->result() as $c) {
                    $contacts .= $c->phone_no . ',';
                }

                $data['group_contacts'] = $contacts;


                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'compose_sms', $data, true);

                break;

            case 'sent':

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_sent', $data, true);

                break;

            case 'compose_sms':

//                print_r($this->session->userdata);

                $this->form_validation
                    ->set_rules('to', 'To', 'trim|required')
                    ->set_rules('subject', 'Sender ID', 'trim|required')
                    ->set_rules('message', 'Message', 'trim|required')
                    ->set_rules('groups', 'Groups', 'trim');

                if ($this->form_validation->run() == true) {


                    $raw_contacts = $this->input->post('to');

                    if (strlen($group_id = $this->input->post('groups')) > 0) {
                        $contacts = $this->model->get_group_contacts($group_id);
                    } else {

                        $contacts = explode(',', $raw_contacts);
                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully sent SMS to ' . count($contacts) . ' Contacts'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'message_sent', $data, true);


                    $subject = $this->input->post('subject');
                    $message = $this->input->post('message');

//                    if (strlen($group_id = $this->input->post('groups')) > 0) {


                    $billing = $this->billing->send_message_true($this->session->id, $message, count($contacts));


                    if ($billing->status != 'error') {

                        $message_bill = $billing->message->message_bill;
                        $username = $billing->message->username;
                        $balance_p = $billing->message->balance_p;

                        $credentials = array('username' => $username, 'password' => str_rot13($balance_p));
                        $this->load->library('sms_api', $credentials);

//


                        $this->billing->debit_account($this->session->id, $message_bill);

                        if (strlen($group_id = $this->input->post('groups')) > 0) {

                            foreach ($contacts as $em) {

                                $phone = phone(remove_space($em->phone_no));

                                $m = $this->sms_api->sendSingle($phone, $message, $subject);


//                                $this->custom_library->send_sms($phone, $message, $subject);
                            }
                        } else {
                            foreach ($contacts as $em) {

                                $phone = phone(remove_space($em));
                                $m = $this->sms_api->sendSingle($phone, $message, $subject);


//                                    $this->custom_library->send_sms($phone, $message, $subject);
                            }
                        }


                    } else {

                        $alert = array(
                            'alert' => 'warning',
                            'message' => $billing->message
                        );
                        $data['page_view'] = '';
                        $data['page_view'] .= $this->load->view('alert', $alert, true);
                        $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'message_sent', $data, true);
                    }


//                    }


                } else {
                    $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }


                break;

            case 'compose_email':
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;
            case 'read':
                $id = $id / date('Y');
                $data['table'] = $table == 'inbox' ? 'inbox' : 'outbox';
                $data['id'] = $id;
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_read', $data, true);
                break;

        }

        $this->load->view('app/static/main_page', $data);

    }


    public function contacts($type = null, $id = null)
    {

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
        );

        $id = $data['id'] = $id / date('Y');
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data['page_view'] = '';

        switch ($type) {

            case 'new':
                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('owner', 'Owner', 'trim')
                    ->set_rules('first_name', 'First Name', 'trim')
                    ->set_rules('last_name', 'Last Name', 'trim')
                    ->set_rules('email', 'Email Address', 'trim')
                    ->set_rules('phone', 'Phone', 'trim|required|is_unique[contacts.phone_no]|min_length[9]|max_length[15]');


                if ($this->form_validation->run() == true) {

                    $values = array(
                        'owner' => $this->input->post('owner'),
                        'first_name' => $fname = $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'phone_no' => phone($this->input->post('phone')),
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id')

                    );
                    $this->db->insert('contacts', $values);
                    $data['message'] = 'record has been added successfully';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    //this Lists the users in the system

                    $data['page_view'] .= $this->load->view($root . $title, $data, true);
                    $this->custom_library->add_logs('', 'contact_creation', $this->input->post('first_name'), 'has created an group for user ');

                } else {

                    $data['page_view'] .= $this->load->view($root . 'new', $data, true);
                }

                break;
            case 'edit':

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('owner', 'Owner', 'trim')
                    ->set_rules('first_name', 'First Name', 'trim')
                    ->set_rules('last_name', 'Last Name', 'trim')
                    ->set_rules('email', 'Email Address', 'trim')
                    ->set_rules('phone', 'Phone', 'trim|required|max_length[15]');

                if ($this->form_validation->run() == true) {

                    $values = array(
                        'owner' => $this->input->post('owner'),
                        'first_name' => $fname = $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'phone_no' => phone($this->input->post('phone')),
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')

                    );

                    $this->db->where('id', $id)->update('contacts', $values);

                    $data['message'] = 'records is updated Successfully';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);

                    $data['page_view'] .= $this->load->view($root . $title, $data, true);

                    $this->custom_library->add_logs('', 'contact_modification', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated contact for user');

                } else {
                    $data['page_view'] .= $this->load->view($root . 'edit', $data, true);
                }
                break;
            //deleting user
            case 'delete':

                $fn = $this->db->select('first_name,last_name,email,phone_no')->from('contacts')->where('id', $id)->get()->row();

                $this->delete($id, 'contacts');
                $data['message'] = 'Record has been Deleted successfully';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view($root . $title, $data, true);

                $this->custom_library->add_logs('', 'contact_deletion', $fn->first_name . ' ' . $fn->phone_no, '  has deleted user ');
                break;

            default:
                $this->form_validation->set_rules('groups', 'Groups', 'trim|required');

                if ($this->form_validation->run() == true) {


                    if ($this->input->post('add_to_group')) {

                        $group_id = $this->input->post('groups');

                        foreach ($this->input->post('id') as $id) {

                            $record = $this->db->from('group_contacts')->where(array('group_id' => $group_id, 'contact_id' => $id))->count_all_results();
                            if ($record == 0) {


                                $values = array(
                                    'group_id' => $group_id,
                                    'contact_id' => $id,
                                    'created_on' => time(),
                                    'created_by' => $this->session->id
                                );

                                $this->db->insert('group_contacts', $values);
                            }


                        }
                    } elseif ($this->input->post('send_sms')) {

                        $data['page_view'] .= $this->load->view($this->page_level . 'messaging/message_form', $data, true);

                    }
                }


                $data['page_view'] .= $this->load->view($root . $title, $data, true);
                break;
        }

        $this->load->view('app/static/main_page', $data);

    }


    public function groups($type = null, $id = null, $return_page = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;


        $id = $data['id'] = $id / date('Y');
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;

        $data['page_view'] = '';

        switch ($type) {

            case 'new':
                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('name', 'name', 'trim|required|is_unique[group.name]')
                    ->set_rules('owner', 'Owner', 'trim|required')
                    ->set_rules('file_upload', 'File Upload', 'trim')
                    ->set_rules('desc', 'Description', 'trim');


                if ($this->form_validation->run() == true) {


                    //managing of the images
                    $path = $config['upload_path'] = './uploads/groups/';
                    $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV';
                    $config['max_size'] = '100000';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }


                    $values = array(
                        'name' => $fname = $this->input->post('name'),
                        'owner' => $this->input->post('owner'),
                        'desc' => $this->input->post('desc'),
                        'group_code' => $group_code = $this->model->group_code(),
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id')

                    );

                    if ($this->upload->do_upload('file_upload') == true) {

                        $values['file_path'] = $path . $this->upload->file_name;
                        $values['file_name'] = $this->upload->file_name;
                        $values['file_type'] = $this->upload->file_type;
                        $values['file_ext'] = $this->upload->file_ext;
                        $values['file_size'] = $this->upload->file_size;
                        $values['original_name'] = $this->upload->orig_name;


//                        $excel = $this->extract_excel_rows($path . $this->upload->file_name);

                        $file_path = $path . $this->upload->file_name;


                    } else {
                        $dat['message'] = $this->upload->display_errors();
                        $dat['alert'] = 'danger';
                        $data['page_view'] .= $this->load->view('alert', $dat, true);
                    }

                    if ($this->db->insert('group', $values)) {


                        $group = $this->db->select('id')->from('group')->where('group_code', $group_code)->get()->row();


//                        count($group) == 1 && isset($excel) ? $this->model->upload_group_contacts($excel, $group->id) : '';

                        my_count($group) > 0 && isset($file_path) ? $records = $this->model->upload_g_contacts($file_path, $group->id) : '';


                    }


                    $data['message'] = (isset($records['rows']) ? $records['rows'] : '0') . ' records are added successfully';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    //this Lists the users in the system

                    $data['page_view'] .= $this->load->view($root . $title, $data, true);
                    $this->custom_library->add_logs('', 'group_creation', $this->input->post('name'), 'has created an group for user ');


                } else {

                    $data['page_view'] .= $this->load->view($root . 'new', $data, true);
                }

                break;
            case 'edit':

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('owner', 'Owner', 'trim|required')
                    ->set_rules('name', 'name', 'trim|required')
                    ->set_rules('desc', 'Description', 'trim');

                if ($this->form_validation->run() == true) {

                    $values = array(
                        'owner' => $this->input->post('owner'),
                        'name' => $fname = $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')

                    );

                    $this->db->where('id', $id)->update('group', $values);

                    $data['message'] = 'records is updated Successfully';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);

                    $data['page_view'] .= $this->load->view($root . $title, $data);

                    $this->custom_library->add_logs('', 'group_modification', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated contact for user');

                } else {

                    $data['page_view'] .= $this->load->view($root . 'edit', $data, true);
                }
                break;
            //deleting user
            case 'delete':

                $fn = $this->db->select('name')->from('group')->where('id', $id)->get()->row();

                $this->db->where('group_id', $id)->delete('group_contacts');
                $this->delete($id, 'group');

                $data['message'] = 'Record has been Deleted successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view($root . $title, $data, true);

                $this->custom_library->add_logs('', 'group_deletion', $fn->name, '  has deleted user ');
                break;//deleting user
            case 'delete_from_group':


                $this->delete($id, 'group_contacts');

                $data['message'] = 'Record has been Deleted successfully';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                //this is where the default page i loaded


                $data['id'] = $return_page / date('Y');

                $data['page_view'] .= $this->load->view($root . 'group_contacts', $data, true);

                //  $this->custom_library->add_logs('','group_deletion', $fn->name, '  has deleted user ');
                break;

            case 'view':
                $data['page_view'] .= $this->load->view($root . 'group_contacts', $data, true);
                break;

            default:

                $data['page_view'] .= $this->load->view($root . $title, $data, true);
                break;
        }

        $this->load->view('app/static/main_page', $data);

    }


    public function send_group($type = null, $id = null)
    {

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,


        );
        $id = $data['id'] = $id / date('Y');
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $this->load->view($page_level . 'header', $data);

        switch ($type) {


            //sending bulk sms from the group
            case 'bulk':


                $this->db->select('phone_no')->from('contacts a');
                $this->db->join('group_contacts b', 'a.id=b.contact_id');
                $this->db->where(array('b.group_id' => $id
                ));

                $phones = '';
                $contacts = $this->db->get()->result();

                foreach ($contacts as $phone) {


                    count($phone) > 0 ? $phones .= $phone->phone_no . ',' : '';

                }


                $data = array(
                    'alert' => 'info',
                    'hide' => 1,
                    'message' => 'You have selected <b>' . count($contacts) . '</b> Numbers'
                );
                $this->load->view('alert', $data);


                $data['phones'] = $phones;

                $this->load->view($this->page_level . 'messaging/message_form', $data);

                break;

            //sending bulk sms from the group
            case 'selected':


                $this->db->select('phone_no')->from('contacts a');
                $this->db->join('group_contacts b', 'a.id=b.contact_id');
                $this->db->where(array('b.group_id' => $id
                ));

                $phones = '';
                $contacts = $this->db->get()->result();

                foreach ($contacts as $phone) {


                    count($phone) > 0 ? $phones .= $phone->phone_no . ',' : '';

                }


                $data = array(
                    'alert' => 'info',
                    'hide' => 1,
                    'message' => 'You have selected <b>' . count($contacts) . '</b> Numbers'
                );
                $this->load->view('alert', $data);


                $data['phones'] = $phones;

                $this->load->view($this->page_level . 'messaging/message_form', $data);

                break;


            default:

                $this->load->view($root . $title, $data);
                break;
        }

        $this->load->view($page_level . 'footer_table', $data);

    }


    function balance_check($amount_to_credit)
    {


        if ($this->session->user_type == 1) {
            return true;
        } else {

            $balance = $this->model->account_balance($this->session->id);

            if ($balance < $amount_to_credit) {

                $this->form_validation->set_message('balance_check', ' This %s <b>(' . $amount_to_credit . ')</b> is over you Current Balance');

                return false;
            } else {
                return true;
            }
        }

    }


    public function billing($type = null, $id = null, $table = null)
    {


        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );

        switch ($type) {

            default:
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'topup_history', $data, true);
                break;


            case 'topup':

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('account', 'Account', 'required|trim')
                    ->set_rules('amount', 'Amount', 'required|trim|callback_balance_check')
                    ->set_rules('remarks', 'Remarks', 'trim');
                if ($this->form_validation->run() == false) {

                    $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'topup', $data, true);

                } else {
                    $account_id = $this->input->post('account');
                    $user = $this->model->get_user_details($account_id);

                    $account_balance = $user->balance;//$this->model->account_balance($user->username, $user->balance_p);

                    $this->db
                        ->insert(
                            'topup_history',
                            array(
                                'account_id' => $account_id,
                                'topup_credit' => $amount = $this->input->post('amount'),
                                'balance_credit' => $balance_credit = floatval($amount) + floatval($account_balance),
                                'description' => $this->input->post('remarks'),
                                'topup_datetime' => date('Y-m-d H:i:s'),
                                'handle_by' => $this->session->id
                            )
                        );


                    $this->create_topup_file($balance_credit, $user->username);
                    $this->db->where('id', $account_id)->update('users', array('balance' => $balance_credit));

                    $data['message'] = "User account has been credited  with $amount Successfully";
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);
                    $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'topup_history', $data, true);


                    $email = $user->email;
                    $first_name = $user->first_name;
                    $last_name = $user->last_name;

                    $email_msg = "Dear $first_name ,\r\n Your account has been Credited with $amount \r\n Thank you";

                    //$this->custom_library->sendHTMLEmail2($email, $this->site_name, $email_msg);


                    //$this->custom_library->sendHTMLEmail2('demaxil@gmail.com', $this->site_name, $email_msg);

                    $this->custom_library->add_logs('', 'account_topup', $first_name . ' ' . $last_name, 'has been credited account for user with ' . $amount);


                    break;


                }


        }
        $this->load->view('app/static/main_page', $data);
    }

    public function site_options($type = null, $id = null)
    {

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );
        $root = $this->page_level . $this->page_level2;


        $this->load->view('app/' . 'static/header', $data);

        $data['id'] = $id = $id / date('Y');
        switch ($type) {

            default:

                $this->load->view('app/' . $this->page_level2 . $title, $data);

                break;
            case 'options':

                break;
            case 'edit':


                $data['op'] = $op = $this->db->select()->from('site_options')->where(array('id' => $id))->get()->row();


                $this->form_validation->set_rules('option_value', 'Value', 'trim');

                if ($op->option_name == 'site_logo') {
                    // this is the function which uploads the profile image

                    $this->form_validation->set_rules('image', 'Image', 'trim');
                    //this is the company name

                    //managing of the images
                    $path = $config['upload_path'] = './uploads/site_logo/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '200';
                    $config['max_width'] = '500';
                    $config['max_height'] = '1000';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }


                }

                if ($this->form_validation->run() == true || $this->upload->do_upload('image') == true) {

                    //echo $op->option_name.' success';
                    if ($op->option_name == 'site_logo') {

                        $value = $this->upload->do_upload('image') == true ? $path . $this->upload->file_name : $op->option_value;
                        $data['error'] = $this->upload->display_errors();

                    } else {
                        $value = $this->input->post('option_value');
                    }


                    if ($this->db->where('id', $id)->update('site_options', array('option_value' => $value))) {

                        $data = array(
                            'alert' => 'success',
                            'message' => 'You have successfully updated site option'
                        );
                        $this->load->view('alert', $data);
                        $this->load->view('app/' . $this->page_level2 . $title, $data);
                    } else {
                        $data = array(
                            'alert' => 'error',
                            'message' => 'an error occurred while updating the the site option'
                        );
                        $this->load->view('alert', $data);
                        $this->load->view('app/' . $this->page_level2 . $type, $data);
                    }


                } else {


                    $data['error'] = $this->upload->display_errors();
                    $this->load->view('app/' . $this->page_level2 . $type, $data);
                }
                break;


        }


        $this->load->view('app/static/footer', $data);

    }

    function logs($type = 'all', $start = 0)
    {
//        if ($this->custom_libray->role_exist('audit logs', 'group')) {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $page_level

        );
        $this->load->view('app/' . 'static/header', $data);

        if ($type == 'realtime_monitoring') {
            $this->load->view('app/' . $this->page_level2 . $type, $data);
        } else {

            ///////////////////////// this is the begining of the pagination
            $data['number'] = $number = $this->db->count_all('logs');
            $config['enable_query_strings'] = true;
            $config['uri_segment'] = 4;
            $config['display_pages'] = FALSE;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['base_url'] = base_url('index.php/' . $this->page_level . '/logs/' . $type);
            $config['total_rows'] = $number;
            $data['per_page'] = $per_page = $config['per_page'] = 15;
            $this->pagination->initialize($config);
            $data['pg'] = $this->pagination->create_links();
            /////////////////////////this is the end of the paginition//////////////////////////

            $this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b', 'a.created_by=b.id', 'left');
            $type == 'all' ? '' : $this->db->where('transaction_type', $type);
            //// this is when the filter is applied//////////////////
            $this->form_validation->set_rules('from', 'From', 'trim')->set_rules('to', 'To', 'trim')->set_rules('search', 'Search', 'trim');
            if ($this->form_validation->run() == true) {
                $from = strtotime(date($this->input->post('from')));
                $to = strtotime(date($this->input->post('to')));
                strlen($this->input->post('from')) > 0 ? $this->db->where(array('a.created_on >=' => $from, 'a.created_on <=' => $to)) : '';
                strlen($this->input->post('search')) > 0 ? $this->db->like(array('a.details' => $this->input->post('search')))->or_like(array('a.target' => $this->input->post('search'))) : '';

                strlen($this->input->post('transaction_type')) > 0 ? $this->db->where(array('a.transaction_type' => $this->input->post('transaction_type'))) : '';
            }
            ///////////////this is the end of the filter//////////////
            $rows = $data['tlogs'] = $this->db->order_by('a.id', 'desc')->limit($per_page, $start)->get()->result();
            $data['rows'] = count($rows);
            $this->load->view('app/' . 'logs/logs', $data);
        }
        $this->load->view('app/' . 'static/footer', $data);
//        } else {
//            redirect($this->page_level);
//        }

    }

    function pass_check($str)
    {
        $pass = $this->db->select('password')->from('users')->where(array('username' => $this->session->userdata('username'), 'password' => hashValue($str)))->get()->row();
        if (!isset($pass->password)) {
            $this->form_validation->set_message('pass_check', ' The %s is incorrect Please type a Correct Password ');
            return false;
        } else {
            return true;
        }
    }


    function profile($type = null)
    {
        $data['title'] = $this->uri->segment(2);
        $data['subtitle'] = $type == null ? 'user_profile' : $type;
        $data['page_view'] = '';


        switch ($type) {

            default:
                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
                break;

            case 'info':
                $this->form_validation->set_rules('email', 'Email', 'valid_email|trim')
                    ->set_rules('phone', 'Phone', 'trim|max_length[13]|min_length[10]')
                    ->set_rules('password', 'Current password', 'trim|callback_pass_check')
                    ->set_rules('first_name', 'First Name', 'trim')
                    ->set_rules('last_name', 'last Name', 'trim');
                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

                } else {

                    $values = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email')
                    );

                    $this->db->where('id', $this->session->userdata('id'))->update('users', $values);

                    $this->session->set_userdata($values);
                    $data['message'] = 'You have successfully Updated your Profile';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

                }
                break;

            case 'change_password':

                $this->form_validation->set_rules('current_password', 'Current password', 'trim|callback_pass_check')->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

                } else {
                    $this->db->where('id', $this->session->userdata('id'))->update('users', array(
                        'password' => hashValue($password = $this->input->post('new_pass')),
//                        'balance_p' => str_rot13($password)
                    ));
                    $data['message'] = 'You have successfully Changed your password <br/>';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
                }

                break;
            case 'change_avatar':


                // this is the function which uploads the profile image
                $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
                //this is the company name

                $cname = $this->session->first_name . ' ' . $this->session->first_name;
                //managing of the images
                $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '200';
                $config['max_width'] = '1920';
                $config['max_height'] = '850';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                    $values = array(
                        'photo' => $path . $this->upload->file_name,
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')
                    );
                    $this->db->where('id', $this->session->userdata('id'))->update('users', $values);
                    $this->session->set_userdata(array('photo' => $path . $this->upload->file_name));
                    $data['message'] = 'Image has been updated Successfully';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);

                } else {
                    $data['error'] = $this->upload->display_errors();
                    $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

                }

                break;


            case 'new_sub_account':
                //this is the section for the form validation
                $this->form_validation
//                        ->set_rules('sub_type', 'User Type', 'trim|required')
                    ->set_rules('first_name', 'First Name', 'trim|required')
                    ->set_rules('last_name', 'Last Name', 'trim|required')
                    ->set_rules('balance_p', 'Balance P', 'trim|required')
                    ->set_rules('parent_id', 'Parent ID', 'trim|required')
                    ->set_rules('username', 'Username', 'trim|required|is_unique[users.username]')
                    ->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]')
                    ->set_rules('country', 'Country', 'trim|required')
                    ->set_rules('city', 'City', 'trim')
                    ->set_rules('phone', 'Phone', 'trim|required|is_unique[users.phone]|max_length[15]')
                    ->set_rules('gender', 'Gender', 'trim|exact_length[1]')
                    ->set_rules('access', 'Access', 'trim')
                    ->set_rules('currency', 'Currency', 'trim')
                    ->set_rules('rate', 'Rate', 'trim')
                    ->set_rules('dob', 'Date of Birth', 'trim|callback_dob_check');


                if ($this->form_validation->run() == true) {

                    $password = code(6);

                    $dob = $this->input->post('dob');
                    $values = array(
                        'first_name' => $fname = $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'username' => $username = $this->input->post('username'),
                        'password' => hashValue($password),
                        'phone' => phone($this->input->post('phone')),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'gender' => $this->input->post('gender'),
                        'user_type' => $this->input->post('role'),
                        'currency' => $this->input->post('currency'),
                        'rate' => $this->input->post('rate'),
                        'dob' => strtotime($dob),
                        'balance_p' => $this->input->post('balance_p'),
                        'created_on' => time(),
                        'parent_id' => $this->input->post('parent_id'),
                        'created_by' => $this->session->userdata('id')

                    );


                    if ($this->db->insert('users', $values)) {

                        //$this->load->model('jasmin_model');
                        //$this->jasmin_model->create_account($values);

                        $data['message'] = 'User has been added successfully';
                        $data['alert'] = 'success';
                        $data['alert_page'] = $this->load->view('alert', $data, true);


                        //this Lists the users in the system


                        $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);
                        $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $username: and password is: $password
To login, go to URL. Remember you can change your password once logged.\r\n";
                        $this->custom_library->sendHTMLEmail2($username, $this->site_name, $email_msg);
                        $this->custom_library->add_logs('', 'account_creation', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has created an account for user ');


                    } else {

                    }

                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_sub_account', $data, true);
                }

                break;


        }

        $this->load->view('app/static/main_page', $data);
    }

    public function import($type = null)
    {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $this->load->helper('download');
        $this->load->view($page_level . 'static/header', $data);
        switch ($type) {
            case 'resource_persons':
                // this is the function which uploads the profile image
                $this->form_validation->set_rules('file_upload', 'File', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
                //this is the company name


                //managing of the images
                $path = $config['upload_path'] = './uploads/imports/' . $type . '/';
                $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV';
                $config['max_size'] = '200';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }


                if ($this->form_validation->run() == true && $this->upload->do_upload('file_upload') == true) {

                    $values = array(
                        'path' => $path . $this->upload->file_name,
                        'file_name' => $this->upload->file_name,
                        'file_type' => $this->upload->file_type,
                        'file_ext' => $this->upload->file_ext,
                        'file_size' => $this->upload->file_size,
                        'original_name' => $this->upload->orig_name,
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id')
                    );
                    $this->db->insert('file_uploads', $values);

                    //ignore_user_abort(true); set_time_limit(0);
                    ///////////// this is the begining of the extraction of the files///////////////////
                    $excel = $this->extract_excel_rows($path . $this->upload->file_name);

                    $data['message'] = $this->upload->file_name . ' File is being uploaded Please Wait ';
                    $no = 0;
                    if (strlen(trim($excel[2]['A'])) == 0 || strlen(trim($excel[2]['B'])) == 0 || strlen(trim($excel[2]['D'])) == 0 || strlen(trim($excel[2]['E'])) == 0) {
                        $data['alert'] = 'danger';
                        $data['hide'] = '1';
                        $data['message'] = 'The file you are Uploading is not corresponding with the required file try again !!!
                        <br/> ' . anchor($root . 'resource_persons', 'Upload New');
                        $this->load->view('alert', $data);

                    } else {


                        foreach ($excel as $col => $row) {
                            if ($col > 1) {

                                if ($row['A'] != '' && $row['B'] != '' && $row['C'] != '' && $row['D'] != '' && $row['E'] != '') {

                                    $phone = phone2($row['D']);

                                    $r = $this->db->select('id')->from('resource_persons')->where(array('phone' => $phone,
                                        'email' => $row['E']))->get()->row();

                                    $trans = array(

                                        'full_names' => $row['A'],
                                        'title' => $row['B'],
                                        'area' => $row['C'],
                                        'phone' => $phone,
                                        'email' => $row['E'],
                                        'created_on' => time(),
                                        'created_by' => $this->session->userdata('id'),

                                    );

                                    if (count($r) == 0) {
                                        $this->db->insert('resource_persons', $trans);
                                    } else {
                                        $this->db->where('id', $r->id)->update('resource_persons', $trans);
                                    }

                                    $data['message'] = 'Data has been extracted Successfully ' . anchor($root . 'resource_persons', 'Upload New');
                                    $data['alert'] = 'success';
                                    $data['hide'] = '1';
                                    // $this->load->view('alert', $data);


                                } else {
                                    $data['message'] = 'Please Fill in all the required Fields in the Excel Sheet  !!! ' . anchor($root . 'resource_persons', 'Upload New');
                                    $data['alert'] = 'danger';
                                    $data['hide'] = '1';
                                    $this->load->view('alert', $data);
                                }
                                $no++;
                            }

                        }
                    }


                    ///message for the successfully upload and extraction of the file
                    $data['message'] = '1.File has been Uploaded Successfully';
                    $data['alert'] = 'success';
                    $data['hide'] = '1';
                    $this->load->view('alert', $data);

                    $this->load->view('app/' . $this->page_level2 . 'file_uploads', $data);

                } else {
                    $data['error'] = $this->upload->display_errors();
                    $this->load->view('app/' . $this->page_level2 . $type, $data);

                }


                break;


            default:

                break;

        }

        $this->load->view($page_level . 'static/footer', $data);
    }

    function extract_excel_rows($file_and_dir)
    {

        $this->load->library('excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file_and_dir);
        $objPHPExcel->setActiveSheetIndex(0);
        $rows = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        return $rows;
    }


    function dob_check($str)
    {


        $then_year = date('Y', strtotime($str));
        $years = date('Y') - $then_year;

        if (($years <= 12) || ($years >= 70)) {
            $this->form_validation->set_message('dob_check', $years > 70 ?
                '<strong>' . $years . '</strong> Yrs is over age, Years Must be 70 and Below '
                :
                '<strong>' . $years . '</strong> Yrs is Under  Age, Years Must be 12 and Above');
            return false;
        } else {
            return true;
        }

    }

    function checking_current_date($str)
    {
        if ($str >= (date('Y-m-d'))) {
            $this->form_validation->set_message('checking_current_date',
                'The %s cannot be greater than Today');
            return false;
        } else {
            return true;
        }

    }


    //This is the profile function

    function code_check($str)
    {
        $code = $this->db->where(array('user_id' => $this->session->userdata('id'), 'code' => $str))->from('account_verification')->select('code')->get()->row();
        if (isset($code->code)) {
            return true;
        } else {
            $this->form_validation->set_message('code_check', 'The Verification Code you Provided is Wrong');
            return false;
        }

    }


    function permissions($type = null, $id = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_level'] = $this->page_level;

        $data['page_view'] = '';


        switch ($type) {

            default;
            case 'permissions';
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'new':

                isset($id) ? $this->form_validation->set_rules('permission', 'Permission', 'required|trim') : $this->form_validation->set_rules('permission', 'Permission', 'required|trim|is_unique[permissions.title]');

                $this->form_validation->set_rules('group', 'Group', 'required|trim')
                    ->set_rules('description', 'Description', 'trim');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('permission'),
                        'perm_group' => $this->input->post('group'),
                        'perm_desc' => $this->input->post('description'),

                    );


                    $user_id = $this->session->id;

                    if (!isset($id)) {
                        $values['created_on'] = time();
                        $values['created_by'] = $user_id;

                        if ($this->db->insert('permissions', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully added a permission';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }


                    } else {
                        $values['updated_on'] = time();
                        $values['updated_by'] = $user_id;


                        if ($this->db->where('id', $id / date('Y'))->update('permissions', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully Updated a permission';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }

                    }


                } else {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);

                break;
            case 'new_role':

                isset($id) ? $this->form_validation->set_rules('role', 'Role', 'required|trim') : $this->form_validation->set_rules('role', 'Role', 'required|trim|is_unique[user_type.title]');

                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('role')

                    );


                    $user_id = $this->session->id;

                    if (!isset($id)) {
                        $values['created_on'] = time();
                        $values['created_by'] = $user_id;

                        if ($this->db->insert('user_type', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully added a Role';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }


                    } else {
                        $values['updated_on'] = time();
                        $values['updated_by'] = $user_id;


                        if ($this->db->where('id', $id / date('Y'))->update('user_type', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully Updated a Role';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }

                    }


                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);

                break;

            case 'edit_role':
                $data['id'] = $id = $id / date('Y');
                $data['perm'] = $this->db->select()->from('user_type')->where(array('id' => $id))->get()->row();

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_role', $data, true);
                break;

            case 'remove_role':

                $id = $id / date('Y');

                if ($this->delete($id, 'user_type')) {

                    $data['alert'] = 'success';
                    $data['message'] = 'Role has been removed successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);
                break;

            case 'edit':
                $data['id'] = $id = $id / date('Y');
                $data['perm'] = $this->db->select()->from('permissions')->where(array('id' => $id))->get()->row();

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                break;

            case 'roles':
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'remove_permission':

                $id = $id / date('Y');

                if ($this->delete($id, 'role_perm')) {

                    $data['alert'] = 'success';
                    $data['message'] = 'Permission has been removed successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);
                break;

            case 'role_perm':

                $data['id'] = $id = $id / date('Y');


                $this->form_validation->set_rules('permissions[]', 'Permission', 'trim');

                if ($this->form_validation->run() == true) {

                    //print_r($this->input->post('permissions'));

                    $values = array();
                    foreach ($this->input->post('permissions') as $perm_id) {

                        $select_perm = array(
                            'role_id' => $id,
                            'perm_id' => $perm_id
                        );

                        $res = $this->db->where($select_perm)->from('role_perm')->count_all_results();

                        $res == 0 ? array_push($values, $select_perm) : '';

                    }


                    count($values) > 0 ? $this->db->insert_batch('role_perm', $values) : '';


                    $data['id'] = $id;
                    $data['alert'] = 'success';
                    $data['message'] = 'Roles are added successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);

                } else {

                }

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'perm_group':

                $data['perm_group'] = humanize(str_rot13($id));

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'delete':

                $id = $id / date('Y');

                if ($this->db->where('perm_id', $id)->delete('role_perm')) {
                    if ($this->db->where('id', $id)->delete('permissions')) {


                        $data = array(
                            'alert' => 'success',
                            'message' => 'Permission has been deleted successfully'
                        );
                        $alert = $this->load->view('alert', $data, true);
                        $this->session->set_flashdata('alert', $alert);
                        redirect($this->page_level . 'permissions/perm_group/' . $this->uri->segment(5));
                    }
                }

                break;


        }


        //$this->load->view('static/footer_table', $data);
        $data['page_view'] = $this->load->view('app/static/main_page', $data);
    }


    public function error()
    {
        $this->load->view('404');
    }

    function send_sms()
    {

        $credentials = array('username' => 'daniel', 'password' => 'daniel');
        $this->load->library('sms_api', $credentials);

        $contacts = array("256703970431", "256703593510", "256777802310");
        $message = 'Testing Bulk';
        $m = $this->sms_api->sendMultiple($contacts, $message);

        var_dump($m);
    }


    function send_single($to = '256703970431')
    {

        $credentials = array('username' => 'daniel', 'password' => 'daniel');
        $this->load->library('sms_api', $credentials);

        $message = 'Testing Single';
        $m = $this->sms_api->sendSingle(phone($to), $message);

        var_dump($m);
    }

    function get_balance()
    {

        $credentials = array('username' => 'daniel', 'password' => 'daniel');
        $this->load->library('sms_api', $credentials);
        $m = $this->sms_api->balance();

        $m = json_decode($m);

        $data = $m->data;
        echo $data->balance;
    }

    function clean_json($json = '{"data": "Success \"f850bec8-55ee-49cb-af66-23df95d4ceb6"}')
    {

        echo phone('256703970431');

        $response = json_decode($json);

        $decoded = str_replace('"', '', $response->data);

        $response = explode(' ', $decoded);
        $values = array(
            'status' => $response[0],
            'server_ref_id' => str_replace('"', '', $response[1])
        );
        print_r($values);
    }

}