<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 27/12/2017
 * Time: 18:03
 */

class auto_create_table
{

    private $CI;
    private $inbox_table;
    private $out_table;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->inbox_table = 'inbox_' . date('dmY');
        $this->out_table = 'outbox_' . date('dmY');

    }
    function create_inbox_table($table=null)
    {

        $this->CI->load->dbforge();


        $this->CI->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
            ),
            'server_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'id_smsc' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'client_ref_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE,
                'comment' => 'client_ref(tags)'
            ),
            'server_ref_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE,
            ),
            'phone' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => TRUE,
            ),
            'network' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
            ),
            'content' => array(
                'type' => 'TEXT',
            ),
            'content_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 1,
            ),
            'from' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
            ),


            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => 40,
                'null' => TRUE,
                'comment' => 'First status while sending message to server'
            ),


            'message_status' => array(
                'type' => 'VARCHAR',
                'constraint' => 40,
                'null' => TRUE,
                'comment' => 'Status coming from the server after being delivered'
            ),
            'dlr_level' => array(
                'type' => 'INT',
                'constraint' => 5,
                'null' => TRUE,
                'comment' => '(default = 3)'
            ),
            'dlr_method' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null' => TRUE,
                'comment' => '(default = get)'
            ),
            'subdate' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
                'comment' => 'Date & time format: YYMMDDhhm'
            ),
            'donedate' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
                'comment' => 'Date & time format: YYMMDDhhm'
            ),
            'sub' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'dlvrd' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),

            'err' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'text' => array(
                'type' => 'text',
                'comment' => 'this is the message'
            ),

            'ip' => array(
                'type' => 'VARCHAR',
                'constraint' => 11,
                'null' => TRUE,
            ),

            'dlr' => array(
                'type' => 'VARCHAR',
                'constraint' => 11,
                'null' => TRUE,
                'comment' => '(default =yes)'
            ),
            'dlr_url' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => TRUE,
                'comment' => '(your delivery report API)'
            ),
            'full_reply_url' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => TRUE,
                'comment' => '(your delivery report API)'
            ),
            'full_reply_url_time' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'created_on' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'updated_on' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
            )
        ));
        $this->CI->dbforge->add_key('id', TRUE);
        $this->CI->dbforge->add_key('server_id');
        $this->CI->dbforge->add_key('client_ref_id');
        $this->CI->dbforge->add_key('server_ref_id');
        $this->CI->dbforge->add_key('username');
        $this->CI->dbforge->add_key('network');
        $this->CI->dbforge->add_key('phone');

        isset($table)?$this->CI->dbforge->create_table($table,TRUE):$this->CI->dbforge->create_table($this->inbox_table, TRUE);

    }

}