<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php

class custom_library{
    private $CI;
    private $product_id = 2;
    private $site_name='';
    private $site_email='';

    public $inbox_table;
    public $out_table;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->library('site_options');

        $this->site_name=$this->CI->site_options->title('site_name');
        $this->site_email=$this->CI->site_options->title('contact_email');

        $this->inbox_table = 'inbox_' . date('dmY');
        $this->out_table = 'outbox_' . date('dmY');
    }


    function dennis(){
        //$this->CI
        $users=$this->CI->db->select()->from('users')->get()->result();
        return($users);


    }


    function send_sms($phone, $sms,$subject=null,$client_ref_id=null)
        {
            if (!empty($phone)) {
                if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                    if (strstr($phone, ',')) {
                        $phone = substr($phone, 0, strpos($phone, ','));
                    }
                    if (strstr($phone, '/')) {
                        $phone = substr($phone, 0, strpos($phone, '/'));
                    }
                    if (strstr($phone, '-')) {
                        $phone = substr($phone, 0, strpos($phone, '-'));
                    }
                    $phone = trim($phone);
                }
                if (strlen($phone) >= 9) {
                    if (substr($phone, 0, 1) == '0') {
                        if (strlen($phone) == 10) {
                            $phone = '250' . substr($phone, 1);
                        }
                    }
                    if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                        $phone = '250' . $phone;
                    }
                    if (substr($phone, 0, 1) == '+') {
                        $phone = substr($phone, 1);
                    }
//                    $q = $this->CI->db->select('id')->from('outbox')->where(array('to_user' => $phone, 'message' => $sms))->limit(1)->get()->result();
//                    if (!isset($q[0]->id)) {


                        if($this->CI->session->user_type==7){

                            $username=$this->CI->session->username;
                            $password=str_rot13($this->CI->session->balance_p);

                        }else{

                            $username='daniel';
                            $password='daniel';

                        }
                       $subject=isset($subject)?$subject:'INFOSMS';



                        $url_encoded_sms = urlencode($sms);
                        
                        $url_to_reply = base_url('send/send_client_reply/');


                        $url = "http://127.0.0.1:1401/send?username=$username&password=$password&to=$phone&content=$url_encoded_sms&from=$subject";
                        $url .= "&dlr=yes&dlr-level=2&dlr-url=$url_to_reply&dlr-method=GET";
                        $socket = url_get_contents($url);

                        if ($socket) {

                            //$this->CI->db->insert('outbox','to_user,subject,m_type,message,created_on,created_by',"'$phone','Notification','SMS','$sms','".time()."','0'");

                            $this->CI->db->insert('outbox', array('to_user' => $phone, 'subject' => $subject, 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => $this->CI->session->id));

//                            fclose($socket);
                        }
//                    }
                }
            }
        }


    public function stringNumbers($x)
        {
            $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven",
                "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
                "nineteen", "twenty", 30 => "thirty", 40 => "forty",
                50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
                90 => "ninety");


            if (!is_numeric($x))
                $w = '#';
            else if (fmod($x, 1) != 0)
                $w = '#';
            else {
                if ($x < 0) {
                    $w = 'minus ';
                    $x = -$x;
                }
                else
                    $w = '';
                // ... now $x is a non-negative integer.

                if ($x < 21)   // 0 to 20
                    $w .= $nwords[$x];
                else if ($x < 100) {   // 21 to 99
                    $w .= $nwords[10 * floor($x / 10)];
                    $r = fmod($x, 10);
                    if ($r > 0)
                        $w .= ' ' . $nwords[$r];
                }
                else if ($x < 1000) {   // 100 to 999
                    $w .= $nwords[floor($x / 100)] . ' hundred';
                    $r = fmod($x, 100);
                    if ($r > 0) {
                        $w .= ' and ';
                        if ($r < 100) {   // 21 to 99
                            $w .= $nwords[10 * floor($r / 10)];
                            $r = fmod($r, 10);
                            if ($r > 0)
                                $w .= ' ' . $nwords[$r];
                        }
                    }
                }
                else if ($x < 1000000) {   // 1000 to 999999
                    $w .= $nwords[floor($x / 1000)] . ' thousand';
                    $r = fmod($x, 1000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= $nwords[$r];
                    }
                }
                else {    //  millions
                    $w .= stringNumbers(floor($x / 1000000)) . ' million';
                    $r = fmod($x, 1000000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= int_to_words($r);
                    }
                }
            }
            //echo $w;

            return $w;


        }



    function site_visits($ip=null){


       $ip=isset($ip)?$ip:$_SERVER["REMOTE_ADDR"];

       $cp=$this->CI->db->select('statusCode,ipAddress,countryCode,countryName,regionName,cityName,zipCode,latitude,longitude,timeZone')->from('site_visit')->where('ipAddress',$ip)->get()->row();
        if(count($cp)==1){

           // statusCode,ipAddress,countryCode,countryName,regionName,cityName,zipCode,latitude,longitude,timeZone
//            $xml=array(
//                'statusCode'=>$cp->statusCode,
//                'ipAddress'=>$cp->ipAddress,
//                'countryCode'=>$cp->countryCode,
//                'countryName'=>$cp->countryName,
//                'regionName'=>$cp->regionName,
//                'cityName'=>$cp->cityName,
//                'zipCode'=>$cp->zipCode,
//                'latitude'=>$cp->latitude,
//                'longitude'=>$cp->longitude,
//                'timeZone'=>$cp->timeZone
//            );

            $xml=$cp;
        }else{
             $xml=simplexml_load_file("http://api.ipinfodb.com/v3/ip-city/?key=1df5a82c001338901a85b18f75576ce3874c14c23b5c547f3e208e63883413bf&ip=$ip&format=xml");
        }

        $this->CI->db->insert('site_visit',$xml);

     // return $xml;
    }




    function get_enum_values($table, $field)
    {
        $type = $this->CI->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }



    function sendHTMLEmail2($to, $subject, $message)
    {


        $mailto = $to;
        // $file="thanks.htm";
        $pcount = 0;
        $gcount = 0;
        $subject = $subject;
        $b = time();
        $pstr = $message;//$this->email_template($message);
        $gstr = $message;//$this->email_template($message);



        $headers = sprintf("From: $this->site_name<$this->site_email>\r\n");
        $headers .= sprintf("MIME-Version: 1.0\r\n");
        $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");

        /////////////
        if ($this->CI->input->method() == 'post') {
            $pcount=count($_POST);
        }
        if ($this->CI->input->method() == 'get') {
            $pcount=count($_GET);
        }
        ////////////
        ///
        if ($pcount > $gcount) {
            $message_body = $message;//$pstr;
            // $message_body = $pstr;
            mail($mailto, $subject, $message_body, $headers);
            //$send = @mail($to, $subject, $body, $headers);

            $this->CI->db->insert('outbox', array('to_user' => $mailto, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message_body, 'created_on' => time()));
            //  include("$file");
            return true;
        } else {
            $message_body = $gstr;

            if (!mail($mailto, $subject, $message_body, $headers)) {
                //die ("Not sent");
                return false;
            } else {
                // include("$file");
                // print $b;
                return true;
            }
        }

    }

    function email_template($body)
        {
            $logo = 'http://citiexpress.net/Ariane/wp-content/uploads/2016/05/Coat_of_arms_of_the_Republic_of_Uganda.svg.png';
            $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Government Evaluation Facility | Email</title>
				<style type="text/css">
				html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
				@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
					*[class="table_width_100"] {
						width: 96% !important;
					}
					*[class="border-right_mob"] {
						border-right: 1px solid #dddddd;
					}
					*[class="mob_100"] {
						width: 100% !important;
					}
					*[class="mob_center"] {
						text-align: center !important;
						padding: 0 !important;
					}
					*[class="mob_center_bl"] {
						float: none !important;
						display: block !important;
						margin: 0px auto;
					}
					.iage_footer a {
						text-decoration: none;
						color: #929ca8;
					}
					img.mob_display_none {
						width: 0px !important;
						height: 0px !important;
						display: none !important;
					}
					img.mob_width_50 {
						width: 40% !important;
						height: auto !important;
					}
					img.mob_width_80 {
						width: 80% !important;
						height: auto !important;
					}
					img.mob_width_80_center {
						width: 80% !important;
						height: auto !important;
						margin: 0px auto;
					}
					.img_margin_bottom {
						font-size: 0;
						height: 25px;
						line-height: 25px;
					}
				}
				.table_width_100 {
					width: 680px;
				}
				</style>
				</head>

				<body style="padding: 0px; margin: 0px;">
				<div id="mailsub" class="notification" align="center">

				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


				<!--[if gte mso 10]>
				<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr><td>
				<![endif]-->

				<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px;min-width: 300px;width: 680px;">
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="180" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="' . $logo . '" alt="MixaKids" border="0" style="display: block;height:100px"></font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
									<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl" style="width: 88px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td width="30" align="center" style="line-height: 19px;">
															<a href="https://www.facebook.com/opmuganda/" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/facebook.gif" width="10" height="19" alt="Follow us on Facebook" border="0" style="display: block;"></font></a>
														</td><td width="39" align="center" style="line-height: 19px;">
															<a href="https://www.twitter.com/mixakids" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/twitter.gif" width="19" height="16" alt="Follow us on Twitter" border="0" style="display: block;"></font></a>
														</td><!--<td width="29" align="right" style="line-height: 19px;">
															<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;"></font></a>
														</td>--></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header END-->

					<!--content 1 -->
					<tr><td align="center" bgcolor="#f8f8f8">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
								<font face="Arial, Helvetica, sans-serif" size="4" color="#333" style="font-size: 15px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #333;">
										' . $body . '
									</span>
								</font>
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>
					<!--content 1 END-->
					<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
						<div style="line-height: 22px;">
							<font face="Arial, Helvetica, sans-serif" size="5" color="#6b6b6b" style="font-size: 20px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #6b6b6b;">
									Thank you for Subscribing with <strong>Governmnent evaluation Facility</strong>
								</span>
							</font>
						</div>
					<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>

					<!--footer -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 15px; line-height: 15px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="115" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="http://www.opm.go.ug" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="src="' . $logo . '"" alt="Government Evaluation Facility" border="0" style="display: block;width:100%" /><br>
													Support Team</font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 150px;">
									<table class="mob_center" width="150" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															FAQ</font></a>
														</td><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Blog</font></a>
														</td><td align="right" style="line-height: 19px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Contact</font></a>
														</td></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--footer END-->
					<tr>
						<td>
							<p>&nbsp;</p>
							<p style="background:#e9e9e9;color:#333"><small>This email was sent automatically by <a href="http://opm.go.ug/">opm.go.ug/</a>. Please, do not reply</small></p>
						</td>
					</tr>
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
				</table>
				<!--[if gte mso 10]>
				</td></tr>
				</table>
				<![endif]-->

				</td></tr>
				</table>

				</div>
				</body>
		</html>';
            return $html;
        }


    function sendHTMLEmail($to, $subject, $message)
        {
            $headers = sprintf("From: $this->site_name<$this->site_email>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Bcc: tamaledns@gmail.com\r\n";
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");
            $send = @mail($to, $subject, $message, $headers);
            if ($send) {
                return true;
            }
            else {
                return false;
            }
        }




    function sendMail($to, $subject, $message,$attachment=null,$cc=null,$bcc=null){

        //$this->CI->load->library('email');
        //$this->CI->load->helper('email');

        $config['protocol'] = 'smtp';
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = $this->CI->site_options->title('site_name');
        $config['smtp_host'] = 'www.wealthmoneytransfer.com';
        $config['smtp_user'] = 'no-reply@wealthmoneytransfer.com';
        $config['smtp_pass'] = 'svuN82^6';
        $this->CI->email->initialize($config);

        if(valid_email($to)) {

            $this->CI->email->clear(TRUE);
            $this->CI->email->from('no-reply@wealthmoneytransfer.com',  $this->CI->site_options->title('site_name'));
            $this->CI->email->to($to);
            isset($cc) && valid_email($cc)?$this->CI->email->cc($cc):'';
            isset($bcc) && valid_email($bcc)?$this->CI->email->bcc($bcc):'';

            $this->CI->email->subject($subject);
//            $this->email->message($message);
            $data['message']=$message;
            isset($attachment)?$this->CI->email->attach($attachment):'';
            $this->CI->email->message($this->CI->load->view('email_templates/email', $data, true));
            $this->CI->email->set_mailtype('html');




            if($this->CI->email->send())
            {
                $this->CI->db->insert('outbox', array('to_user' => $to, 'subject' => $subject, 'm_type' => 'email', 'message' => $message, 'created_on' => time(),));

                echo 'message sent';
                // return true;
            }else{
                echo 'message failed';
//                false;
            }

        }else{
            echo 'Message Failed firsttime';
            //return false;
        }



    }

    function role_exist($value,$type='title'){


        if(isset($this->CI->session->permission)) {

            $array = $this->CI->session->permission;

            $key = array_search(strtolower($value), array_column($array, $type));

//             var_dump($key);



            return strlen($key)==0 ? false : true;
        }else{
            return false;
        }


    }


    function any_role()
    {
        return count($this->CI->session->permission) > 0 ? true : false;


    }


    // this is the email check for the user

    /**
     * @param $trans_type accountModification_accountCreation_accountDeletion_AccountChange
     * @param string $target
     * @param string $desc
     */
    function add_logs($user = null, $trans_type, $target = '', $desc = '')
    {
        $this->CI->load->library('user_agent');
        $user = strlen($user) > 0 ? $user : $this->CI->session->id;

        $values = array(
            'transaction_type' => $trans_type,
            'target' => character_limiter($target, 100),
            'details' => $desc,

            'created_by' => $user,
            'created_on' => time(),
            'platform' => $this->CI->agent->platform(),
            'browser' => $this->CI->agent->browser() . '-' . $this->CI->agent->version(),
            'agent_string' => $this->CI->agent->agent_string(),
            'ip' => $this->CI->input->ip_address(),
            'agent_referral' => $this->CI->agent->is_referral() ? $this->CI->agent->referrer() : '',
        );
        $this->CI->db->insert('logs', $values);
    }



    function get_table_columns($table_name=null){
        if(isset($table_name)){
          $columns=$this->CI->db->select('column_name')->from('INFORMATION_SCHEMA.COLUMNS')->where("TABLE_NAME = N'$table_name'")->get()->result();
          if(count($columns)>0){
              return $columns;
          }else{
              return false;
          }


        }else{
            return false;
        }
    }





    function create_log($string)
    {
        $this->CI->load->helper("file");

        $path='error_logs/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $date=date('dmY');
        $file = "error_log_$date.txt";

        $time=' >>>'.date('Y-m-d H:i:s').PHP_EOL;

        if ( ! write_file($path.$file, $string.$time, "a+"))
        {
            //echo 'Unable to write the file';
        }
        else
        {
            //echo 'File written!';
        }

    }




}






?>


