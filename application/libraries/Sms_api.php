<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 15/02/2018
 * Time: 11:08
 */

class Sms_api
{

    private $CI;
    private $sms_base_url = 'http://127.0.0.1:';
    private $port=8080;
    private $api_key = 'Basic Zm9vOmJhcg==';
    private $url_to_reply;
    private $username;
    private $password;



    function __construct($credentials)
    {

        if(isset($credentials)){
            $this->username='daniel';//$credentials['username'];fdi
            $this->password='daniel';//$credentials['password'];fdi
        }


        $this->CI =& get_instance();
        $this->CI->load->database();

        $this->url_to_reply=base_url('send/send_client_reply/');
    }


    public function sendSingle($to, $message,$subject='INFOSMS')
    {

        $fields=array(
            'to'=>phone($to),
            'from'=>$subject,
            'content'=>$message,
            'dlr'=> 'yes',
            'dlr-url'=> $this->url_to_reply,
            'dlr-level'=> 3,

        );




        $sub_url='/secure/send';
        return $this->sendPushMessage($fields,$sub_url,'single');
    }

    // sending push message to multiple users by firebase registration ids
    public function sendMultiple($contacts, $message,$subject='INFOSMS')
    {
        $fields = array(

            'messages' => array(array(
                'to' => $contacts,
                'content' => $message,
                'from'=> $subject,
            ))
        );

        $sub_url='/secure/sendbatch';

        return $this->sendPushMessage($fields,$sub_url);
    }


    protected function validate_phone($phone){

        if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
            if (strstr($phone, ',')) {
                $phone = substr($phone, 0, strpos($phone, ','));
            }
            if (strstr($phone, '/')) {
                $phone = substr($phone, 0, strpos($phone, '/'));
            }
            if (strstr($phone, '-')) {
                $phone = substr($phone, 0, strpos($phone, '-'));
            }
            $phone = trim($phone);
        }

        if (strlen($phone) >= 9) {


            if (substr($phone, 0, 1) == '0') {
                if (strlen($phone) == 10) {
                    $phone = '250' . substr($phone, 1);
                }
            }
            if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                $phone = '250' . $phone;
            }
            if (substr($phone, 0, 1) == '+') {
                $phone = substr($phone, 1);
            }


        }

        return $phone;

    }


    function sendPushMessage($fields,$sub_url,$message_type='bulk')
    {

        $url = $this->sms_base_url .$this->port. $sub_url;
        $curl = curl_init();

        $credentials = base64_encode($this->username . ':' . $this->password);

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "$this->port",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($fields, JSON_PRETTY_PRINT),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $credentials",
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->log_responses('Curl failed: ' . $err);
        } else {


            $this->save_sent_messages($fields,$message_type);
            return $response;


        }
    }

    function balance()
    {

        $sub_url='/secure/balance';
        $url = $this->sms_base_url .$this->port. $sub_url;
        $curl = curl_init();
        $credentials = base64_encode($this->username . ':' . $this->password);

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "$this->port",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $credentials",
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->log_responses('Curl failed-balance: ' . $err);
        } else {
            return $response;
        }
    }

    function save_sent_messages($fields,$message_type)
    {
        if($message_type=='bulk') {
            $message = $fields['messages'][0]['content'];
            $subject = $fields['messages'][0]['from'];
            $batch_messages = array();

            foreach ($fields['messages'][0]['to'] as $phone) {

                $values = array(
                    'to_user' => $phone,
                    'subject' => $subject,
                    'm_type' => 'SMS',
                    'message' => $message,
                    'created_on' => time(),
                    'created_by' => $this->CI->session->id
                );

                array_push($batch_messages, $values);

            }

            count($batch_messages) > 0 ? $this->CI->db->insert_batch('outbox', $batch_messages) : '';

        }else{
            $message = $fields['content'];
            $phone=$fields['to'];
            $subject=$fields['from'];

            $values = array(
                'to_user' => $phone,
                'subject' => $subject,
                'm_type' => 'SMS',
                'message' => $message,
                'created_on' => time(),
                'created_by' => $this->CI->session->id
            );
            $this->CI->db->insert('outbox',$values);
        }


    }


    function log_responses($string)
    {
        $this->CI->load->helper("file");

        $path = 'logs/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $date = date('dmY');
        $file = "log_response_$date.txt";

        if (!write_file($path . $file, $string . '-' . date('Y-m-d H:i:s ' . PHP_EOL), "a+")) {
            //echo 'Unable to write the file';
        } else {
            //echo 'File written!';
        }

    }




    function send_sms($phone, $sms, $subject = null, $client_ref_id = null)
    {
        if (!empty($phone)) {

            if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                if (strstr($phone, ',')) {
                    $phone = substr($phone, 0, strpos($phone, ','));
                }
                if (strstr($phone, '/')) {
                    $phone = substr($phone, 0, strpos($phone, '/'));
                }
                if (strstr($phone, '-')) {
                    $phone = substr($phone, 0, strpos($phone, '-'));
                }
                $phone = trim($phone);
            }
            if (strlen($phone) >= 9) {
                if (substr($phone, 0, 1) == '0') {
                    if (strlen($phone) == 10) {
                        $phone = '250' . substr($phone, 1);
                    }
                }
                if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                    $phone = '250' . $phone;
                }
                if (substr($phone, 0, 1) == '+') {
                    $phone = substr($phone, 1);
                }
//                    $q = $this->CI->db->select('id')->from('outbox')->where(array('to_user' => $phone, 'message' => $sms))->limit(1)->get()->result();
//                    if (!isset($q[0]->id)) {


                if ($this->CI->session->user_type == 7) {


                    $username = $this->CI->session->username;
                    $password = str_rot13($this->CI->session->balance_p);

                } else {

                    $username = 'daniel';
                    $password = 'daniel';

                }
                $subject = isset($subject) ? $subject : 'INFOSMS';


                $url_encoded_sms = urlencode($sms);
                $url_to_reply = base_url('send/send_client_reply/');


                $url = $this->sms_base_url . "send?username=$username&password=$password&to=$phone&content=$url_encoded_sms&from=$subject";
                $url .= "&dlr=yes&dlr-level=2&dlr-url=$url_to_reply&dlr-method=GET";
                $socket = url_get_contents($url);

                if ($socket) {

                    //$this->CI->db->insert('outbox','to_user,subject,m_type,message,created_on,created_by',"'$phone','Notification','SMS','$sms','".time()."','0'");

                    $this->CI->db->insert('outbox', array('to_user' => $phone, 'subject' => $subject, 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => $this->CI->session->id));

//                            fclose($socket);
                }
//                    }
            }
        }
    }


}