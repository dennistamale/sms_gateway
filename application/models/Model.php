<?php

class model extends CI_Model
{

    private $site_name = '';
    private $site_email = '';
    private $siteurl = '';
    public $out_table;


    function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->site_name = $this->site_options->title('site_name');
        $this->site_email = $this->site_options->title('contact_email');
        $this->siteurl = $this->site_options->title('siteurl');
        $this->out_table = 'outbox_' . date('dmY');
    }


    function get_user_details($value,$field='id')
    {

        $output = false;

        $user_details = $this->db->select()
            ->from('users a')
            ->where(array('a.'.$field => $value))->get()->row();



            $output=!empty($user_details)?$user_details:$output;


        return $output;

    }

    function get_alluser($value,$field='user_type'){


        $output = false;

        $this->db->select()
            ->from('users a')
            ->where(array('a.'.$field => $value));

        $this->session->user_type != 1 ? $this->db->where('id', $this->session->id) : '';
        $user_details = $this->db->get()->result();
        if (!empty($user_details)) {

            return $user_details;
        }

        return $output;
    }

function get_sub_accounts($value,$field='user_type'){


        $output = false;

        $this->db->select()
            ->from('users a')
            ->where(array('a.'.$field => $value));

        $user_details = $this->db->get()->result();
        if (!empty($user_details)) {

            return $user_details;
        }

        return $output;
    }

    function get_nationality($country)
    {

        $n = $this->db->select('nationality')->from('country')->where('a2_iso', $country)->get()->row();

        return !empty($n) ? $n->nationality : 'N/A';
    }


    function register_user($values)
    {

        $values['created_on'] = time();
        $values['created_by'] = isset($this->session->id)?$this->session->id:0;

        if ($this->db->insert('users', $values)) {

            $fname = $values['first_name'];

            $username = $values['username'];


            $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $username
To login, go to " . anchor('login') . ". Remember you can change your password once logged.\r\n";

            $this->custom_library->sendHTMLEmail2($values['email'], $this->site_name, $email_msg);


            //Creating A log in the database
            $account = $this->db->select('id')->from('users')->where(array('username' => $values['username'], 'phone' => $values['phone']))->get()->row();
            $user_id = $account->id;

            $this->custom_library->add_logs($user_id, 'account_creation', $values['first_name'] . ' ' . $values['last_name'], 'has created an account for user ');


            return array(
                'user_id' => $user_id,
                'registered' => 0
            );


        }
    }



    function author_register($values)
    {

        if (!empty($values)) {

            $where = array(
                'email' => $values['email'],
                'phone' => $values['phone']
            );


            $output = $this->db->select('id')->from('author')->where($where)->get()->row();

            if (count($output) == 1) {

                $user_id = $output->id;

                return array(
                    'user_id' => $user_id,
                    'registered' => 1
                );

            } else {

                $values['created_on'] = time();

                // print_r($values);

                if ($this->db->insert('author', $values)) {


                    $output = $this->db->select('id,first_name,last_name')->from('author')->where($where)->get()->row();

                    // print_r($output);

                    if (count($output) == 1) {
//returning the user_id which will be user to create the application
                        $user_id = $output->id;
                        $this->custom_library->add_logs($user_id, 'account_creation', $values['first_name'] . ' ' . $values['last_name'], 'has created an account for user ');

                        return array(
                            'user_id' => $user_id,
                            'registered' => 0
                        );

                    } else {
                        return false;
                    }


                } else {

                    return false;
                }
            }


        } else {
            return false;
        }
    }

    function get_countries(){
        return $this->db->select()->from('country')->get()->result();
    }


    function get_country_name($country = null)
    {

        $output = 'N/A';

        if (isset($country)) {

            $country = $this->db->select()->from('country')->where(array('a2_iso' => $country))->get()->row();

            $output = isset($country) ? $country->country : 'N/A';
        }
        return $output;

    }

    function country_a3_iso($country = null)
    {

        $output = 'N/A';

        if (isset($country)) {

            $country = $this->db->select()->from('country')->where(array('a2_iso' => $country))->get()->row();

            $output = isset($country) ? $country->a3_iso : 'N/A';
        }
        return $output;

    }



//$status_list = array(
//array("bg-info" => "Submitted")0,
//array("bg-warning" => "Assigned")1,
//array("bg-teal" => "Edited")2,
//array("bg-success" => "Approved")3,
//array("bg-danger" => "not approved")4,
//array("bg-violet" => "illustration")5,
//array("bg-indigo" => "published")6
//);


    function process_story($ids, $action)
    {

        $action_checking_for = '';

        if ($action == 1) {
            $action_checking_for = 'assigned';
        } elseif ($action == 2) {
            $action_checking_for = 'edited';
        } elseif ($action == 3) {
            $action_checking_for = 'approved';
        } elseif ($action == 4) {
            $action_checking_for = 'approved';//this also stands for the rejection
        } elseif ($action == 5) {
            $action_checking_for = 'illustrated';
        } elseif ($action == 6) {
            $action_checking_for = 'published';
        }
        $updates = array();
        $non_updates = array();

        foreach ($ids as $id) {

            //this part checks if the request type is either//,'approval'=>0
            $exist = $this->db->where(array('id' => $id))->from('story')->count_all_results();

            $current_user = $this->session->id;
            $time = time();

            $values = array(
                'id' => $id,
                'status' => $action,
                'updated_on' => $time,
                'updated_by' => $current_user,
            );
            if ($action == 3 || $action == 6) {

                $values[$action_checking_for . '_on'] = $time;
                $values[$action_checking_for . '_by'] = $current_user;

            }


            $exist == 1 ? array_push($updates, $values) : array_push($non_updates, $id);

        }
        count($updates) > 0 ? $this->db->update_batch('story', $updates, 'id') : '';


        $message = '';
        $message .= '<br/><b style="font-size: larger">' . count($updates) . '</b> Records(s) have been  <b>' . humanize($action_checking_for) . '</b>';
        $message .= count($non_updates) > 0 ? '<br/><b  style="font-size: larger">' . count($non_updates) . '</b> Record(s) cannot be <b>' . humanize($action_checking_for) . '</b>' : '';

        $result = array(
            'message' => $message,
            'records_updated' => count($updates)
        );

        return $result;
    }


//this is the method for changing the password
    function reset_password($id)
    {
        $return = false;
        $user = $this->db->select()->from('users')->where(array('id' => $id))->get()->row();

        if (count($user) == 1) {
            $year = date('Y', $user->dob);
            $last_name = strtolower($user->last_name);

            if ($pp = $this->create_password($year, $last_name)) {
                $this->db->where(array('id' => $id))->update('users', array(
                    'password' => $pp,
//                    'balance_p'=>str_rot13($year . $last_name),
                    'verified' => 0
                ));

                $return = $year . $last_name;
            }
        }

        return $return;
    }


    function create_password($year = null, $last_name = null)
    {

        if (isset($year) && isset($last_name)) {
            return hashValue($year . $last_name);
            // return $year.$last_name;
        } else {
            return false;
        }
    }


    //checking wether the account exists
    public function account_exists($username, $password)
    {
        $status = 0;

        $results = $this->db->select('a.*,b.title')
            ->from('users a')
            ->join('user_type b','a.user_type=b.id')
            ->where(array('username' => $username, 'password' => hashValue($password)))
            ->get()->row();

        $this->login_attempts($username,count($results));

        return $results;

    }



    //this is session is assigned to the user after signing up an account , it is a one time function
    function session_assign($u, $p)
    {
        $results = $this->account_exists($u, $p);
        if (isset($results->username)) {
            //$this->db->insert('wallet',array('user'=>$results->id,'start_amount'=>0,'current_amount'=>0,'created_on'=>time(),'created_by'=>$results->id));

            $session_data = array(
                'id' => $results->id,
                'username' => $results->username,
                'first_name' => $results->first_name,
                'last_name' => $results->last_name,
                'photo' => $results->photo,
                'email' => $results->email,
                'user_type' => $results->user_type,
                'sub_type' => $results->sub_type,
                'phone' => $results->phone

            );

            $signin = $this->session->set_userdata($session_data);
            $this->get_permissions($this->session->user_type);
            $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));



            // header('Refresh: 3; url=' . base_url('index.php/myportal'));
//            $signin == true ? redirect('/login/', 'location') : $this->login();
           // redirect('/login/', 'location');


        }
        else {
            redirect('/login/', 'location') ;
//            $this->login();
        }


    }



    function get_permissions($user_role){


        $per=$this->db->select('a.perm_id,b.perm_group,b.title')
            ->from('role_perm a')->join('permissions b','a.perm_id=b.id')
            ->where(array('role_id'=>$user_role))
            ->get()->result();

        $user_role=array();
        foreach ($per as $p){
            $f= array(
                'role'=>$p->perm_id,
                'title'=>strtolower(trim($p->title)),
                'group'=>strtolower(trim($p->perm_group))
            );
            array_push($user_role,$f);
        }
        //  print_r($user_role);

        count($user_role)>0?$this->session->set_userdata(array('permission'=>$user_role)):'';

        //print_r($this->session->perm);
    }

    ///function for checking the login attempts

    function login_attempts($username, $status)
    {
        $user = $this->db
            ->select('id,status,login_attempts,first_name,last_name')
            ->from('users')->where(array('username' => $username))
            ->get()->row();

        if(isset($user)){
            if ($user->status != 2) {

                if ($status == 0) {


                    if (count($user) == 1 && $user->login_attempts < 4) {

                        $qry1 = "UPDATE `users` SET `login_attempts` = login_attempts+1 WHERE `username` = '$username'";
                        $this->db->query($qry1);

                    } elseif (count($user) == 1 && $user->login_attempts >= 4) {


                        $time = time();
                        $qry1 = "UPDATE `users` SET `login_attempts` = login_attempts+1 ,`status`=2,updated_on=$time WHERE `username` = '$username'";
                        $this->db->query($qry1);
                        $this->custom_library->add_logs('System', 'account_modification', $user->first_name . ' ' . $user->last_name, '  has blocked user');


                        $dat = array(
                            'alert' => 'danger',
                            'message' => '<span style="font-size: larger;">Your account has been Blocked from accessing the System <b>Please contact administrator !!!!</b></span>'
                        );

                        $this->load->view('alert', $dat);

                    }

                } else {
                    $qry_reset = "UPDATE `users` SET `login_attempts` = 0 WHERE `username` = '$username'";
                    $this->db->query($qry_reset);
                }
            } else {
                $dat = array(
                    'alert' => 'danger',
                    'message' => '<span style=" font-size: larger;">Your account has been Blocked from accessing the System <b>Please contact administrator !!!!</b></span>'
                );

                $this->load->view('alert', $dat);
            }
        }


    }



    function account_balance($user_id=null){

        $user_id=isset($user_id)?$user_id:$this->session->id;

        $user= $this->get_user_details($user_id);

        return my_count($user)>0?$user->balance:'N/A';
    }



    function upload_group_contacts($excel,$group_id){
        $no = 0;
        if (strlen(trim($excel[1]['A'])) == 0 || strlen(trim($excel[2]['A'])) == 0) {
            $response['alert'] = 'danger';
            $response['message'] = 'No Contacts founs';
            }
        else {


            $extracted=0;
            $not_extracted=0;
            $batch_data=array();

            foreach ($excel as $col => $row) {

                if ($col > 0) {

                    if ($row['A'] != '') {

                        $phone = phone($row['A']);



                        $values = array(

                            'phone_no' => $phone,
                            'group_id'=>$group_id,
                            'first_name' => isset($row['B'])?$row['B']:'',
                            'created_on' => time(),
                            'created_by' => $this->session->id,

                        );

                        $r = $this->db->where(array('phone_no' => $phone, 'group_id' => $group_id))->from('contacts')->count_all_results();

                        if ($r == 0) {

                            array_push($batch_data,$values);
                            $extracted ++;

                        } else {

                            $not_extracted ++;

                        }

                        } else {
                        $not_extracted ++;
                    }
                    $no++;
                }


            }

            $message = $extracted.' Records extracted Successfully <br/>';
            $message .= $not_extracted>0?$not_extracted.' Records Not extracted Successfully ':'';


            $response['message'] = $message;
            $response['alert'] = 'success';

            count($batch_data)>0?$this->db->insert_batch('contacts', $batch_data):'';

            return $response;


        }

    }


    function upload_g_contacts($file_path,$group_id){
        $this->db->save_queries = false; // ADD THIS LINE TO SOLVE ISSUE
        $this->load->library('spout');

        try {



            $reader=$this->spout->reader;

            //Lokasi file excel
//            $file_path = "/Applications/XAMPP/xamppfiles/htdocs/sms_gateway/uploads/groups/test10k2.xlsx";
            $reader->open($file_path); //open the file

            $i = 0;

            /**
             * Sheets Iterator. Kali aja multiple sheets
             **/
            foreach ($reader->getSheetIterator() as $sheet) {

                @ignore_user_abort(true);
                @ini_set('MAX_EXECUTION_TIME', -1);
                @set_time_limit(0);
                @ini_set('memory_limit', '1000M');

                //Rows iterator

//                print_r($sheet->getRowIterator());
//                exit;
                $extracted=0;
                $not_extracted=0;
                $contacts=array();
                foreach ($sheet->getRowIterator() as $row) {


                    $values = array(

                        'phone_no' => $phone=$row[0],
                        'group_id'=>$group_id,
                        'first_name' => '',//isset($row['B'])?$row['B']:'',
                        'created_on' => time(),
                        'created_by' => $this->session->id,

                    );


                    $r = $this->db->where(array('phone_no' => $phone, 'group_id' => $group_id))->from('contacts')->count_all_results();

                    if ($r == 0) {

                        array_push($contacts,$values);
                        $extracted ++;

                    } else {

                        $not_extracted ++;

                    }



                    ++$i;



                }

//                print_r($contacts);
            }

            $rows= "Total Rows : " . $i;
            $reader->close();
            count($contacts)>0?$this->db->insert_batch('contacts', $contacts):'';


            $memory= "Peak memory:". (memory_get_peak_usage(true) / 1024 / 1024). " MB";

            return array(
                'rows'=>$rows,
                'memory'=>$memory,
                'extracted'=>$extracted,
                'not_extracted'=>$not_extracted
            );

        } catch (Exception $e) {

            echo  $e->getMessage();
            exit;
        }
    }



    function group_code()
    {


        $token = code(10);

        while (1) {
            if ($this->db->where(array('group_code' => $token))->from('group')->count_all_results() == 0) {
                break;
            } else {
                $token = code(10);
            }

        }

        return strtoupper($token);
    }

    function get_sent_message($id=null){

        $this->db->select()->from('outbox');

        $this->db->order_by('id', 'desc');

        $this->session->user_type!=1?$this->db->where(array('created_by'=>$this->session->id)):'';

        if (isset($id)) {

            $this->db->where('id', $id);

            $result = $this->db->get()->row();

        } else {


            $result = $this->db->limit(200)->get()->result();

        }
        return $result;
    }


    function get_groups(){
        $this->db->select()->from('group');
        $this->session->user_type!=1?$this->db->where(array('owner'=>$this->session->id)):'';
        $this->db->order_by('id','desc');
        $groups=$this->db->get()->result();

        return $groups;
    }


    function get_group_contacts($id=null,$limit=null){



        $this->db->select('a.*')->from('contacts a');


        if(isset($id)){


            $this->db->where(array('a.group_id'=>$id));

        }

        isset($limit)?$this->db->limit($limit):'';

        return $this->db->get()->result();





    }

    function sms_per_day($date,$network,$usename=null){
        $this->load->library('auto_create_table');

        $table='inbox_' . date('dmY',$date);

        // echo $table;
        $this->auto_create_table->create_inbox_table($table);

        $this->db->select_sum('content_count');
        $this->db
            ->group_start()
            ->where(
                array(
                'status'=>'Success',
                'network'=>($network=='OTHERS'?'RWANDACELL':$network)
                )
            )
            ->group_end();

        isset($usename)?$this->db->where(array('username'=>$usename)):'';

        $this->session->user_type!=1?$this->db->where('username',$this->session->id):'';

        $a = $this->db->from($table)->get()->row();

        return isset($a->content_count)?$a->content_count:0;
    }


}