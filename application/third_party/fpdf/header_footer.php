
<?php
//error_reporting(0);
//define('FPDF_FONTPATH', 'font/');

//define('HEAD', 'Head');
//define('SUBHEAD', 'Sub head');
define('ORIENTATION',isset($orientation)?$orientation:'P');
require('fpdf-1.8.php');
//require('htmlparser.inc.php');



class PDF extends FPDF{
    var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';
    var $orientation='';


    public $first_heading_line='THE REPUBLIC OF UGANDA';
    public $second_heading_line='MINISTRY OF FOREIGN AFFAIRS ';


    function __construct()
    {
        parent::__construct();


    }

  //  var $level=$level_title;
        // Page header
            function Header()
            {

                if ( $this->PageNo() !== SKIP_HEADER_FOOTER ) {


                    //$this->FPDF('P','mm','A4');

                    if (ORIENTATION == 'L') {
                        $y = 4;
                        $this->SetLineWidth(.1);
                        $this->SetY($y);
                        $this->SetFont('Arial', 'B', 6);
                        // $this->Cell(0,8,'LLIN '.HEAD,0,1,'C');


                        // Avatar
                        strlen(LOGO) > 0 ? (ORIENTATION == 'L' ? $this->Image(LOGO, 100, 5, 15) : $this->Image(LOGO, 95, 5, 15)) : '';
                        $this->ln(12);


                        // Title
                        $this->SetFont('Arial', 'B', 10);
                        $this->SetTextColor(0, 0, 0);

//                        MINISTRY OF FOREIGN AFFAIRS
//APPLICATION FOR AN IDENTITY CARD

                        $this->Cell(0, 5, $this->first_heading_line, 0, 1, 'C');
                        $this->Cell(0, 5, $this->second_heading_line, 0, 1, 'C');
                        //$this->Cell(0, 5, 'APPLICATION FOR AN IDENTITY CARD', 0, 1, 'C');

                        // $this->Line(0, 190, 300, 190);//landscape
                        ORIENTATION == 'L' ? $this->Line(0, 30, 300, 30) : $this->Line(10, 30, 203, 30);
                        $this->SetFont('Arial', '', 9);
                        $this->SetTextColor(0, 0, 0);
//                $this->Cell(190,6,SUBHEAD,0,1,'C');

                        $this->SetTextColor(0, 0, 0);
                        $this->SetFont('Arial', '', 8);
                        $this->Ln(13);
                        $this->SetFont('Arial', 'B', 10);
                    }
                    else {
                        $y = 4;
                        $this->SetLineWidth(.1);
                        $this->SetY($y);
                        $this->SetFont('Arial', 'B', 6);

                        // Avatar
                        $fg = intval($this->GetPageWidth());
                        strlen(LOGO) > 0 ? ($fg >= 297 ? $this->Image(LOGO, 133, 3, 25) : $this->Image(LOGO, 93, 3, 25)) : '';


                        $this->ln(23);


                        // Title
                        $this->SetFont('Arial', 'B', 8);
                        $this->Cell(0, 5, $this->first_heading_line, 0, 1, 'C');
                        $this->SetFont('Arial', 'B', 10);
                        $this->Cell(0, 5, $this->second_heading_line, 0, 1, 'C');
                        ORIENTATION == 'L' ? $this->Line(10, 11, 300, 11) : $this->Line(0, 45, 300, 45);
                        $this->SetFont('Arial', '', 9);
                        $this->SetTextColor(0, 0, 255);
//                $this->Cell(190,6,SUBHEAD,0,1,'C');

                        $this->SetTextColor(0, 0, 0);
                        $this->SetFont('Arial', '', 8);

                        $this->Ln(-2);
                        $this->SetFont('Arial', 'B', 10);

                    }
                }

            }



        // Page footer
            function Footer()
            {
                if ( $this->PageNo() !== SKIP_HEADER_FOOTER ) {
                    // Position at 1.5 cm from bottom
                    if (ORIENTATION == 'L') {
                        $this->SetY(-20);

                        $this->SetDrawColor(0, 0, 0);
                        $this->SetLineWidth(.1);
                        $this->Line(0, 190, 300, 190);//landscape
                        //$this->Line(0,275,250,275);//portrait
                        $this->SetFont('Arial', '', 6);
                        $this->SetTextColor(0, 0, 255);

                        $this->Cell(0, 5, $this->first_heading_line, 0, 1, 'C');
                        $this->Cell(0, 0, SUBHEAD, 0, 1, 'C');
                        // Arial italic 8
                        $this->SetTextColor(0, 0, 0);
                        $this->SetFont('Arial', 'I', 8);
                        // Page number
                        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');

                    }
                    else {
                        $this->SetY(-25);

                        $this->SetDrawColor(0, 0, 0);
                        $this->SetLineWidth(.1);
                        $this->Line(0, 275, 250, 275);//landscape
                        //$this->Line(0,275,250,275);//portrait
                        $this->SetFont('Arial', '', 6);
//                    $this->SetTextColor(0, 0, 255);
                        $this->Cell(0, 10, $this->first_heading_line, 0, 1, 'C');
                        $this->Cell(0, 0, SUBHEAD, 0, 1, 'C');
                        // Arial italic 8
                        $this->SetTextColor(0, 0, 0);
                        $this->SetFont('Arial', 'I', 8);
                        // Page number
                        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
                    }
                }
            }

    function WriteHTML($html,$line_height=6)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,3,$e,0,1,'C');
                else
                    $this->Write($line_height,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
        if($tag=='BLOCKQUOTE'){
            $this->SetLeftMargin(28);
            $this->Ln(10);
        }
        if($tag=='PARA'){
            $this->SetLeftMargin(50);
            $this->Ln(10);
        }
        if($tag=='TAB'){
            $this->SetLeftMargin(65);
        }
        /*if($tag=='NAME'){
            $n = "Azam Wahaj";
            //$this->Write(5, $n);
            $this->Cell(0 , 5, $n, 0, 0, 'L', false, '');

        }
        if($tag=='ADDRESS'){
            $address = "skjhdkajshfkjhsdkf salkdfjls";
            //$this->Write(5, $n);
            $this->Cell(0 , 5, $address, 0, 0, 'L', false, '');

        }*/
        if($tag=='TAB2'){
            $this->SetLeftMargin(110);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
        if($tag=='BLOCKQUOTE')
            $this->SetLeftMargin(20);
        //$this->Ln(5);
        if($tag=='PARA')
            $this->SetLeftMargin(20);
        if($tag=='TAB')
            $this->SetLeftMargin(28);
        if($tag=='TAB2')
            $this->SetLeftMargin(28);

    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
    function WriteTable($data, $w)
    {
        //$this->SetLineWidth(.1);
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        //$this->SetFont('Arial','',8);
        foreach($data as $row)
        {
            $nb=0;
            for($i=0;$i<count($row);$i++)
                $nb=max($nb,$this->NbLines($w[$i],trim($row[$i])));
            $h=5*$nb;
            $this->CheckPageBreak($h);
            for($i=0;$i<count($row);$i++)
            {
                $x=$this->GetX();
                $y=$this->GetY();
                $this->Rect($x,$y,$w[$i],$h);
                $this->MultiCell($w[$i],5,trim($row[$i]),0,'L');
                //Put the position to the right of the cell
                $this->SetXY($x+$w[$i],$y);//
            }
            $this->Ln($h);

        }
    }


    function FancyTable($header, $data)
        {
            // Colors, line width and bold font
            $this->SetFillColor(255,0,0);
            $this->SetTextColor(255);
            $this->SetDrawColor(128,0,0);
            $this->SetLineWidth(.3);
            $this->SetFont('','B');
            // Header
            $w = array(40, 35, 40, 45);
            for($i=0;$i<count($header);$i++)
                $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(240, 240, 245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = false;
            foreach($data as $row)
            {
                $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
                $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
                $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
                $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
                $this->Ln();
                $fill = !$fill;
            }
            // Closing line
            $this->Cell(array_sum($w),0,'','T');
        }


    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 && $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function ReplaceHTML($html)
    {
        $html = str_replace( '<li>', "\n<br> - " , $html );
        $html = str_replace( '<LI>', "\n - " , $html );
        $html = str_replace( '</ul>', "\n\n" , $html );
        $html = str_replace( '<strong>', "<b>" , $html );
        $html = str_replace( '</strong>', "</b>" , $html );
        $html = str_replace( '&#160;', "\n" , $html );
        $html = str_replace( '&nbsp;', " " , $html );
        $html = str_replace( '&quot;', "\"" , $html );
        $html = str_replace( '&#39;', "'" , $html );
        $html = str_replace( '<br>', "\n<br>" , $html );
        $html = str_replace( '<h1>', "</h1>" , $html );
        $html = str_replace( '<center>', "</center>" , $html );

        return $html;
    }

    function ParseTable($Table)
    {
        $_var='';
        $htmlText = $Table;
        $parser = new HtmlParser ($htmlText);
        while ($parser->parse())
        {
            if(strtolower($parser->iNodeName)=='table')
            {
                if($parser->iNodeType == NODE_TYPE_ENDELEMENT)
                    $_var .='/::';
                else
                    $_var .='::';
            }

            if(strtolower($parser->iNodeName)=='tr')
            {
                if($parser->iNodeType == NODE_TYPE_ENDELEMENT)
                    $_var .='!-:'; //opening row
                else
                    $_var .=':-!'; //closing row
            }
            if(strtolower($parser->iNodeName)=='td' && $parser->iNodeType == NODE_TYPE_ENDELEMENT)
            {
                $_var .='#,#';
            }
            if ($parser->iNodeName=='Text' && isset($parser->iNodeValue))
            {
                $_var .= $parser->iNodeValue;
            }
        }
        $elems = explode(':-!',str_replace('/','',str_replace('::','',str_replace('!-:','',$_var)))); //opening row
        foreach($elems as $key=>$value)
        {
            if(trim($value)!='')
            {
                $elems2 = explode('#,#',$value);
                array_pop($elems2);
                $data[] = $elems2;
            }
        }
        return $data;
    }

    function WriteHTML2($html)
    {
        $html = $this->ReplaceHTML($html);
        //Search for a table
        $start = strpos(strtolower($html),'<table');
        $end = strpos(strtolower($html),'</table');
        if($start!==false && $end!==false)
        {
            $this->WriteHTML2(substr($html,0,$start).'<BR>');

            $tableVar = substr($html,$start,$end-$start);
            $tableData = $this->ParseTable($tableVar);
            for($i=1;$i<=count($tableData[0]);$i++)
            {
                if($this->CurOrientation=='L')
                    $w[] = abs(80/(count($tableData[0])-1))+24;  //80 => 120
                else
                    $w[] = abs(80/(count($tableData[0])-1))+5;  //80 => 120
            }
            $this->WriteTable($tableData,$w);

            $this->WriteHTML3(substr($html,$end+8,strlen($html)-1).'<BR>');
        }
        else
        {
            $this->WriteHTML3($html);
        }
    }

    function PDF($orientation='P', $unit='mm', $format='A4')
    {
        //Call parent constructor
        $this->FPDF($orientation,$unit,$format);
        //Initialization
        $this->B=0;
        $this->I=0;
        $this->U=0;
        $this->HREF='';
    }

    function WriteHTML3($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract attributes
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $attr=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $attr[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$attr);
                }
            }
        }
    }
        }


?>