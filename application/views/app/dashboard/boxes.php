<?php

$ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
$sent =$this->dashboard_model->sum_sent_sms($fdate,$ldate);

//$sent = $this->db->where(array('status'=>'Success'))->from('inbox')->count_all_results();
$delivered_sms = $this->dashboard_model->sum_delivered_sms($fdate,$ldate);
$un_delivered_sms =$this->dashboard_model->sum_un_delivered_sms($fdate,$ldate);;
$users = $this->db->from('users')->count_all_results();

?>



<!-- Quick stats boxes -->
<div class="row">
    <div class="col-lg-3 col-sm-6 col-xs-6">

        <!-- Members online -->
        <div class="panel bg-teal-400">
            <div class="panel-body">
                <div class="heading-elements">
                    <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                </div>

                <h3 class="no-margin"><?= number_format($sent) ?></h3>
                Sent
                <!--                        <div class="text-muted text-size-small">489 avg</div>-->
            </div>

            <div class="container-fluid">
                <div id="members-onlin"></div>
            </div>
        </div>
        <!-- /members online -->

    </div>

    <div class="col-lg-3  col-sm-6  col-xs-6">

        <!-- Current server load -->
        <div class="panel bg-danger">
            <div class="panel-body">
                <div class="heading-elements">

                </div>

                <h3 class="no-margin"><?= number_format($delivered_sms) ?></h3>
                Delivered SMS
                <!--                        <div class="text-muted text-size-small">34.6% avg</div>-->
            </div>

            <div id="server-loa"></div>
        </div>
        <!-- /current server load -->

    </div>

    <div class="col-lg-3  col-sm-6  col-xs-6">

        <!-- Today's revenue -->
        <div class="panel bg-blue-400">
            <div class="panel-body">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>

                <h3 class="no-margin"><?= number_format($un_delivered_sms); ?></h3>
                UN Delivered SMS
                <!--                        <div class="text-muted text-size-small">$37,578 avg</div>-->
            </div>

            <div id="today-revenu"></div>
        </div>
        <!-- /today's revenue -->

    </div>

    <div class="col-lg-3  col-sm-6   col-xs-6">

        <?php if($this->session->user_type==1){ ?>
            <!-- Today's revenue -->
            <div class="panel bg-indigo-300">
                <div class="panel-body">
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>


                    <h3 class="no-margin"><?= number_format($users) ?></h3>
                    Users



                    <!--                        <div class="text-muted text-size-small">$37,578 avg</div>-->
                </div>

                <div id="today-revenu"></div>
            </div>
            <!-- /today's revenue -->

        <?php }else{ ?>

            <!-- Today's revenue -->
            <div class="panel bg-indigo-300">
                <div class="panel-body">




                    <?php

                    $balance=$this->model->account_balance(); ?>
                    <h3 class="no-margin text-bold" >
                        <span style="font-size: smaller;"><?= $this->session->currency ?></span>

                        <?= strlen($balance)>0?$balance:$balance ?></h3>
                    Credit  Balance



                </div>
                <div id="today-revenu" hidden></div>

            </div>
            <!-- /today's revenue -->

        <?php } ?>
    </div>
</div>


<?php

$status_list = array(
    array("success" => "Pending"),
    array("info" => "Closed"),
    array("danger" => "On Hold"),
    array("warning" => "Fraud"),
    array("primary" => "Fraud"),
    array("pink" => "Fraud"),
    array("violet" => "Fraud"),
    array("indigo" => "Fraud")
);

?>

            <div class="row">
                <?php
               $networks=$this->dashboard_model->networks_sent($fdate,$ldate);



                foreach ($networks as $n){

                    $status = $status_list[rand(0, 6)];

                    ?>

            <div class="col-lg-3 col-sm-6 col-xs-6">

                <!-- Members online -->
                <div class="panel bg-<?= key($status) ?>-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                        </div>

                        <h3 class="no-margin"><?= number_format($n->sent) ?></h3>
                        <?=  ($n->network=='RWANDACELL'?'OTHERS':strtoupper(humanize($n->network))).' SENT' ?>
                        <!--                        <div class="text-muted text-size-small">489 avg</div>-->
                    </div>

                    <div class="container-fluid">
                        <div id="members-onlinesd"></div>
                    </div>
                </div>
                <!-- /members online -->

            </div>

                <?php } ?>
            </div>





<!-- /quick stats boxes -->