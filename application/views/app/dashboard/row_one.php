<?php
$households = $this->db->from('household')->count_all_results();
$users = $this->db->from('users')->count_all_results();
$health_facilities = $this->db->from('healthfacilities')->count_all_results();
$health_workers = $this->db->from('vht')->count_all_results();

//dependants

$dep_male = 0;
$dep_female = 0;
$dependants = $dep_male + $dep_female;
$male = 0;
$female = 0;

//missions

//diplomatic_ids
$diplomatic_ids = 0;

?>

<style>
    .gender {
        font-size: 168px;
        margin-left: -9px;
    }
</style>


<!-- Quick stats boxes -->
<div class="row">
    <div class="col-md-6">

        <div class="row">

            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-blue-400 blue-replace">
                    <div class="panel-body">
                        <!--                        <div class="heading-elements">-->
                        <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                        <!--                        </div>-->

                        <h3 class="no-margin"><?= number_format($households) ?></h3>
                        Households

                    </div>

                    <div class="container-fluid">
                        <div id="members-online"></div>
                    </div>
                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-purple-400">
                    <div class="panel-body">
                        <!--                        <div class="heading-elements">-->
                        <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                        <!--                        </div>-->

                        <h3 class="no-margin"><?= number_format($health_facilities) ?></h3>
                        Health facilities

                    </div>

                    <div class="container-fluid">
                        <div id="members-online2"></div>
                    </div>
                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-6">

                <!-- Current server load -->
                <div class="panel bg-orange-400">
                    <div class="panel-body">
                        <h3 class="no-margin"><?= number_format($users) ?></h3>
                        Users

                    </div>

                    <div id="server-load"></div>
                </div>
                <!-- /current server load -->

            </div>

            <div class="col-lg-6">

                <!-- Today's revenue -->
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>

                        <h3 class="no-margin"><?= number_format($health_workers); ?></h3>
                        Health Workers

                    </div>

                    <div id="today-revenue"></div>
                </div>
                <!-- /today's revenue -->

            </div>


        </div>

    </div>

    <div class="col-md-6">
        <div class="row" style="text-align: center">
            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <i class="icon-man gender"></i>
                        <h3 class="no-margin"><?= number_format($male) ?></h3>
                        <h5>Male Diplomats</h5>
                        <div class="text-muted text-size-small"><?= number_format($dep_male) ?> Dependants</div>

                    </div>

                </div>
                <!-- /members online -->

            </div>
            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-red red-replace">
                    <div class="panel-body">


                        <i class="icon-woman gender"></i>
                        <h3 class="no-margin"><?= number_format($female) ?></h3>
                        <h5>Female Diplomats</h5>
                        <div class="text-muted"><?= number_format($dep_female) ?> Dependants</div>

                    </div>

                </div>
                <!-- /members online -->

            </div>


        </div>
    </div>

</div>
<!-- /quick stats boxes --
