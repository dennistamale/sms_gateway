<!-- Dashboard content -->
<div class="row">
    <div class="col-lg-8">

        <!-- BEGIN PORTLET-->
        <div class="panel panel-flat">

            <div class="panel-heading hidden">
                <h6 class="panel-title">Daily Ussd Transactions</h6>
            </div>
            <div class="panel-body">
                <div id="sms" style="min-width: 310px; height: 430px; margin: 0 auto"></div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>

    <div class="col-lg-4">

        <!-- Scrollable table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Latest Activities</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive pre-scrollabl" style="height: 345px;">
                <table class="table">
                    <thead>
                    <tr>
                        <!--                        <th>Type</th>-->
                        <th>Activity</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($this->dashboard_model->recent_activities() as $tl): ?>
                        <tr>
                            <td>
                                <a href="#"> <?= humanize($tl->first_name . ' ' . $tl->last_name) ?></a> <?php echo $tl->details ?> <?= $tl->target ?>
                                <span class="pull-right text-muted">
                            <?= trending_date($tl->created_on) ?>
                        </span>
                            </td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <?= anchor($this->page_level . 'logs', 'See All Records <i class="icon-arrow-right16"></i> ') ?>

                </div>
            </div>
        </div>
        <!-- /scrollable table -->

    </div>


</div>
<!-- /dashboard content -->

