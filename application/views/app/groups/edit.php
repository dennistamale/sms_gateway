<?php $cont= $this->db->select()->from('group')->where(array('id'=>$id))->get()->row();



?>
<div class="row">

    <div class="col-md-6">

        <!-- BEGIN SAMPLE FORM panel-->
        <div class="panel light bordered">
            <div class="panel-heading">
                <div class="panel-title">
                <h5>
                    <span class="caption-subject bold uppercase">Edit Contact</span>
                </h5>
            </div>
                <div class="actions hidden">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="panel-body form">

                <?php echo form_open('') ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">



                                <div class="col-md-12" <?= $this->session->user_type==1?'':'hidden'; ?>>
                                    <div class="form-group form-md-line-input form-md-floating-label">

                                        <select class="select" name="owner">
                                            <option value="" <?= set_select('owner','',true) ?>>Owner</option>

                                            <?php foreach ($this->db->select()->from('users')->get()->result() as $owner): ?>
                                                <option value="<?= $owner->id ?>" <?= set_select('owner', $owner->id, $owner->id==$cont->owner?true:'') ?>><?= $owner->first_name.' '.$owner->last_name ?></option>
                                            <?php endforeach; ?>

                                        </select>


                                        <label for="form_control_1">Owner <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>




                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="name" value="<?= $cont->name ?>">
                                        <label for="form_control_1">Group Name <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">

                                        <textarea class="form-control" name="desc"><?= $cont->desc ?></textarea>


                                        <label for="form_control_1">Group Description <?php echo form_error('desc','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>
                            </div>


                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn blue"><i class="icon-plus"></i> Save</button>

                    <button type="reset" class="btn default pull-right"> Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM panel-->

    </div>
</div>

