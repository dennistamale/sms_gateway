<?php

$contacts=$this->model->get_groups();

?>



<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE panel-->
        <div class="panel panel-flat">
            <div class="panel-heading">


                <div class="panel-title caption font-dark hidden-print">


                    <?php echo form_open('','class="form-inline" ')   ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New Group','class="btn btn-sm btn-success"'); ?>
                    <div class="form-group hidden">
                        <div class="">
                            <div class="input-group">


                                <div class="input-group">

                                    <select class="form-control input-sm" name="group_action">

                                        <option value="sms" <?php echo set_select('group_action','sms',true) ?>>Send SMS</option>
                                        <option value="email" <?php echo set_select('group_action','email',true) ?>>Send Email</option>
                                        <option value="mail_sms" <?php echo set_select('group_action','mail_sms',true) ?>>Send Mail + SMS</option>

                                    </select>

                                </div>

                            </div>

                            <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                    </div>

                    <?php echo form_close(); ?>

                </div>
                <div class="tools"> </div>
            </div>


                <table class="table datatable-button-init-basic table-striped  table-bordered  table-hover">

                    <thead>
                    <tr>
                        <th width="2%" class="hidden-print">
                            #
                        </th>
                        <th> Name </th>
                        <th> Contacts </th>
                        <th><i class="icon-menu9"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $no=1;
                    foreach($contacts as $user): ?>
                    <tr>


                        <td><?php echo $no; ?></td>


                        <td>

                            <?= anchor($this->page_level.$this->page_level2.'view/'.$user->id*date('Y'),ucwords($user->name)) ?>


                        </td>
                        <td>
                            <?php
                           echo $c=$this->db->from('contacts')->where(array('group_id'=>$user->id))->count_all_results();

                            ?>
                        </td>


                        <td style="width: 80px;">
                            <div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">

                                    <li <?= $c==0?'hidden':'' ?>>
                                        <?php echo anchor('app/messaging/group/'.$user->id*date('Y'),'<i class=" icon-paperplane"></i>  Send SMS') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'view/'.$user->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>
                                    <?php if($this->session->userdata('user_type')!='2'){ ?>
                                    <li >

                                        <?php echo anchor($this->page_level.$this->page_level2.'delete/'.$user->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                    </li>


                                    <?php } ?>
                                </ul>
                            </div></td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>

        </div>
        <!-- END EXAMPLE TABLE panel-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

