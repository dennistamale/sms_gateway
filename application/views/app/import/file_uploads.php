<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">



    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title); ?>s&nbsp;</span>
                </div>
                <div class="actions">

                    <?php //echo anchor($this->page_level.'transfer',' <i class="fa fa-exchange"></i> View Transactions','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>
                        <th width="3">#</th>
                        <th> File Name </th>
                        <th> Original Name </th>



                        <th> File Size(KB) </th>

                        <th> Uploaded by </th>


                        <th>Date</th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $no=1;
                    foreach($this->db->select('a.*,b.first_name,b.last_name')->from('file_uploads a')->join('users b','a.created_by=b.id')->order_by('a.id','desc')->get()->result() as $fl): ?>

                        <tr>

                            <td><?php echo $no; ?></td>
                            <td>

                                <a title="Download File" href="<?php echo base_url($fl->path) ?>" download ><i class=" fa fa-cloud-download" ></i> <?php
                                    echo $fl->file_name

                                    ?> </a>


                            </td>
                            <td><?php
                                echo $fl->original_name
                                ?></td>
                            <td > <?php echo $fl->file_size  ?> </td>




                            <td>

                                <?php

                              echo  anchor($this->page_level.'users/edit/'.$fl->created_by*date('Y'),ucwords($fl->first_name.' '.$fl->last_name))
                                ?> </td>


                            <td><?php echo date('d-m-Y H:i',$fl->created_on) ?></td>


                        </tr>
                        <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->