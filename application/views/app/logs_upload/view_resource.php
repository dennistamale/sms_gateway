<?php if($this->uri->segment(1)=='admin'){ ?>
    <link href="<?php echo base_url() ?>assets_public/style.css" rel="stylesheet">
<?php } ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php
$this->db->select('a.*,d.title as resource_title,d.title as res_type')
    ->from('resources a');
$this->db->join('resource_types d', 'd.id=a.resource_type');
$pub= $this->db->where(array('a.id'=>$id))->get()->row(); ?>
<div class="container">
    <div class="blog-page blog-content-2">
        <div class="row">
            <div class="col-lg-9">
                <div class="blog-single-content bordered blog-container">
                    <div class="blog-single-head">
                        <div class="kode-text ">
                            <h3>
                                <?php echo $title= ucfirst($pub->title) ?>
                            </h3>
                            <div class="kode-meta">
                                <ul>
                                    <li><a href="#"><i class="fa fa-file-text"></i><b>Type :</b> <?php echo $pub->res_type ?></a></li>

                                    <li><a href="#"><i class="fa fa-comments"></i><?php $this->db->from('pub_comments')->where('publication_id',$pub->id)->count_all_results(); ?></a>
                                        <a href="javascript:;">

                                            <span class="fb-comments-count" data-href="<?php echo base_url('index.php/'.$this->page_level.$this->page_level2.'view_resource/'.$pub->id*date('Y')) ?>"></span> Comments
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="blog-single-desc" style="padding-bottom: 5px;">
                                <?php echo ucfirst($pub->content) ?>

                            </div>
                        </div>

                    </div>
                    <div class="blog-single-foot" style="padding: 0px;border-bottom: 0px;border-top: 0px;">
                        <ul class="blog-post-tags">


                            <li class="pull-right"><?php  echo (strlen($pub->attachment)>0?'<a style="background-color: #FF9606; color:#fff;" href="'.base_url($pub->attachment).'" target="_blank"><i class="fa fa-download" ></i> Download</a>':'') ?></li>


                            <?php if($this->session->userdata('user_type')==1&&$this->uri->segment(1)=='admin'){ ?>
                                <li class="pull-right">
                                    <?php echo anchor($this->page_level.$this->page_level2.'edit_resource/'.$pub->id*date('Y'),' <i class="fa fa-pencil"></i> Edit') ?>
                                </li>
                                <li class="uppercase pull-right" >

                            <span class="pull-right" >
                                <?php echo form_open($this->page_level.$this->page_level2.'approve/'.$pub->id*date('Y'),'style="margin-top: -4px;"') ?>
                                <!--                                <label>Action</label>-->
                                <select name="action" class=" input-sm"  onchange="return confirm('Are sure you want to do this'), this.form.submit()">
                                    <option value=""  <?php echo set_select('action', '', $pub->status=='pending'?TRUE:''); ?>>Select...</option>
                                    <option value="published" <?php echo set_select('action', '', $pub->status=='published'?TRUE:''); ?> >Approve & Publish</option>
                                    <option value="evaluated" <?php echo set_select('action', '', $pub->status=='evaluated'?TRUE:''); ?>><?php echo $pub->status=='evaluated'?'Evaluated':'Evaluate' ?></option>
                                    <option value="published" <?php echo set_select('action', '', $pub->status=='published'?TRUE:''); ?>><?php echo $pub->status=='published'?'Published':'Publish' ?></option>
                                    <option value="canceled" <?php echo set_select('action', '', $pub->status=='canceled'?TRUE:''); ?>>Decline</option>
                                </select>
                                <?php echo form_close() ?>
                        </span>

                                </li>

                            <?php } ?>

                        </ul>

                    </div>
                    <div class="blog-single-img">
                        <!--                    <img src="../assets/pages/img/background/4.jpg" />-->

                        <?php


                        if(strlen($pub->attachment)>0 && strpos($pub->attachment,'.pdf')){

                            if($this->agent->browser()=='Firefox'){

                            }else {
                                echo '<div id="pdf_view"></div>';
                            }

                        }elseif(strlen($pub->attachment)>0 && (strpos($pub->attachment,'.png')||strpos($pub->attachment,'.jpg')||strpos($pub->attachment,'.jpeg')||strpos($pub->attachment,'.gif'))){

                            echo img($pub->attachment);

                        }

                        ?>





                    </div>


                    <div class="blog-comments">

                        <div class="media">
                            <div class="media-body">
                                <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                            </div>
                        </div>

                        <?php if($this->session->userdata('user_type')==1){ ?>

                            <?php $comments= $this->db->select('a.*,b.first_name,b.last_name,b.photo')->from('pub_evaluation_comments a')->join('users b','a.created_by=b.id')->where(array('publication_id'=>$id))->get()->result(); ?>
                            <h3 class="sbold blog-comments-title">Comments(<?php echo count($comments); ?>)</h3>
                            <div class="c-comment-list">
                                <?php if(count($comments)==0){ ?>
                                    <div class="media">
                                        <div class="media-body ">

                                            <?php

                                            $data=array('alert'=>'info',
                                                'message'=>'No Evaluation made yet',
                                                'hide'=>0
                                            );

                                            $this->load->view('alert',$data) ?>

                                        </div>
                                    </div>
                                <?php } ?>


                                <?php foreach($comments as $com): ?>
                                    <div class="media">
                                        <div class="media-left">

                                            <a href="#">
                                                <!--                                    <img class="media-object" alt="" src="--><?php //echo $com->photo ?><!--"> -->
                                                <?php echo img(array('src'=>$com->photo,'class'=>'media-object'))


                                                ?>

                                            </a>
                                        </div>

                                        <div class="media-body ">
                                            <h4 class="media-heading">
                                                <a href="#"><?php echo humanize($com->first_name.' '.$com->last_name) ?></a> on
                                                <span class="c-date"><?php echo  date('d M  Y, g:i a',$com->created_on) ?></span>
                                            </h4><?php

                                            echo ucfirst($com->comment);

                                            ?>



                                        </div>
                                    </div>
                                <?php endforeach; ?>



                            </div>

                            <h3 class="sbold blog-comments-title">Leave A Comment</h3>
                            <?php echo form_open_multipart() ?>
                            <!--                        <div class="form-group">-->
                            <!--                            <input type="text" placeholder="Your Name" class="form-control c-square"> </div>-->
                            <!--                        <div class="form-group">-->
                            <!--                            <input type="text" placeholder="Your Email" class="form-control c-square"> </div>-->
                            <!--                        <div class="form-group">-->
                            <!--                            <input type="text" placeholder="Your Website" class="form-control c-square"> </div>-->
                            <div class="form-group">
                                <?php echo form_error('comment','<label class="text-danger">','</label>'); ?>
                                <textarea rows="8" name="comment" value="<?php echo set_value('comment') ?>" placeholder="Write comment here ..." class="form-control c-square "></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn blue uppercase btn-md sbold btn-block">Submit</button>
                            </div>
                            <?php echo form_close(); ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="blog-single-sidebar bordered blog-container"  style="padding-top:0;">

                    <div class="<?php echo $this->uri->segment(1)=='admin'?'blog-single-sidebar-recent':'widget widget-categories' ?>" >
                        <h3 class="blog-sidebar-title uppercase">Relevant Publications</h3>
                        <ul>
                            <?php $relevant=$this->db->select('id,title')->from('resources')->where(array('status'=>'published','id !='=>$pub->id,'resource_type'=>$pub->resource_type))
                                //  ->or_where(array('sector'=>$pub->sector))
                                ->order_by('id','desc')->limit(10)->get()->result(); ?>

                            <?php
                            foreach($relevant as $r): ?>

                                <li >
                                    <?php
                                    echo   anchor($this->page_level .$this->page_level2.'view_publication/' . $r->id * date('Y'), humanize(character_limiter($r->title,20)),'class="tooltips" title="'.$r->title.'"')
                                    ?>
                                    <!--                                'class="tooltip" data-original-title="'.$r->title.'"'-->
                                </li>

                            <?php  endforeach;
                            ?>

                        </ul>
                    </div>


                    <div class="blog-single-sidebar-ui hidden">
                        <h3 class="blog-sidebar-title uppercase">UI Examples</h3>
                        <div class="row ui-margin">
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/1.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/37.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/57.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/53.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/59.jpg" />
                                </a>
                            </div>
                            <div class="col-xs-4 ui-padding">
                                <a href="javascript:;">
                                    <img src="../assets/pages/img/background/42.jpg" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .pdfobject-container { height: 500px;}
    .pdfobject { border: 1px solid #666; }
</style>
<?php  $this->load->view('plugins/pdfobject') ?>
<script>PDFObject.embed("<?php echo base_url($pub->attachment); ?>", "#pdf_view");</script>
