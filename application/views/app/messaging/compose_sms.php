
<script>

    $(document).ready(function(){
        $('.this_group').click(function(){
            var pc= $("#country").val();

            var group=$('.groups').val();

            if(group !='') {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo base_url("index.php/ajax_api/get_group")?>/' + group,
                    beforeSend:function(){
                        $(".message").html('<span class="text-primary">Please Wait Loading.....</span>');
                    },
                    success: function (d) {
                        $(".contacts").removeClass("tokenfield");
                        $(".message").html('');

                        $(".message").html(d);
                        $(".contacts").val('contacts');


                    }


                });

            }else{
                alert('Please Select Group and try again');
            }

        });
    });
</script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/editors/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/mail_list_write.js"></script>


<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/tags/tagsinput.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/tags/tokenfield.min.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() ?><!--assets/js/plugins/ui/prism.min.js"></script>-->
<!--<script type="text/javascript" src="--><? //= base_url() ?><!--assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>-->

<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_tags_input.js"></script>

<!-- Single mail -->
<div class="panel panel-white">

<!--    --><?php //print_r($this->session->userdata) ?>



    <?= form_open($this->page_level.$this->page_level2.'compose_sms'); ?>
    <!-- Mail toolbar -->
    <div class="panel-toolbar panel-toolbar-inbox">
        <div class="navbar navbar-default">
            <ul class="nav navbar-nav visible-xs-block no-border">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                        <i class="icon-circle-down2"></i>
                    </a>
                </li>
            </ul>

            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                <div class="btn-group navbar-btn">
                    <button type="submit" class="btn bg-blue"><i class=" icon-paperplane position-left"></i> Send SMS
                    </button>
                </div>


                <div class="btn-group navbar-btn" >
                    <select class="select groups" name="groups">

                        <option value="" <?php echo set_select('groups','',true) ?>>------Groups Contacts------</option>

                        <?php

                        $groups=$this->model->get_groups();

                        echo count($groups)==0?'<option value="">No groups</option>':'';

                        foreach( $groups as $u): ?>


                            <option value="<?php echo $u->id  ?>" <?= $u->id==$this->input->post('groups')?'selected':''?>><?php echo ucwords($u->name) ?></option>
                        <?php endforeach; ?>
                    </select>


                </div>



                <div class="btn-group navbar-btn" >
                    <button type="button" class="btn btn-default this_group"><i class="icon-point-left"></i> <span
                                class="hidden-xs position-right">Select This Group</span></button>
                </div>



                    <span class="message"></span>






                <div class="btn-group navbar-btn hidden">
                    <button type="button" class="btn btn-default"><i class="icon-plus2"></i> <span
                                class="hidden-xs position-right">Save</span></button>
                    <button type="button" class="btn btn-default"><i class="icon-cross2"></i> <span
                                class="hidden-xs position-right">Cancel</span></button>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu7"></i>
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">One more line</a></li>
                        </ul>
                    </div>
                </div>

                <div class="pull-right-lg ">
                    <div class="btn-group navbar-btn">
                        <button type="button" class="btn btn-default"><i class="icon-printer"></i> <span
                                    class="hidden-xs position-right">Print</span></button>
                        <button type="button" class="btn btn-default"><i class="icon-new-tab2"></i> <span
                                    class="hidden-xs position-right">Share</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /mail toolbar -->


    <!-- Mail details -->
    <div class="table-responsive mail-details-write">
        <table class="table">
            <tbody>
            <tr>
                <td style="width: 150px">To: <?= isset($total_contacts)?'('.$total_contacts.' Contacts)':''; ?></td>
                <td class="no-padding">
                    <input type="text" name="to" class="form-control tokenfield contacts"
                                              value="<?= set_value('to') ?> <?=isset($group_contacts)? $group_contacts:'' ?>" placeholder="Add recipients" >
                    <?php echo form_error('to', '<span style=" color:red;">', '</span>') ?>
                </td>
                <td  class="text-right ">
                    <ul class="list-inline list-inline-separate no-margin hidden">
                        <li><a href="#">Copy</a></li>
                        <li><a href="#">Hidden copy</a></li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td>Sender id:</td>
                <td class="no-padding">
                    <input type="text" name="subject" class="form-control"
                                              value="<?php echo set_value('subject') ?>" placeholder="Add Sender ID" maxlength="11">

                    <?php echo form_error('subject', '<span style=" color:red;">', '</span>') ?>
                </td>
                <td>&nbsp;</td>
            </tr>

            </tbody>
        </table>
    </div>
    <!-- /mail details -->


    <!-- Mail container -->
    <div class="mail-container-write">
        <div class="summernot">


            <?php

            $val = array(/* message */
                'id' => 'message',
                'type' => 'textarea',
                'placeholder' => 'Message',
                'label' => ' Message',
                'name' => 'message',
                'class' => 'form-control',
                'value' => set_value('message')
            );

            echo form_textarea($val);
            echo form_error($val['name'], '<span class="text-danger">', '</span>');

            ?>

        </div>
    </div>
    <!-- /mail container -->

    <?= form_close(); ?>
</div>
<!-- /single mail -->

