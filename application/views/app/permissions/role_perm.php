<?php
//user type is treated as role

$role=$this->db->select()->from('user_type')->where(array('id'=>$id))->get()->row();

//print_r($role);

?>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="panel panel-white">
            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-green-haze">

                    <h5 class="caption-subject text-semibold bold uppercase"> <i class="icon-key font-green-haze"></i> <?php echo $role->title ?></h5><?php echo strlen($role->description)>0? '('.$role->description.')':'' ?>
                </div>

            </div>
        </div>
            <div class="panel-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>


                <?php

                $no=1;

                foreach($this->db->select('perm_group')->from('permissions')->group_by('perm_group')->get()->result() as $pe): ?>

                    <fieldset>
                        <legend class="text-semibold"><?= humanize($pe->perm_group) ?></legend>
<!--                    <div style="border-bottom: dashed thin grey;">--><?php //echo humanize($pe->perm_group) ?><!--</div>-->


                <div class="form-body">

                    <div class="form-group">

                            <?php




                            foreach($this->db->select()->from('permissions')->where(array('perm_group'=>$pe->perm_group))->get()->result() as $perm):

                              $selected_perm=$this->db->select()->from('role_perm')->where(array('role_id'=>$role->id,'perm_id'=>$perm->id))->get()->row();

                                ?>
                                <?php if(strlen($perm->title)>0) { ?>
                                    <div class="col-md-3">

                                        <div class="checkbox">
                                            <label>
                                                <input  name="permissions[]" value="<?php echo $perm->id ?>" type="checkbox" id="checkbox<?php echo $no ?>" class="styled"  <?php echo set_checkbox('permissions[]', $perm->id,my_count($selected_perm)>0&&$perm->id==$selected_perm->perm_id?true:''); ?>>
                                                <?php echo humanize($perm->title); ?>
                                            </label>
                                        </div>


                                    </div>
                                    <?php

                                    $no++;
                                }
                            endforeach; ?>





                        <?php echo form_error('permissions', '<label class="text-danger">', '<label>') ?>
                    </div>


                </div>

                    </fieldset>

                <?php endforeach;  ?>
                <div class="form-actions">
                    <div class="row">
<!--                        col-md-offset-2-->
                        <div class="col-md-offset-10 col-md-12">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>
