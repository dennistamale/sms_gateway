

    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/user_profile_tabbed.js"></script>


    <?php $data['prof'] = $prof = $this->db->select()->from('users')->where('id', $this->session->id)->get()->row(); ?>


    <!-- Detached sidebar -->
    <div class="sidebar-detached">
        <div class="sidebar sidebar-default sidebar-separate">
            <div class="sidebar-content">

                <!-- User details -->
                <div class="content-group">
                    <div class="panel-body bg-indigo-400 border-radius-top text-center"
                         style="background-image: url(<?= strlen($prof->photo) > 0 ? base_url() . $prof->photo : base_url() . 'assets/profile_placeholder.png' ?>); background-size: contain;">
                        <div class="content-group-sm">
                            <h6 class="text-semibold no-margin-bottom">
                                <?= ucwords($prof->first_name . ' ' . $prof->last_name); ?>
                            </h6>

                            <span class="display-block">


                            <?php


                            $user_type = $this->db->select('title')->from('user_type')->where(array('id' => $prof->user_type))->get()->row();

                            echo !empty($user_type) ? $user_type->title : '';

                            ?>

                        </span>
                        </div>

                        <a href="#" class="display-inline-block content-group-sm">
                            <img src="<?= strlen($prof->photo) > 0 ? base_url() . $prof->photo : base_url() . 'assets/profile_placeholder.png' ?>"
                                 class="img-circle img-responsive" alt="" style="width: 110px; height: 110px;">
                        </a>

                        <ul class="list-inline list-inline-condensed no-margin-bottom">
                            <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-google-drive"></i></a></li>
                            <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>
                            <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-github"></i></a></li>
                        </ul>
                    </div>

                    <div class="panel no-border-top no-border-radius-top">
                        <ul class="navigation">

                            <li class="<?= $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>">
                                <a href="#basic_info" data-toggle="tab"> <i class="icon-files-empty"></i> Account Info</a>
                            </li>

                            <li hidden class="<?php echo $subtitle == 'topup' ? 'active' : ''; ?>">
                                <a href="#topup" data-toggle="tab"> <i class="icon-file-upload"></i> Topup</a>
                            </li>

                            <li hidden class="<?php echo $subtitle == 'account_statement' ? 'active' : ''; ?>">
                                <a href="#account_statement" data-toggle="tab"> <i class="icon-files-empty"></i> Account Statement</a>
                            </li>

                            <li class="navigation-header">Others</li>


                            <li class="<?php echo $subtitle == 'change_avatar' ? 'active' : ''; ?>">
                                <a href="#change_avatar" data-toggle="tab"> <i class="icon-files-empty"></i> Change
                                    Avatar</a>
                            </li>
                            <li <?php echo $subtitle == 'change_password' ? 'active' : ''; ?>>
                                <a href="#change_password" data-toggle="tab"> <i class="icon-files-empty"></i> Change
                                    Password</a>
                            </li>

                            <?php if($this->session->user_type!=9){ ?>
                            <li <?php echo $subtitle == 'sub_accounts' ? 'active' : ''; ?>>
                                <a href="#sub_accounts" data-toggle="tab"> <i class="icon-users4"></i> Sub Accounts</a>
                            </li>


                            <li <?php echo $subtitle == 'new_sub_account' ? 'active' : ''; ?>>
                                <a href="#new_sub_account" data-toggle="tab"> <i class="icon-user-plus"></i> New Sub Account</a>
                            </li>

                            <?php } ?>
                            <li>
                                <?= anchor($this->page_level . 'logout', '<i class="icon-switch2"></i> Log out') ?>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /user details -->


            </div>
        </div>
    </div>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Tab content -->
            <div class="tab-content">

                <div class="tab-pane fade in  <?= $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>" id="basic_info">
                    <?php $this->load->view('app/profile/account_info', $data); ?>
                </div>

                <div class="tab-pane fade in <?php echo $subtitle == 'topup' ? 'active' : ''; ?>" id="topup">
<!--                    --><?php //$this->load->view('app/users/topup', $data); ?>
                </div>

                <div class="tab-pane fade in <?php echo $subtitle == 'account_statement' ? 'active' : ''; ?>" id="account_statement">
<!--                    --><?php //$this->load->view('app/users/account_statement', $data); ?>
                </div>

                <div class="tab-pane fade in <?php echo $subtitle == 'change_avatar' ? 'active' : ''; ?>" id="change_avatar">
                    <?php $this->load->view('app/users/change_avatar', $data); ?>
                </div>

                <?php if($this->session->user_type!=9){ ?>

                <div class="tab-pane fade in <?php echo $subtitle == 'sub_accounts' ? 'active' : ''; ?>" id="sub_accounts">
                    <?php $this->load->view('app/users/sub_accounts', $data); ?>
                </div>



                <div class="tab-pane fade in <?php echo $subtitle == 'new_sub_account' ? 'active' : ''; ?>" id="new_sub_account">
                    <?php $this->load->view('app/profile/new_sub_account', $data); ?>
                </div>

                <?php } ?>

                <div class="tab-pane fade in <?php echo $subtitle == 'change_password' ? 'active' : ''; ?>" id="change_password">
                    <?php $this->load->view('app/profile/change_password', $data); ?>
                </div>

            </div>
            <!-- /tab content -->

        </div>
    </div>
    <!-- /detached content -->