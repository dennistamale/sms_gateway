<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css"/>
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->


<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">


    <div class="panel-body">
        <div class="table-container">
            <div class="table-actions-wrapper">

                <h6 class="panel-title bold"><?= strtoupper($subtitle) ?> Reports</h6>
                <!--                            <span> </span>-->
                <!--                            <select class="table-group-action-input form-control input-inline input-small input-sm">-->
                <!--                                <option value="">Select...</option>-->
                <!--                                <option value="Cancel">Cancel</option>-->
                <!--                                <option value="Cancel">Hold</option>-->
                <!--                                <option value="Cancel">On Hold</option>-->
                <!--                                <option value="Close">Close</option>-->
                <!--                            </select>-->
                <!--                            <button class="btn btn-sm green table-group-action-submit">-->
                <!--                                <i class="fa fa-check"></i> Submit</button>-->
            </div>

            <?= form_open(); ?>

<!--            --><?php //print_r($this->input->post()); ?>


            <table class="table table-bordered table-striped table-hover datatable-ajax" id="messages">
                <thead>

                <tr style="border-top: solid thin silver;">
                    <td colspan="10">

                        <div class="col-md-2">

                            <!--                    <div class="input-group">-->
                            <!--                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>-->





                                <input name="message_date" type="text" value="<?= date('d F, Y') ?>" class="form-filter form-control pickadate-selectors" placeholder="Date">


                            <!--                    </div>-->
                        </div>

                        <div class="col-md-2">
                            <select class="form-filter select form-control" name="network" placeholder="Network">
                                <option value="" selected>Network</option>
                                <option value="AIRTEL_RW">AIRTEL&nbsp;RW</option>
                                <option value="TIGO_RW">TIGO&nbsp;RW</option>
                                <option value="MTN_UG">MTN&nbsp;UG</option>
                                <option value="AIRTEL_UG">AIRTEL&nbsp;UG</option>
                                <option value="UTL_UG">UTL&nbsp;UG</option>

                            </select>
                        </div>

                        <div class="col-md-2">
                            <select class="form-filter select form-control" name="username" placeholder="Network">
                                <option value="" selected>Username</option>
                                <?php foreach ($this->model->get_alluser(7, 'user_type') as $client): ?>
                                    <option value="<?= $client->id ?>"><?= $client->username ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>


                        <div class="col-md-2"><input class="form-filter form-control" name="msisdn"
                                                     placeholder="msisdn"></div>


                        <div class="col-md-1"><input class="form-filter form-control" name="sender_id"
                                                     placeholder="Sender ID"></div>


                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default filter-submit"><i
                                        class="icon-equalizer2 position-left"></i>Filter
                            </button>

                            <span  class="pull-right">
                    <button type="submit" name="export" value="excel" class="btn btn-success filter-submi"><i
                                class="fa fa-file-excel-o position-left"></i>Export</button>

                </span>
                        </div>


                    </td>
                </tr>


                <tr style="border-bottom: solid thin silver !important;">

                    <th> USERNAME</th>
                    <th> MSISDN</th>
                    <th> NETWORK</th>
                    <th> SENDER&nbsp;ID</th>
                    <th> MESSAGE</th>

                    <?php if ($this->uri->segment(4) == 2015 || $this->uri->segment(4) == 2016 || $this->uri->segment(4) == 2017) {
                    } else { ?>
                        <th> CLIENT&nbsp;REF</th>
                        <th> SMSC&nbsp;STATUS</th>

                    <?php } ?>

                    <th> SENT&nbsp;DATE</th>

                    <?php if ($this->uri->segment(4) == 2015 || $this->uri->segment(4) == 2016 || $this->uri->segment(4) == 2017) {
                    } else { ?>
                        <th> DELIVERY&nbsp;DATE</th>
                    <?php } ?>


                </tr>

                </thead>
                <tbody></tbody>
            </table>

            <?= form_close() ?>
        </div>
    </div>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php $this->load->view('ajax/messages'); ?>
<!-- END PAGE LEVEL SCRIPTS -->
