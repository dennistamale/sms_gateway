<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css"/>
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->


<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">


    <div class="panel-body">
        <div class="table-container">
            <div class="table-actions-wrapper">

                <h6 class="panel-title bold"><?= strtoupper(humanize($subtitle)) ?></h6>
                <!--                            <span> </span>-->
                <!--                            <select class="table-group-action-input form-control input-inline input-small input-sm">-->
                <!--                                <option value="">Select...</option>-->
                <!--                                <option value="Cancel">Cancel</option>-->
                <!--                                <option value="Cancel">Hold</option>-->
                <!--                                <option value="Cancel">On Hold</option>-->
                <!--                                <option value="Close">Close</option>-->
                <!--                            </select>-->
                <!--                            <button class="btn btn-sm green table-group-action-submit">-->
                <!--                                <i class="fa fa-check"></i> Submit</button>-->
            </div>

            <?= form_open(); ?>

<!--            --><?php //print_r($this->input->post()); ?>


            <table class="table table-bordered table-striped table-hover datatable-ajax" id="<?= $subtitle ?>">
                <thead>

                <tr style="border-top: solid thin silver;">
                    <td colspan="6">

                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>


                                <input  name="date_range" readonly value=""  type="text" class="form-filter form-control daterange-basi daterange-predefined form-filter input-x" required="required"
                                       data-popup="tooltip" title="" data-placement="top" data-original-title="Date of "
                                />

                            </div>


                        </div>

                        <div class="col-md-2">
                            <select class="form-filter select form-control" name="username" placeholder="Network">
                                <option value="" selected>Username</option>
                                <?php foreach ($this->model->get_alluser(7, 'user_type') as $client): ?>
                                    <option value="<?= $client->id ?>"><?= $client->username ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <div  class="col-md-1"><input class="form-filter form-control" name="sender_id"
                                                     placeholder="Sender ID"></div>


                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default filter-submit"><i
                                        class="icon-equalizer2 position-left"></i>Filter
                            </button>

                            <span HIDDEN  class="pull-right">
                    <button type="submit" name="export" value="excel" class="btn btn-success filter-submi"><i
                                class="fa fa-file-excel-o position-left"></i>Export</button>

                </span>
                        </div>


                    </td>
                </tr>


                <tr style="border-bottom: solid thin silver !important;">

                    <th> Date</th>

                    <?php foreach ($networks as $n){ ?>
                    <th> <?= strtoupper(humanize($n->title)) ?></th>
                    <?php } ?>
                    <th>TOTAL</th>



                </tr>

                </thead>
                <tbody></tbody>
            </table>

            <?= form_close() ?>
        </div>
    </div>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php



$this->load->view('ajax/usage_report'); ?>
<!-- END PAGE LEVEL SCRIPTS -->
