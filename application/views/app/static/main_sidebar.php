<!-- Main sidebar -->
<div class="sidebar sidebar-main" style="background: <?//= $this->site_options->title('site_color_code') ?>;">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user hidden">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="<?php echo base_url($this->session->userdata('photo')) ?>" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo ucwords($this->session->userdata('first_name').' '.$this->session->userdata('last_name')); ?></span>
                        <div class="text-size-mini text-muted ">
                            <i class="icon-key text-size-small"></i> &nbsp;<?= $this->session->userdata('username'); ?>
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="#"><i class="icon-circle"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>


                    <li  <?=  $this->custom_library->role_exist('dashboard','group')?'':'hidden'; ?>   class="<?php echo $title=='dashboard'?'active':''; ?>">

                        <?php echo anchor('','<i class="icon-meter2"></i> <span>Dashboard</span>') ?>

                    </li>











                    <li <?=  $this->custom_library->role_exist('messaging','group')?'':'hidden'; ?> >
                        <a href="#"><i class="icon-bubbles9"></i> <span>Messaging</span></a>
                        <ul>



                            <li   class="<?php echo $title=='messaging'?'active':''; ?>">

                                <?php echo anchor($this->page_level.'messaging','<i class="icon-bubbles9"></i> <span>Messaging</span>') ?>

                            </li>




                            <li class="<?php echo $title=='contacts'?'active':''; ?>"><?php echo anchor($this->page_level.'contacts','<i class="icon-notebook"></i> <span>Contacts</span> ') ?></li>



                            <li class="<?php echo $title=='groups'?'active':''; ?>"><?php echo anchor($this->page_level.'groups','<i class="icon-users"></i> <span>Groups</span>') ?></li>



                        </ul>
                    </li>






                    <li <?=  $this->custom_library->role_exist('reports','group')?'':'hidden'; ?> >
                        <a href="#"><i class="icon-magazine"></i> <span>Reports</span></a>

                        <ul>






                            <li class="<?php echo $title=='reports'&&$subtitle=='mt'?'active':''; ?>">
                                <a href="#"><span>MT Reports</span></a>
                                <ul>
                                    <li class="<?php echo $title=='reports'&&$subtitle=='details'?'active':''; ?>">
                                        <?php echo anchor($this->page_level.'reports/details','Details Report') ?>

                                    </li>
                                    <?php for($i=2015;$i<=date('Y');$i++){ ?>
                                        <li class="activ"> <?php echo anchor($this->page_level.'reports/mt/'.$i,'MT Reports '.$i) ?></li>
                                    <?php } ?>


                                </ul>
                            </li>



                            <li class="<?php echo $title=='reports'&&$subtitle=='mo'?'active':''; ?>">
                                <a href="#"><span>MO Reports</span></a>
                                <ul>
                                    <?php for($i=2015;$i<=date('Y');$i++){ ?>
                                        <li class="activ"> <?php echo anchor($this->page_level.'reports/mo/'.$i,'MO Reports '.$i) ?></li>
                                    <?php } ?>


                                </ul>
                            </li>

                            <li class="<?php echo $title=='reports'&&$subtitle=='aggregated'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'reports/aggregated','Aggregated Summary') ?>
                            </li>



                            <li class="<?php echo $title=='reports'&&$subtitle=='usage_report'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'reports/usage_report','Usage  Report') ?>
                            </li>





                        </ul>
                    </li>





                    <li hidden class="navigation-header"><span>System Utilities</span> <i class="icon-menu" title="Main pages"></i></li>




                    <li <?=  $this->custom_library->role_exist('billing','group')?'':'hidden'; ?> >
                        <a href="#"><i class="icon-price-tag"></i> <span>Billing</span></a>
                        <ul>
                            <li  <?=  $this->custom_library->role_exist('view_topup_history')?'':'hidden'; ?>  class="<?= $title=='billing'&&$subtitle=='topup'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'billing/topup','Topup') ?>
                            </li>

                            <li <?=  $this->custom_library->role_exist('topup')?'':'hidden'; ?>  class="<?= $title=='billing'&&$subtitle==''?'active':''; ?>">
                                <?php echo anchor($this->page_level.'billing','Topup History') ?>
                            </li>

                        </ul>
                    </li>




                    <li  <?=  $this->custom_library->role_exist('user management','group')?'':'hidden'; ?>    class=" <?= $title=='profile'||$title=='permissions'?'active':''; ?>">
                        <a href="#"><i class="icon-user"></i> <span>Accounts Management</span></a>
                        <ul>


                            <li class="<?php echo $subtitle=='profile'?'active':''; ?>"><?php echo anchor($this->page_level.'profile','Basic Info') ?></li>


                            <li <?=  $this->custom_library->role_exist('new user')?'':'hidden'; ?> class="<?php echo $title=='users' && ($subtitle=='new'||$subtitle=='edit')?'active':''; ?>">
                                <?php echo anchor($this->page_level.'users/new','Add User') ?>
                            </li>

                            <li <?=  $this->custom_library->role_exist('View User List')?'':'hidden'; ?> class="<?php echo $title=='users' && ($subtitle==''||$subtitle=='view_users')?'active':''; ?>">
                                <?php echo anchor($this->page_level.'users','View Users') ?>
                            </li>


                            <li <?=  $this->custom_library->role_exist('View User List')?'':'hidden'; ?>  class="<?php echo $title=='users' && $subtitle=='filter' && $this->uri->segment(4) == 'active'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'users/filter/active','active') ?>
                            </li>


                            <li <?=  $this->custom_library->role_exist('View User List')?'':'hidden'; ?>  class="<?php echo $title=='users' && $subtitle=='filter'&& $this->uri->segment(4) == 'blocked'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'users/filter/blocked','Inactive') ?>
                            </li>




                            <li class="  <?=  $this->custom_library->role_exist('permissions')&& $this->session->user_type=='1'?'':'hidden'; ?>  <?php echo ($title=='permissions'&&$subtitle=='')||$subtitle=='perm_group'||($title=='permissions'&&$subtitle=='edit')||($title=='permissions'&&$subtitle=='new')?'active':''; ?>">
                                <?php echo anchor($this->page_level.'permissions','Permissions') ?></li>

                            <li class="  <?php echo  $this->custom_library->role_exist('roles')&& $this->session->user_type=='1'?'':'hidden'; ?>  <?php echo $title=='permissions'&&$subtitle=='roles'||$subtitle=='remove_permission'?'active':''; ?>"><?php echo anchor($this->page_level.'permissions/roles','Roles') ?></li>




                        </ul>
                    </li>



                    <li <?=  $this->custom_library->role_exist('audit logs','group')?'':'hidden'; ?>  class="<?php echo $title=='logs'?'active':''; ?>">

                        <?php echo anchor($this->page_level.'logs','<i class="icon-list"></i> <span>Audit Trail</span>') ?>

                    </li>


                    <li <?=  $this->custom_library->role_exist('settings','group')?'':'hidden'; ?> >
                        <a href="#"><i class="icon-cogs"></i> <span>Settings</span></a>
                        <ul>
                            <li class="<?php
                            echo $title=='settings' &&($subtitle=='countries'|| $subtitle==''||$subtitle=='new_country'||$subtitle=='ban_country'||$subtitle=='delete_country'||$subtitle=='unblock_country')?'active':''; ?>">
                                <?php echo anchor($this->page_level.'settings/countries','Countries') ?>
                            </li>
<li class="<?php echo $title=='settings'&&$subtitle=='regions'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'settings/regions','Regions') ?>
                            </li>

                            <li  class="<?php echo $title=='site_options'?'active':''; ?>">
                                <?php echo anchor($this->page_level.'site_options','Site Options') ?>
                            </li>



                        </ul>
                    </li>




                    <li hidden>
                        <a href="#"><i class="icon-bubble"></i> <span>Color system</span></a>
                        <ul>
                            <li class="active"><a href="colors_primary.html">Primary palette</a></li>
                            <li><a href="colors_danger.html">Danger palette</a></li>

                        </ul>
                    </li>


                    <!-- /main -->



                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->


<?php  if($this->uri->segment(2)=='messaging'){ ?>


<!-- Secondary sidebar -->
<div class="sidebar sidebar-secondary sidebar-default">
    <div class="sidebar-content">

        <!-- Actions -->
        <div class="sidebar-category">
            <div class="category-title">
                <span>Actions</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">

                <?= anchor($this->page_level.'messaging/compose','Compose','class="btn bg-indigo-400 btn-block"') ?>
            </div>
        </div>
        <!-- /actions -->


        <!-- Sub navigation -->
        <div class="sidebar-category">
            <div class="category-title hidden">
                <span>Navigation</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-alt navigation-accordion no-padding-bottom">
                    <li class="navigation-header">Folders</li>
                    <li class="<?= $this->uri->segment(3)==''?'active':'' ?>"><a href="<?= base_url($this->page_level.'messaging') ?>"><i class="icon-drawer-in"></i> Inbox <span class="badge badge-success hidden">32</span></a></li>
                    <li class=" <?= $this->uri->segment(3)=='sent'?'active':'' ?>"> <?= anchor($this->page_level.'messaging/sent','<i class="icon-drawer-out"></i> Sent') ?></li>
                    <li class="<?= $this->uri->segment(3)=='drafts'?'active':'' ?>"><a href="#"><i class="icon-drawer3"></i> Drafts</a></li>


                </ul>
            </div>
        </div>
        <!-- /sub navigation -->



    </div>
</div>
<!-- /secondary sidebar -->

<?php } ?>