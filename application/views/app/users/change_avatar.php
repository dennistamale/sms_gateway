
<div class="panel panel-flat">
    <div class="panel-heading ">
        <h5 class="panel-title">Change Photo </h5>
        <div class="heading-elements">
            <ul class="icons-list">

                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">



<p>
    You Can Make Changes to the photo.
</p>
<?php echo form_open_multipart($this->page_level.$this->page_level2.'change_avatar/'.$prof->id*date('Y'))?>
<div class="form-group">

    <?php if( isset($error)){?>
        <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
    <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

    <div class="fileinput fileinput-new" data-provides="fileinput">
<!--        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
            <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
<!--        </div>-->
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
        </div>
        <div>
																<span class="btn btn-warning btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
            <a href="#" class="btn  fileinput-exists" data-dismiss="fileinput">
                Remove </a>
        </div>
    </div>

</div>

<?php echo form_error('pa','<label style="color:red;">','</label>') ?>
<input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>

<div class="margin-top-10">

    <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
    <a href="#" class="btn default">
        Cancel </a>
</div>
<?php echo form_close() ?>

    </div>
</div>
