

<div class="row">

    <div class="col-md-12 ">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="panel panel-flat">

            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">New User</span>
                </div>
                <div class="heading-elements">

                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
        </div>
            <div class="panel-body form">

                <?php echo form_open('') ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <select class="select" name="role" id="role"  required >

                                            <option value="" <?php echo set_select('role', '', TRUE); ?> >Select User Role</option>
                                            <?php foreach($this->db->select('id,title')->from('user_type')->order_by('id','asc')->get()->result() as $role): ?>
                                                <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
                                            <?php  endforeach; ?>



                                        </select>
                                        <label for="form_control_1">User Role</label>
                                    </div>
                                </div>


                                <div class="col-md-6">



                                    <div class="form-group">


                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="gender" value="M" <?php echo set_radio('gender','M'); ?> >
                                            Male
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="gender" value="F" <?php echo set_radio('gender','F'); ?> >
                                            Female
                                        </label>
                                        <label class="display-block">Gender: <?php echo form_error('gender','<span style=" color:red;">','</span>') ?></label>
                                    </div>

                                </div>

</div>

                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name') ?>">
                                        <label for="form_control_1">First Name <?php echo form_error('first_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name') ?>">
                                        <label for="form_control_1">Last Name <?php echo form_error('last_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input required type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>">
                                        <label for="form_control_1">Username <?php echo form_error('username','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="email" class="form-control" name="email" value="<?php echo set_value('email') ?>" id="form_control_1">
                                        <label for="form_control_1">Email  <?php echo form_error('email','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="phone" class="form-control" name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1">
                                        <label for="for m_control_1">Phone  <?php echo form_error('phone','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">



                                    <div class="form-group">


                                            <div class="input-group">
                                                <input name="dob" type="text" class="form-control pickadate-accessibility picker__input picker__input--active" data-date-format="YYYY-mm-dd" placeholder="Date of birth" size="16" value="<?php echo set_value('dob') ?>">
                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            </div>
                                        <label for="form_control_1">Date of birth <?php echo form_error('dob','<label style="color: red;">','<label>') ?></label>
                                    </div>




                                </div>


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <select class="select" name="country" id="country"><?php echo form_error('country','<label class="text-danger">','<label>') ?>
                                            <option value="" <?php echo set_select('country', ''); ?>>Select Country...</option>
                                            <?php foreach($this->model->get_countries() as $tg): ?>
                                                <option value="<?php echo $tg->a2_iso ?>" <?php echo set_select('country', $tg->a2_iso,$tg->a2_iso=='UG'?true:''); ?>><?php echo $tg->country; ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                        <label for="form_control_1">Select Country</label>
                                    </div>
                                </div>

                                <div class="col-md-6" hidden>
                                    <div class="form-group form-md-line-input has-success"><?php echo form_error('city','<label style="color: red;">','<label>') ?>
                                        <select class="form-control" name="city" id="city">
                                            <option value="" <?php echo set_select('city', '',true); ?>>Select City...</option>

                                        </select>
                                        <label for="form_control_1">Select City</label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <select class="select" name="currency" id="currency"><?php echo form_error('country','<label class="text-danger">','<label>') ?>
                                            <option value="" <?php echo set_select('currency', ''); ?>>Select Currency...</option>
                                            <?php foreach($this->model->get_countries() as $tg): ?>
                                                <option value="<?php echo $tg->currency_alphabetic_code ?>" <?php echo set_select('currency', $tg->currency_alphabetic_code,$tg->currency_alphabetic_code=='RWF'?true:''); ?>><?php echo $tg->currency_name.' ('.$tg->currency_alphabetic_code.')'; ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                        <label for="form_control_1">Select Currency</label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="number" class="form-control" name="rate" value="<?php echo set_value('rate') ?>" id="rate">
                                        <label for="for m_control_1">Rate  <?php echo form_error('rate','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>





                            </div>

                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn btn-success"><i class="icon-user-plus"></i> Add User</button>

                    <button type="reset" class="btn default">Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>
</div>
