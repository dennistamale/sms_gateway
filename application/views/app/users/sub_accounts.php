

<?php if(isset($this->session->id)){ ?>


    <?php



    $user_id=isset($user_id)?$user_id:$this->session->id;




    $t=$this->model->get_sub_accounts($user_id,'parent_id');





    ?>

    <?php

    $status_list = array(
        array("info" => "Submitted"),
        array("warning" => "Printing"),
        array("default" => "Issued"),
        array("success" => "Picked"),
        array("danger" => "Error")
    );

    ?>


    <!-- BEGIN PAGE CONTENT-->
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="panel panel-flat">



                <div class="panel-heading ">
                    <div class="panel-title">


                        <div class="caption font-dark hidden-print">


                            <?php echo form_open('','class="form-inline" ')   ?>
                            <?php echo anchor($this->page_level.$this->page_level2.'new_sub_account',' <i class="fa fa-plus"></i> New Account','class="btn btn-sm btn-primary"'); ?>
                            <?php echo anchor($this->page_level.'billing/topup',' <i class="icon-file-upload"></i> Account Topup','class="btn btn-sm btn-success"'); ?>


                            <?php echo form_close(); ?>

                        </div>
                        <div class="tools"> </div>
                    </div>

                    <!--                    <h5 class="panel-title">Topup </h5>-->
                    <div class="heading-elements">
                        <ul class="icons-list">

                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>


                </div>
                <div class="panel-body">
                    <table class="table  datatable-button-init-basic" id="sample_1">


                        <thead>
                        <tr>
                            <th width="2">#</th>

                            <th> Name </th>
                            <th> Phone </th>
                            <th> Rate </th>
                            <th> Balance </th>
                            <th> Gender </th>





                            <th> Created on</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>



                        <?php

                        if($t!=false) {

                            $no = 1;
                            foreach ($t as $app): ?>
                                <tr>
                                    <td><?= $no; ?></td>

                                    <td>
                                        <?= $this->session->user_type == 1 ||$this->session->user_type == 7  ? anchor($this->page_level . 'users/edit/' . $app->id * date('Y'), $app->first_name . ' ' . $app->last_name) : $app->first_name . ' ' . $app->last_name; ?>

                                    </td>




                                    <td> <?= $app->phone ?> </td>

                                    <!--                            <th>  Applicant </th>-->
                                    <td>  <?= $app->rate ?> </td>
                                    <td>  <?= $app->balance ?> </td>
                                    <td>  <?= $app->gender ?> </td>

                                    <td> <?= trending_date_time($app->created_on) ?> </td>


                                    <td style="width: 80px;">


                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><?= $this->session->user_type == 1 ||$this->session->user_type == 7  ? anchor($this->page_level . 'users/edit/' . $app->id * date('Y'), '  <i class=" icon-eye"></i> View') : ''; ?></li>

                                                    <li><?= $this->session->user_type == 7 ? anchor($this->page_level . 'billing/topup/' . $app->id * date('Y'), '  <i class=" icon-price-tag"></i> Topup') : ''; ?></li>


                                                </ul>
                                            </li>
                                        </ul>


                                    </td>
                                </tr>
                                <?php $no++; endforeach;
                        }
                            ?>

                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->



<?php } ?>