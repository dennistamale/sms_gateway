<?//= validation_errors() ?>
<div class="panel panel-flat">
    <div class="panel-heading ">
        <h5 class="panel-title">Topup </h5>
        <div class="heading-elements">
            <ul class="icons-list">

                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

<?php


$prof_id=isset($prof)?$prof->id*date('Y'):'';

echo form_open($this->page_level.$this->page_level2.'topup/'.$prof_id) ?>

       <div class="row">
           <div class="col-md-6">
        <div class="form-group">
    <label class="control-label">Account </label><?php echo form_error('account','<label style="color: red;">','</label>') ?>

            <?php


            $accounts=$this->session->user_type == 7?$this->db->select()->from('users')->where('parent_id',$this->session->id)->get()->result():$this->model->get_alluser(7, 'user_type');

            $default_topup=strlen($this->uri->segment(4))>0?intval($this->uri->segment(4))/date('Y'):null;

            ?>


    <select class="form-filter select form-control" required name="account" placeholder="account">
        <option value="" selected>Account</option>
        <?php foreach ($accounts as $client): ?>
            <option value="<?= $client->id ?>"
                <?php echo set_select('account', $client->id,($client->id==(isset($default_topup)?$default_topup:$prof_id)?true:'')); ?>
            ><?= $client->username ?></option>
        <?php endforeach; ?>

    </select>

</div>
           </div>
           <div class="col-md-6">
        <div class="form-group">
    <label class="control-label">Amount </label><?php echo form_error('amount','<label style="color: red;">','</label>') ?>
    <input name="amount" required autocomplete="off" type="number" value="<?php echo set_value('amount') ?>" class="form-control"/>
</div>
           </div>

       </div>

        <div class="form-group">
    <label class="control-label">Remarks </label><?php echo form_error('remarks','<label style="color: red;">','</label>') ?>
    <textarea class="form-control" name="remarks"></textarea>

</div>

<div class="margin-top-">
    <button type="submit" class="btn btn-success"><i class="icon-file-upload"></i> Topup </button>
    <button type="reset" class="btn btn-default"> Cancel </button>
</div>
<?php echo form_close(); ?>

    </div>
</div>

