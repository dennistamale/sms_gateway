
<?php $data['prof']=$prof=$this->db->select()->from('users')->where('id',$id)->get()->row(); ?>


<div class="row">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar col-md-3" >
        <!-- PORTLET MAIN -->
<!--        <div class="panel panel-flat">-->
            <!-- SIDEBAR USERPIC -->
            <div class="thumbnail">
            <div class="thumb">
                <img src="<?= strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="caption">
                <h6 class="text-semibold no-margin-top">
                   <?= ucwords($prof->first_name.' '.$prof->last_name); ?>
                </h6>

            </div>
        </div>
            <!-- END SIDEBAR USER TITLE -->


<!--        </div>-->
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">

                    <div class="panel-heading">
                    <div class="panel-title tabbable-line">
                        <div class="caption caption-md">



                            <div class="panel-headings">

                                <i class="icon-globe theme-font hide"></i>

                                <?php if($prof->user_type==9){
                                   $user_details= $this->model->get_user_details($prof->id);
                                   $diplomat=$this->model->get_user_details($user_details->parent_id);
                                } ?>

                                <span class="caption-subject font-blue-madison bold uppercase" style="font-size: larger;"> <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?> Profile Account &nbsp;&nbsp;&nbsp;



                                    <?= $prof->user_type==9?'(Reseller : '.(!empty($diplomat)?(anchor($this->page_level.'users/edit/'.$user_details->parent_id*date('Y'),$diplomat->first_name.' '.$diplomat->last_name)):'').')':'' ?></span>

                                <div class="heading-elements">

                                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn btn-primary btn-xs"'); ?>
                                    <?php echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-warning btn-xs"'); ?>


                                    <?php if($prof->user_type==7){ ?>

                                        <?php echo anchor($this->page_level.'arrival_departure/add_dependant/'.$prof->id*date('Y'),'  <i class="icon-user-plus"></i> Add Sub account','class="btn bg-violet btn-xs"') ?>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>


                    <div class="panel-body">

                        <ul class="nav nav-tabs">

                            <li class="<?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>">
                                <a href="#basic_info" data-toggle="tab">Account Info</a>
                            </li>

                            <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                                <a href="#change_avatar" data-toggle="tab">Change Avatar</a>
                            </li>
                            <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                                <a href="#change_password" data-toggle="tab">Change Password</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->

                            <div class="tab-pane fade in <?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>" id="basic_info">

                                <?php $this->load->view('app/users/basic_info',$data); ?>

                            </div>

                            <!-- END PERSONAL INFO TAB -->


                            <!-- END CHANGE AVATAR TAB --> <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane fade <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="change_avatar">
                                <?php $this->load->view('app/users/change_avatar',$data); ?>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane fade in <?= $subtitle=='change_password'?'active':''; ?>" id="change_password">

                                <?php $this->load->view('app/users/change_password',$data); ?>

                            </div>
                            <!-- END CHANGE PASSWORD TAB -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
</div>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>-->
