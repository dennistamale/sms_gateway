<div class="row">
    <div class="col-md-5">
        <div class="dataTables_info hidden-print" id="sample_2_info" role="status" aria-live="polite">Showing <?php echo isset($per_page)?$per_page:''; ?> of <?php echo isset($number)?$number:''; ?> entries</div>
    </div>
    <div class="col-md-7 pull-right">
        <div class="dataTables_paginate pull-right hidden-print paging_simple_numbers" id="sample_2_paginate">
            <ul class="pagination">
            <?php echo isset($pg)?$pg:''; ?>
            </ul>
        </div>
    </div>
</div>