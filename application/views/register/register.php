


<!-- Theme JS files -->
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/validation/validate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/inputs/touchspin.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/switchery.min.js"></script>


<?php $this->load->view('register/form_validation_js'); ?>

<body class="login-container">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Registration form -->
                <?php
                $attr=array(
                        'class'=>'form-validate-jquery'
                );
                ?>
                <?= form_open('',$attr) ?>

                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="panel registration-form">
                                <div class="panel-body shadow">
                                    <div class="text-center">
                                        <div class="icon-object border-success text-success">
<!--                                            <i class="icon-plus3"></i>-->
                                            <img class="login-logo"  src="<?php echo base_url($this->site_options->title('site_logo')) ?>" style=" max-width: 130px !important;" />
                                        </div>
                                        <h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="username" value="<?= set_value('username') ?>" class="form-control" placeholder="Choose username" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-plus text-muted"></i>
                                                </div>
                                            </div>

                                            <?= form_error('username','<span class="text-danger">','</span>') ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="national_id"  value="<?= set_value('national_id') ?>"  class="form-control" placeholder="Enter National ID" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-vcard text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('national_id','<span class="text-danger">','</span>') ?>
                                        </div>
                                </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="first_name"  value="<?= set_value('first_name') ?>"  class="form-control" placeholder="First name" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('first_name','<span class="text-danger">','</span>') ?>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="last_name"  value="<?= set_value('last_name') ?>"  class="form-control" placeholder="Surname" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('last_name','<span class="text-danger">','</span>') ?>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="phone1"  value="<?= set_value('phone1') ?>"  class="form-control" placeholder="Phone No1 e.g(07XXXXXXXX)" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-phone text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('phone1','<span class="text-danger">','</span>') ?>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="text" name="phone2"  value="<?= set_value('phone2') ?>"  class="form-control" placeholder="Phone No2 e.g(07XXXXXXXX)">
                                                <div class="form-control-feedback">
                                                    <i class="icon-phone text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('phone2','<span class="text-danger">','</span>') ?>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="email" name="email"  value="<?= set_value('email') ?>"  class="form-control" placeholder="Your email">
                                                <div class="form-control-feedback">
                                                    <i class="icon-mention text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('email','<span class="text-danger">','</span>') ?>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">

                                                <select name="account_type" class="select form-control" required>
                                                    <option value="" <?= set_select('account_type','',true) ?>>Account Type</option>
                                                    <option value="3" <?= set_select('account_type',3) ?>>Cow Owner</option>
                                                    <option value="4" <?= set_select('account_type',4) ?>>Trader</option>
                                                </select>
                                                <div class="form-control-feedback">
                                                    <i class="icon-list text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('account_type','<span class="text-danger">','</span>') ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="password"  name="password" id="password"  class="form-control" placeholder="Create password">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-lock text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('password','<span class="text-danger">','</span>') ?>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <input type="password"  name="repeat_password"  class="form-control" placeholder="Repeat password" required>
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-lock text-muted"></i>
                                                </div>
                                            </div>
                                            <?= form_error('repeat_password','<span class="text-danger">','</span>') ?>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <div class="checkbox">
                                            <label>
                                                <input value="1" <?php echo set_checkbox('single_basic_checkbox', '1'); ?> type="checkbox"  name="single_basic_checkbox"  class="styled" required>

                                                Accept <a href="#">terms of service</a>
                                            </label>
                                        </div>
                                        <?= form_error('single_basic_checkbox','<span class="text-danger">','</span>') ?>
                                    </div>

                                    <div class="text-right">

                                        <?=  anchor('login','<i class="icon-arrow-left13 position-left"></i> Back to login form','class="btn btn-link"'); ?>

                                        <button type="submit" class="btn bg-success btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               <?= form_close() ?>
                <!-- /registration form -->

